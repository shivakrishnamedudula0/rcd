#include <stdio.h>
#include <string.h>
#include <string.h>


struct person
{
    char name[30];
    double gdp[20];
	struct node *next;
};

int main()
{
    FILE* fp = fopen("cscfile_country.csv", "r");
 
    if (!fp)
        printf("Can't open file\n");
 
    else {
        char buffer[1024];
        int row = 0;
        int column = 0; 
        while (fgets(buffer, 1024, fp)) {
            column = 0;
            row++;
            if (row == 1)
                continue;
 
            // Splitting the data
			//char gfg[100] = " 1997 Ford E350 ac 3000.00";

            char gfg[100] = strtok(buffer, ",");
			
			// Declaration of delimiter
			const char s[2] = ",";
			char* tok;

	// Use of strtok
	// get first token
	tok = strtok(gfg, s);
 
         // Checks for delimiter
	while (tok != 0) {
		printf("%s, ", tok);

		// Use of strtok
		// go through other tokens
		tok = strtok(0, s);
	}
        // Close the file
        fclose(fp);
    }
    return 0;
}
}
