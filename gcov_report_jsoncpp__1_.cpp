File 'jsoncpp.cpp'
Lines executed:24.46% of 3082
Creating 'jsoncpp.cpp.gcov'

    -:    0:Source:jsoncpp.cpp
        -:    1:
        -:    2:#include "json/json.h"
        -:    3:
        -:    4:#ifndef JSON_IS_AMALGAMATION
        -:    5:#error "Compile with -I PATH_TO_JSON_DIRECTORY"
        -:    6:#endif
        -:    7:
        -:    8:
        -:    9:
        -:   10:#ifndef LIB_JSONCPP_JSON_TOOL_H_INCLUDED
        -:   11:#define LIB_JSONCPP_JSON_TOOL_H_INCLUDED
        -:   12:
        -:   13:#if !defined(JSON_IS_AMALGAMATION)
        -:   14:#include <json/config.h>
        -:   15:#endif
        -:   16:
        -:   17:// Also support old flag NO_LOCALE_SUPPORT
        -:   18:#ifdef NO_LOCALE_SUPPORT
        -:   19:#define JSONCPP_NO_LOCALE_SUPPORT
        -:   20:#endif
        -:   21:
        -:   22:#ifndef JSONCPP_NO_LOCALE_SUPPORT
        -:   23:#include <clocale>
        -:   24:#endif
        -:   25:
        -:   26:/* This header provides common string manipulation support, such as UTF-8,
        -:   27: * portable conversion from/to string...
        -:   28: *
        -:   29: * It is an internal header that must not be exposed.
        -:   30: */
        -:   31:
        -:   32:namespace Json {
        -:   33:static inline char getDecimalPoint() {
        -:   34:#ifdef JSONCPP_NO_LOCALE_SUPPORT
        -:   35:  return '\0';
        -:   36:#else
        -:   37:  struct lconv* lc = localeconv();
        -:   38:  return lc ? *(lc->decimal_point) : '\0';
        -:   39:#endif
        -:   40:}
        -:   41:
        -:   42:/// Converts a unicode code-point to UTF-8.
    #####:   43:static inline String codePointToUTF8(unsigned int cp) {
    #####:   44:  String result;
        -:   45:
        -:   46:  // based on description from http://en.wikipedia.org/wiki/UTF-8
        -:   47:
    #####:   48:  if (cp <= 0x7f) {
    #####:   49:    result.resize(1);
    #####:   50:    result[0] = static_cast<char>(cp);
    #####:   51:  } else if (cp <= 0x7FF) {
    #####:   52:    result.resize(2);
    #####:   53:    result[1] = static_cast<char>(0x80 | (0x3f & cp));
    #####:   54:    result[0] = static_cast<char>(0xC0 | (0x1f & (cp >> 6)));
    #####:   55:  } else if (cp <= 0xFFFF) {
    #####:   56:    result.resize(3);
    #####:   57:    result[2] = static_cast<char>(0x80 | (0x3f & cp));
    #####:   58:    result[1] = static_cast<char>(0x80 | (0x3f & (cp >> 6)));
    #####:   59:    result[0] = static_cast<char>(0xE0 | (0xf & (cp >> 12)));
    #####:   60:  } else if (cp <= 0x10FFFF) {
    #####:   61:    result.resize(4);
    #####:   62:    result[3] = static_cast<char>(0x80 | (0x3f & cp));
    #####:   63:    result[2] = static_cast<char>(0x80 | (0x3f & (cp >> 6)));
    #####:   64:    result[1] = static_cast<char>(0x80 | (0x3f & (cp >> 12)));
    #####:   65:    result[0] = static_cast<char>(0xF0 | (0x7 & (cp >> 18)));
        -:   66:  }
        -:   67:
    #####:   68:  return result;
        -:   69:}
        -:   70:
        -:   71:enum {
        -:   72:  /// Constant that specify the size of the buffer that must be passed to
        -:   73:  /// uintToString.
        -:   74:  uintToStringBufferSize = 3 * sizeof(LargestUInt) + 1
        -:   75:};
        -:   76:
        -:   77:// Defines a char buffer for use with uintToString().
        -:   78:using UIntToStringBuffer = char[uintToStringBufferSize];
        -:   79:
        -:   80:/** Converts an unsigned integer to string.
        -:   81: * @param value Unsigned integer to convert to string
        -:   82: * @param current Input/Output string buffer.
        -:   83: *        Must have at least uintToStringBufferSize chars free.
        -:   84: */
      166:   85:static inline void uintToString(LargestUInt value, char*& current) {
      166:   86:  *--current = 0;
      296:   87:  do {
      462:   88:    *--current = static_cast<char>(value % 10U + static_cast<unsigned>('0'));
      462:   89:    value /= 10;
      462:   90:  } while (value != 0);
      166:   91:}
        -:   92:
        -:   93:/** Change ',' to '.' everywhere in buffer.
        -:   94: *
        -:   95: * We had a sophisticated way, but it did not work in WinCE.
        -:   96: * @see https://github.com/open-source-parsers/jsoncpp/pull/9
        -:   97: */
    #####:   98:template <typename Iter> Iter fixNumericLocale(Iter begin, Iter end) {
    #####:   99:  for (; begin != end; ++begin) {
    #####:  100:    if (*begin == ',') {
    #####:  101:      *begin = '.';
        -:  102:    }
        -:  103:  }
    #####:  104:  return begin;
        -:  105:}
        -:  106:
        -:  107:template <typename Iter> void fixNumericLocaleInput(Iter begin, Iter end) {
        -:  108:  char decimalPoint = getDecimalPoint();
        -:  109:  if (decimalPoint == '\0' || decimalPoint == '.') {
        -:  110:    return;
        -:  111:  }
        -:  112:  for (; begin != end; ++begin) {
        -:  113:    if (*begin == '.') {
        -:  114:      *begin = decimalPoint;
        -:  115:    }
        -:  116:  }
        -:  117:}
        -:  118:
        -:  119:/**
        -:  120: * Return iterator that would be the new end of the range [begin,end), if we
        -:  121: * were to delete zeros in the end of string, but not the last zero before '.'.
        -:  122: */
    #####:  123:template <typename Iter> Iter fixZerosInTheEnd(Iter begin, Iter end) {
    #####:  124:  for (; begin != end; --end) {
    #####:  125:    if (*(end - 1) != '0') {
    #####:  126:      return end;
        -:  127:    }
        -:  128:    // Don't delete the last zero before the decimal point.
    #####:  129:    if (begin != (end - 1) && *(end - 2) == '.') {
    #####:  130:      return end;
        -:  131:    }
        -:  132:  }
    #####:  133:  return end;
        -:  134:}
        -:  135:
        -:  136:} // namespace Json
        -:  137:
        -:  138:#endif // LIB_JSONCPP_JSON_TOOL_H_INCLUDED
        -:  139:
        -:  140:// //////////////////////////////////////////////////////////////////////
        -:  141:// End of content of file: src/lib_json/json_tool.h
        -:  142:// //////////////////////////////////////////////////////////////////////
        -:  143:
        -:  144:
        -:  145:
        -:  146:
        -:  147:
        -:  148:
        -:  149:// //////////////////////////////////////////////////////////////////////
        -:  150:// Beginning of content of file: src/lib_json/json_reader.cpp
        -:  151:// //////////////////////////////////////////////////////////////////////
        -:  152:
        -:  153:
        -:  154:#if !defined(JSON_IS_AMALGAMATION)
        -:  155:#include "json_tool.h"
        -:  156:#include <json/assertions.h>
        -:  157:#include <json/reader.h>
        -:  158:#include <json/value.h>
        -:  159:#endif // if !defined(JSON_IS_AMALGAMATION)
        -:  160:#include <algorithm>
        -:  161:#include <cassert>
        -:  162:#include <cstring>
        -:  163:#include <iostream>
        -:  164:#include <istream>
        -:  165:#include <limits>
        -:  166:#include <memory>
        -:  167:#include <set>
        -:  168:#include <sstream>
        -:  169:#include <utility>
        -:  170:
        -:  171:#include <cstdio>
        -:  172:#if __cplusplus >= 201103L
        -:  173:
        -:  174:#if !defined(sscanf)
        -:  175:#define sscanf std::sscanf
        -:  176:#endif
        -:  177:
        -:  178:#endif //__cplusplus
        -:  179:
        -:  180:#if defined(_MSC_VER)
        -:  181:#if !defined(_CRT_SECURE_CPP_OVERLOAD_STANDARD_NAMES)
        -:  182:#define _CRT_SECURE_CPP_OVERLOAD_STANDARD_NAMES 1
        -:  183:#endif //_CRT_SECURE_CPP_OVERLOAD_STANDARD_NAMES
        -:  184:#endif //_MSC_VER
        -:  185:
        -:  186:#if defined(_MSC_VER)
        -:  187:// Disable warning about strdup being deprecated.
        -:  188:#pragma warning(disable : 4996)
        -:  189:#endif
        -:  190:
        -:  191:// Define JSONCPP_DEPRECATED_STACK_LIMIT as an appropriate integer at compile
        -:  192:// time to change the stack limit
        -:  193:#if !defined(JSONCPP_DEPRECATED_STACK_LIMIT)
        -:  194:#define JSONCPP_DEPRECATED_STACK_LIMIT 1000
        -:  195:#endif
        -:  196:
        -:  197:static size_t const stackLimit_g =
        -:  198:    JSONCPP_DEPRECATED_STACK_LIMIT; // see readValue()
        -:  199:
        -:  200:namespace Json {
        -:  201:
        -:  202:#if __cplusplus >= 201103L || (defined(_CPPLIB_VER) && _CPPLIB_VER >= 520)
        -:  203:using CharReaderPtr = std::unique_ptr<CharReader>;
        -:  204:#else
        -:  205:using CharReaderPtr = std::auto_ptr<CharReader>;
        -:  206:#endif
        -:  207:
        -:  208:// Implementation of class Features
        -:  209:// ////////////////////////////////
        -:  210:
        -:  211:Features::Features() = default;
        -:  212:
    #####:  213:Features Features::all() { return {}; }
        -:  214:
    #####:  215:Features Features::strictMode() {
    #####:  216:  Features features;
    #####:  217:  features.allowComments_ = false;
    #####:  218:  features.strictRoot_ = true;
    #####:  219:  features.allowDroppedNullPlaceholders_ = false;
    #####:  220:  features.allowNumericKeys_ = false;
    #####:  221:  return features;
        -:  222:}
        -:  223:
        -:  224:// Implementation of class Reader
        -:  225:// ////////////////////////////////
        -:  226:
    #####:  227:bool Reader::containsNewLine(Reader::Location begin, Reader::Location end) {
    #####:  228:  return std::any_of(begin, end, [](char b) { return b == '\n' || b == '\r'; });
        -:  229:}
        -:  230:
        -:  231:// Class Reader
        -:  232:// //////////////////////////////////////////////////////////////////
        -:  233:
    #####:  234:Reader::Reader() : features_(Features::all()) {}
        -:  235:
    #####:  236:Reader::Reader(const Features& features) : features_(features) {}
        -:  237:
    #####:  238:bool Reader::parse(const std::string& document, Value& root,
        -:  239:                   bool collectComments) {
    #####:  240:  document_.assign(document.begin(), document.end());
    #####:  241:  const char* begin = document_.c_str();
    #####:  242:  const char* end = begin + document_.length();
    #####:  243:  return parse(begin, end, root, collectComments);
        -:  244:}
        -:  245:
    #####:  246:bool Reader::parse(std::istream& is, Value& root, bool collectComments) {
        -:  247:  // std::istream_iterator<char> begin(is);
        -:  248:  // std::istream_iterator<char> end;
        -:  249:  // Those would allow streamed input from a file, if parse() were a
        -:  250:  // template function.
        -:  251:
        -:  252:  // Since String is reference-counted, this at least does not
        -:  253:  // create an extra copy.
    #####:  254:  String doc;
    #####:  255:  std::getline(is, doc, static_cast<char> EOF);
    #####:  256:  return parse(doc.data(), doc.data() + doc.size(), root, collectComments);
        -:  257:}
        -:  258:
    #####:  259:bool Reader::parse(const char* beginDoc, const char* endDoc, Value& root,
        -:  260:                   bool collectComments) {
    #####:  261:  if (!features_.allowComments_) {
    #####:  262:    collectComments = false;
        -:  263:  }
        -:  264:
    #####:  265:  begin_ = beginDoc;
    #####:  266:  end_ = endDoc;
    #####:  267:  collectComments_ = collectComments;
    #####:  268:  current_ = begin_;
    #####:  269:  lastValueEnd_ = nullptr;
    #####:  270:  lastValue_ = nullptr;
    #####:  271:  commentsBefore_.clear();
    #####:  272:  errors_.clear();
    #####:  273:  while (!nodes_.empty())
    #####:  274:    nodes_.pop();
    #####:  275:  nodes_.push(&root);
        -:  276:
    #####:  277:  bool successful = readValue();
        -:  278:  Token token;
    #####:  279:  skipCommentTokens(token);
    #####:  280:  if (collectComments_ && !commentsBefore_.empty())
    #####:  281:    root.setComment(commentsBefore_, commentAfter);
    #####:  282:  if (features_.strictRoot_) {
    #####:  283:    if (!root.isArray() && !root.isObject()) {
        -:  284:      // Set error location to start of doc, ideally should be first token found
        -:  285:      // in doc
    #####:  286:      token.type_ = tokenError;
    #####:  287:      token.start_ = beginDoc;
    #####:  288:      token.end_ = endDoc;
    #####:  289:      addError(
        -:  290:          "A valid JSON document must be either an array or an object value.",
        -:  291:          token);
    #####:  292:      return false;
        -:  293:    }
        -:  294:  }
    #####:  295:  return successful;
        -:  296:}
        -:  297:
    #####:  298:bool Reader::readValue() {
        -:  299:  // readValue() may call itself only if it calls readObject() or ReadArray().
        -:  300:  // These methods execute nodes_.push() just before and nodes_.pop)() just
        -:  301:  // after calling readValue(). parse() executes one nodes_.push(), so > instead
        -:  302:  // of >=.
    #####:  303:  if (nodes_.size() > stackLimit_g)
    #####:  304:    throwRuntimeError("Exceeded stackLimit in readValue().");
        -:  305:
        -:  306:  Token token;
    #####:  307:  skipCommentTokens(token);
    #####:  308:  bool successful = true;
        -:  309:
    #####:  310:  if (collectComments_ && !commentsBefore_.empty()) {
    #####:  311:    currentValue().setComment(commentsBefore_, commentBefore);
    #####:  312:    commentsBefore_.clear();
        -:  313:  }
        -:  314:
    #####:  315:  switch (token.type_) {
    #####:  316:  case tokenObjectBegin:
    #####:  317:    successful = readObject(token);
    #####:  318:    currentValue().setOffsetLimit(current_ - begin_);
    #####:  319:    break;
    #####:  320:  case tokenArrayBegin:
    #####:  321:    successful = readArray(token);
    #####:  322:    currentValue().setOffsetLimit(current_ - begin_);
    #####:  323:    break;
    #####:  324:  case tokenNumber:
    #####:  325:    successful = decodeNumber(token);
    #####:  326:    break;
    #####:  327:  case tokenString:
    #####:  328:    successful = decodeString(token);
    #####:  329:    break;
    #####:  330:  case tokenTrue: {
    #####:  331:    Value v(true);
    #####:  332:    currentValue().swapPayload(v);
    #####:  333:    currentValue().setOffsetStart(token.start_ - begin_);
    #####:  334:    currentValue().setOffsetLimit(token.end_ - begin_);
    #####:  335:  } break;
    #####:  336:  case tokenFalse: {
    #####:  337:    Value v(false);
    #####:  338:    currentValue().swapPayload(v);
    #####:  339:    currentValue().setOffsetStart(token.start_ - begin_);
    #####:  340:    currentValue().setOffsetLimit(token.end_ - begin_);
    #####:  341:  } break;
    #####:  342:  case tokenNull: {
    #####:  343:    Value v;
    #####:  344:    currentValue().swapPayload(v);
    #####:  345:    currentValue().setOffsetStart(token.start_ - begin_);
    #####:  346:    currentValue().setOffsetLimit(token.end_ - begin_);
    #####:  347:  } break;
    #####:  348:  case tokenArraySeparator:
        -:  349:  case tokenObjectEnd:
        -:  350:  case tokenArrayEnd:
    #####:  351:    if (features_.allowDroppedNullPlaceholders_) {
        -:  352:      // "Un-read" the current token and mark the current value as a null
        -:  353:      // token.
    #####:  354:      current_--;
    #####:  355:      Value v;
    #####:  356:      currentValue().swapPayload(v);
    #####:  357:      currentValue().setOffsetStart(current_ - begin_ - 1);
    #####:  358:      currentValue().setOffsetLimit(current_ - begin_);
    #####:  359:      break;
        -:  360:    } // Else, fall through...
        -:  361:  default:
    #####:  362:    currentValue().setOffsetStart(token.start_ - begin_);
    #####:  363:    currentValue().setOffsetLimit(token.end_ - begin_);
    #####:  364:    return addError("Syntax error: value, object or array expected.", token);
        -:  365:  }
        -:  366:
    #####:  367:  if (collectComments_) {
    #####:  368:    lastValueEnd_ = current_;
    #####:  369:    lastValue_ = &currentValue();
        -:  370:  }
        -:  371:
    #####:  372:  return successful;
        -:  373:}
        -:  374:
    #####:  375:void Reader::skipCommentTokens(Token& token) {
    #####:  376:  if (features_.allowComments_) {
    #####:  377:    do {
    #####:  378:      readToken(token);
    #####:  379:    } while (token.type_ == tokenComment);
        -:  380:  } else {
    #####:  381:    readToken(token);
        -:  382:  }
    #####:  383:}
        -:  384:
    #####:  385:bool Reader::readToken(Token& token) {
    #####:  386:  skipSpaces();
    #####:  387:  token.start_ = current_;
    #####:  388:  Char c = getNextChar();
    #####:  389:  bool ok = true;
    #####:  390:  switch (c) {
    #####:  391:  case '{':
    #####:  392:    token.type_ = tokenObjectBegin;
    #####:  393:    break;
    #####:  394:  case '}':
    #####:  395:    token.type_ = tokenObjectEnd;
    #####:  396:    break;
    #####:  397:  case '[':
    #####:  398:    token.type_ = tokenArrayBegin;
    #####:  399:    break;
    #####:  400:  case ']':
    #####:  401:    token.type_ = tokenArrayEnd;
    #####:  402:    break;
    #####:  403:  case '"':
    #####:  404:    token.type_ = tokenString;
    #####:  405:    ok = readString();
    #####:  406:    break;
    #####:  407:  case '/':
    #####:  408:    token.type_ = tokenComment;
    #####:  409:    ok = readComment();
    #####:  410:    break;
    #####:  411:  case '0':
        -:  412:  case '1':
        -:  413:  case '2':
        -:  414:  case '3':
        -:  415:  case '4':
        -:  416:  case '5':
        -:  417:  case '6':
        -:  418:  case '7':
        -:  419:  case '8':
        -:  420:  case '9':
        -:  421:  case '-':
    #####:  422:    token.type_ = tokenNumber;
    #####:  423:    readNumber();
    #####:  424:    break;
    #####:  425:  case 't':
    #####:  426:    token.type_ = tokenTrue;
    #####:  427:    ok = match("rue", 3);
    #####:  428:    break;
    #####:  429:  case 'f':
    #####:  430:    token.type_ = tokenFalse;
    #####:  431:    ok = match("alse", 4);
    #####:  432:    break;
    #####:  433:  case 'n':
    #####:  434:    token.type_ = tokenNull;
    #####:  435:    ok = match("ull", 3);
    #####:  436:    break;
    #####:  437:  case ',':
    #####:  438:    token.type_ = tokenArraySeparator;
    #####:  439:    break;
    #####:  440:  case ':':
    #####:  441:    token.type_ = tokenMemberSeparator;
    #####:  442:    break;
    #####:  443:  case 0:
    #####:  444:    token.type_ = tokenEndOfStream;
    #####:  445:    break;
    #####:  446:  default:
    #####:  447:    ok = false;
    #####:  448:    break;
        -:  449:  }
    #####:  450:  if (!ok)
    #####:  451:    token.type_ = tokenError;
    #####:  452:  token.end_ = current_;
    #####:  453:  return ok;
        -:  454:}
        -:  455:
    #####:  456:void Reader::skipSpaces() {
    #####:  457:  while (current_ != end_) {
    #####:  458:    Char c = *current_;
    #####:  459:    if (c == ' ' || c == '\t' || c == '\r' || c == '\n')
    #####:  460:      ++current_;
        -:  461:    else
        -:  462:      break;
        -:  463:  }
    #####:  464:}
        -:  465:
    #####:  466:bool Reader::match(const Char* pattern, int patternLength) {
    #####:  467:  if (end_ - current_ < patternLength)
    #####:  468:    return false;
    #####:  469:  int index = patternLength;
    #####:  470:  while (index--)
    #####:  471:    if (current_[index] != pattern[index])
    #####:  472:      return false;
    #####:  473:  current_ += patternLength;
    #####:  474:  return true;
        -:  475:}
        -:  476:
    #####:  477:bool Reader::readComment() {
    #####:  478:  Location commentBegin = current_ - 1;
    #####:  479:  Char c = getNextChar();
    #####:  480:  bool successful = false;
    #####:  481:  if (c == '*')
    #####:  482:    successful = readCStyleComment();
    #####:  483:  else if (c == '/')
    #####:  484:    successful = readCppStyleComment();
    #####:  485:  if (!successful)
    #####:  486:    return false;
        -:  487:
    #####:  488:  if (collectComments_) {
    #####:  489:    CommentPlacement placement = commentBefore;
    #####:  490:    if (lastValueEnd_ && !containsNewLine(lastValueEnd_, commentBegin)) {
    #####:  491:      if (c != '*' || !containsNewLine(commentBegin, current_))
    #####:  492:        placement = commentAfterOnSameLine;
        -:  493:    }
        -:  494:
    #####:  495:    addComment(commentBegin, current_, placement);
        -:  496:  }
    #####:  497:  return true;
        -:  498:}
        -:  499:
    #####:  500:String Reader::normalizeEOL(Reader::Location begin, Reader::Location end) {
    #####:  501:  String normalized;
    #####:  502:  normalized.reserve(static_cast<size_t>(end - begin));
    #####:  503:  Reader::Location current = begin;
    #####:  504:  while (current != end) {
    #####:  505:    char c = *current++;
    #####:  506:    if (c == '\r') {
    #####:  507:      if (current != end && *current == '\n')
        -:  508:        // convert dos EOL
    #####:  509:        ++current;
        -:  510:      // convert Mac EOL
    #####:  511:      normalized += '\n';
        -:  512:    } else {
    #####:  513:      normalized += c;
        -:  514:    }
        -:  515:  }
    #####:  516:  return normalized;
        -:  517:}
        -:  518:
    #####:  519:void Reader::addComment(Location begin, Location end,
        -:  520:                        CommentPlacement placement) {
    #####:  521:  assert(collectComments_);
    #####:  522:  const String& normalized = normalizeEOL(begin, end);
    #####:  523:  if (placement == commentAfterOnSameLine) {
    #####:  524:    assert(lastValue_ != nullptr);
    #####:  525:    lastValue_->setComment(normalized, placement);
        -:  526:  } else {
    #####:  527:    commentsBefore_ += normalized;
        -:  528:  }
    #####:  529:}
        -:  530:
    #####:  531:bool Reader::readCStyleComment() {
    #####:  532:  while ((current_ + 1) < end_) {
    #####:  533:    Char c = getNextChar();
    #####:  534:    if (c == '*' && *current_ == '/')
    #####:  535:      break;
        -:  536:  }
    #####:  537:  return getNextChar() == '/';
        -:  538:}
        -:  539:
    #####:  540:bool Reader::readCppStyleComment() {
    #####:  541:  while (current_ != end_) {
    #####:  542:    Char c = getNextChar();
    #####:  543:    if (c == '\n')
    #####:  544:      break;
    #####:  545:    if (c == '\r') {
        -:  546:      // Consume DOS EOL. It will be normalized in addComment.
    #####:  547:      if (current_ != end_ && *current_ == '\n')
    #####:  548:        getNextChar();
        -:  549:      // Break on Moc OS 9 EOL.
    #####:  550:      break;
        -:  551:    }
        -:  552:  }
    #####:  553:  return true;
        -:  554:}
        -:  555:
    #####:  556:void Reader::readNumber() {
    #####:  557:  Location p = current_;
    #####:  558:  char c = '0'; // stopgap for already consumed character
        -:  559:  // integral part
    #####:  560:  while (c >= '0' && c <= '9')
    #####:  561:    c = (current_ = p) < end_ ? *p++ : '\0';
        -:  562:  // fractional part
    #####:  563:  if (c == '.') {
    #####:  564:    c = (current_ = p) < end_ ? *p++ : '\0';
    #####:  565:    while (c >= '0' && c <= '9')
    #####:  566:      c = (current_ = p) < end_ ? *p++ : '\0';
        -:  567:  }
        -:  568:  // exponential part
    #####:  569:  if (c == 'e' || c == 'E') {
    #####:  570:    c = (current_ = p) < end_ ? *p++ : '\0';
    #####:  571:    if (c == '+' || c == '-')
    #####:  572:      c = (current_ = p) < end_ ? *p++ : '\0';
    #####:  573:    while (c >= '0' && c <= '9')
    #####:  574:      c = (current_ = p) < end_ ? *p++ : '\0';
        -:  575:  }
    #####:  576:}
        -:  577:
    #####:  578:bool Reader::readString() {
    #####:  579:  Char c = '\0';
    #####:  580:  while (current_ != end_) {
    #####:  581:    c = getNextChar();
    #####:  582:    if (c == '\\')
    #####:  583:      getNextChar();
    #####:  584:    else if (c == '"')
    #####:  585:      break;
        -:  586:  }
    #####:  587:  return c == '"';
        -:  588:}
        -:  589:
    #####:  590:bool Reader::readObject(Token& token) {
        -:  591:  Token tokenName;
    #####:  592:  String name;
    #####:  593:  Value init(objectValue);
    #####:  594:  currentValue().swapPayload(init);
    #####:  595:  currentValue().setOffsetStart(token.start_ - begin_);
    #####:  596:  while (readToken(tokenName)) {
    #####:  597:    bool initialTokenOk = true;
    #####:  598:    while (tokenName.type_ == tokenComment && initialTokenOk)
    #####:  599:      initialTokenOk = readToken(tokenName);
    #####:  600:    if (!initialTokenOk)
    #####:  601:      break;
    #####:  602:    if (tokenName.type_ == tokenObjectEnd && name.empty()) // empty object
    #####:  603:      return true;
    #####:  604:    name.clear();
    #####:  605:    if (tokenName.type_ == tokenString) {
    #####:  606:      if (!decodeString(tokenName, name))
    #####:  607:        return recoverFromError(tokenObjectEnd);
    #####:  608:    } else if (tokenName.type_ == tokenNumber && features_.allowNumericKeys_) {
    #####:  609:      Value numberName;
    #####:  610:      if (!decodeNumber(tokenName, numberName))
    #####:  611:        return recoverFromError(tokenObjectEnd);
    #####:  612:      name = numberName.asString();
        -:  613:    } else {
        -:  614:      break;
        -:  615:    }
        -:  616:
        -:  617:    Token colon;
    #####:  618:    if (!readToken(colon) || colon.type_ != tokenMemberSeparator) {
    #####:  619:      return addErrorAndRecover("Missing ':' after object member name", colon,
    #####:  620:                                tokenObjectEnd);
        -:  621:    }
    #####:  622:    Value& value = currentValue()[name];
    #####:  623:    nodes_.push(&value);
    #####:  624:    bool ok = readValue();
    #####:  625:    nodes_.pop();
    #####:  626:    if (!ok) // error already set
    #####:  627:      return recoverFromError(tokenObjectEnd);
        -:  628:
        -:  629:    Token comma;
    #####:  630:    if (!readToken(comma) ||
    #####:  631:        (comma.type_ != tokenObjectEnd && comma.type_ != tokenArraySeparator &&
    #####:  632:         comma.type_ != tokenComment)) {
    #####:  633:      return addErrorAndRecover("Missing ',' or '}' in object declaration",
    #####:  634:                                comma, tokenObjectEnd);
        -:  635:    }
    #####:  636:    bool finalizeTokenOk = true;
    #####:  637:    while (comma.type_ == tokenComment && finalizeTokenOk)
    #####:  638:      finalizeTokenOk = readToken(comma);
    #####:  639:    if (comma.type_ == tokenObjectEnd)
    #####:  640:      return true;
        -:  641:  }
    #####:  642:  return addErrorAndRecover("Missing '}' or object member name", tokenName,
    #####:  643:                            tokenObjectEnd);
        -:  644:}
        -:  645:
    #####:  646:bool Reader::readArray(Token& token) {
    #####:  647:  Value init(arrayValue);
    #####:  648:  currentValue().swapPayload(init);
    #####:  649:  currentValue().setOffsetStart(token.start_ - begin_);
    #####:  650:  skipSpaces();
    #####:  651:  if (current_ != end_ && *current_ == ']') // empty array
        -:  652:  {
        -:  653:    Token endArray;
    #####:  654:    readToken(endArray);
    #####:  655:    return true;
        -:  656:  }
    #####:  657:  int index = 0;
        -:  658:  for (;;) {
    #####:  659:    Value& value = currentValue()[index++];
    #####:  660:    nodes_.push(&value);
    #####:  661:    bool ok = readValue();
    #####:  662:    nodes_.pop();
    #####:  663:    if (!ok) // error already set
    #####:  664:      return recoverFromError(tokenArrayEnd);
        -:  665:
        -:  666:    Token currentToken;
        -:  667:    // Accept Comment after last item in the array.
    #####:  668:    ok = readToken(currentToken);
    #####:  669:    while (currentToken.type_ == tokenComment && ok) {
    #####:  670:      ok = readToken(currentToken);
        -:  671:    }
    #####:  672:    bool badTokenType = (currentToken.type_ != tokenArraySeparator &&
    #####:  673:                         currentToken.type_ != tokenArrayEnd);
    #####:  674:    if (!ok || badTokenType) {
    #####:  675:      return addErrorAndRecover("Missing ',' or ']' in array declaration",
    #####:  676:                                currentToken, tokenArrayEnd);
        -:  677:    }
    #####:  678:    if (currentToken.type_ == tokenArrayEnd)
    #####:  679:      break;
    #####:  680:  }
    #####:  681:  return true;
        -:  682:}
        -:  683:
    #####:  684:bool Reader::decodeNumber(Token& token) {
    #####:  685:  Value decoded;
    #####:  686:  if (!decodeNumber(token, decoded))
    #####:  687:    return false;
    #####:  688:  currentValue().swapPayload(decoded);
    #####:  689:  currentValue().setOffsetStart(token.start_ - begin_);
    #####:  690:  currentValue().setOffsetLimit(token.end_ - begin_);
    #####:  691:  return true;
        -:  692:}
        -:  693:
    #####:  694:bool Reader::decodeNumber(Token& token, Value& decoded) {
        -:  695:  // Attempts to parse the number as an integer. If the number is
        -:  696:  // larger than the maximum supported value of an integer then
        -:  697:  // we decode the number as a double.
    #####:  698:  Location current = token.start_;
    #####:  699:  bool isNegative = *current == '-';
    #####:  700:  if (isNegative)
    #####:  701:    ++current;
        -:  702:  // TODO: Help the compiler do the div and mod at compile time or get rid of
        -:  703:  // them.
    #####:  704:  Value::LargestUInt maxIntegerValue =
    #####:  705:      isNegative ? Value::LargestUInt(Value::maxLargestInt) + 1
        -:  706:                 : Value::maxLargestUInt;
    #####:  707:  Value::LargestUInt threshold = maxIntegerValue / 10;
    #####:  708:  Value::LargestUInt value = 0;
    #####:  709:  while (current < token.end_) {
    #####:  710:    Char c = *current++;
    #####:  711:    if (c < '0' || c > '9')
    #####:  712:      return decodeDouble(token, decoded);
    #####:  713:    auto digit(static_cast<Value::UInt>(c - '0'));
    #####:  714:    if (value >= threshold) {
        -:  715:      // We've hit or exceeded the max value divided by 10 (rounded down). If
        -:  716:      // a) we've only just touched the limit, b) this is the last digit, and
        -:  717:      // c) it's small enough to fit in that rounding delta, we're okay.
        -:  718:      // Otherwise treat this number as a double to avoid overflow.
    #####:  719:      if (value > threshold || current != token.end_ ||
    #####:  720:          digit > maxIntegerValue % 10) {
    #####:  721:        return decodeDouble(token, decoded);
        -:  722:      }
        -:  723:    }
    #####:  724:    value = value * 10 + digit;
        -:  725:  }
    #####:  726:  if (isNegative && value == maxIntegerValue)
    #####:  727:    decoded = Value::minLargestInt;
    #####:  728:  else if (isNegative)
    #####:  729:    decoded = -Value::LargestInt(value);
    #####:  730:  else if (value <= Value::LargestUInt(Value::maxInt))
    #####:  731:    decoded = Value::LargestInt(value);
        -:  732:  else
    #####:  733:    decoded = value;
    #####:  734:  return true;
        -:  735:}
        -:  736:
    #####:  737:bool Reader::decodeDouble(Token& token) {
    #####:  738:  Value decoded;
    #####:  739:  if (!decodeDouble(token, decoded))
    #####:  740:    return false;
    #####:  741:  currentValue().swapPayload(decoded);
    #####:  742:  currentValue().setOffsetStart(token.start_ - begin_);
    #####:  743:  currentValue().setOffsetLimit(token.end_ - begin_);
    #####:  744:  return true;
        -:  745:}
        -:  746:
    #####:  747:bool Reader::decodeDouble(Token& token, Value& decoded) {
    #####:  748:  double value = 0;
    #####:  749:  String buffer(token.start_, token.end_);
    #####:  750:  IStringStream is(buffer);
    #####:  751:  if (!(is >> value))
    #####:  752:    return addError(
    #####:  753:        "'" + String(token.start_, token.end_) + "' is not a number.", token);
    #####:  754:  decoded = value;
    #####:  755:  return true;
        -:  756:}
        -:  757:
    #####:  758:bool Reader::decodeString(Token& token) {
    #####:  759:  String decoded_string;
    #####:  760:  if (!decodeString(token, decoded_string))
    #####:  761:    return false;
    #####:  762:  Value decoded(decoded_string);
    #####:  763:  currentValue().swapPayload(decoded);
    #####:  764:  currentValue().setOffsetStart(token.start_ - begin_);
    #####:  765:  currentValue().setOffsetLimit(token.end_ - begin_);
    #####:  766:  return true;
        -:  767:}
        -:  768:
    #####:  769:bool Reader::decodeString(Token& token, String& decoded) {
    #####:  770:  decoded.reserve(static_cast<size_t>(token.end_ - token.start_ - 2));
    #####:  771:  Location current = token.start_ + 1; // skip '"'
    #####:  772:  Location end = token.end_ - 1;       // do not include '"'
    #####:  773:  while (current != end) {
    #####:  774:    Char c = *current++;
    #####:  775:    if (c == '"')
    #####:  776:      break;
    #####:  777:    if (c == '\\') {
    #####:  778:      if (current == end)
    #####:  779:        return addError("Empty escape sequence in string", token, current);
    #####:  780:      Char escape = *current++;
    #####:  781:      switch (escape) {
    #####:  782:      case '"':
    #####:  783:        decoded += '"';
    #####:  784:        break;
    #####:  785:      case '/':
    #####:  786:        decoded += '/';
    #####:  787:        break;
    #####:  788:      case '\\':
    #####:  789:        decoded += '\\';
    #####:  790:        break;
    #####:  791:      case 'b':
    #####:  792:        decoded += '\b';
    #####:  793:        break;
    #####:  794:      case 'f':
    #####:  795:        decoded += '\f';
    #####:  796:        break;
    #####:  797:      case 'n':
    #####:  798:        decoded += '\n';
    #####:  799:        break;
    #####:  800:      case 'r':
    #####:  801:        decoded += '\r';
    #####:  802:        break;
    #####:  803:      case 't':
    #####:  804:        decoded += '\t';
    #####:  805:        break;
    #####:  806:      case 'u': {
        -:  807:        unsigned int unicode;
    #####:  808:        if (!decodeUnicodeCodePoint(token, current, end, unicode))
    #####:  809:          return false;
    #####:  810:        decoded += codePointToUTF8(unicode);
    #####:  811:      } break;
    #####:  812:      default:
    #####:  813:        return addError("Bad escape sequence in string", token, current);
        -:  814:      }
        -:  815:    } else {
    #####:  816:      decoded += c;
        -:  817:    }
        -:  818:  }
    #####:  819:  return true;
        -:  820:}
        -:  821:
    #####:  822:bool Reader::decodeUnicodeCodePoint(Token& token, Location& current,
        -:  823:                                    Location end, unsigned int& unicode) {
        -:  824:
    #####:  825:  if (!decodeUnicodeEscapeSequence(token, current, end, unicode))
    #####:  826:    return false;
    #####:  827:  if (unicode >= 0xD800 && unicode <= 0xDBFF) {
        -:  828:    // surrogate pairs
    #####:  829:    if (end - current < 6)
    #####:  830:      return addError(
        -:  831:          "additional six characters expected to parse unicode surrogate pair.",
    #####:  832:          token, current);
    #####:  833:    if (*(current++) == '\\' && *(current++) == 'u') {
        -:  834:      unsigned int surrogatePair;
    #####:  835:      if (decodeUnicodeEscapeSequence(token, current, end, surrogatePair)) {
    #####:  836:        unicode = 0x10000 + ((unicode & 0x3FF) << 10) + (surrogatePair & 0x3FF);
        -:  837:      } else
    #####:  838:        return false;
        -:  839:    } else
    #####:  840:      return addError("expecting another \\u token to begin the second half of "
        -:  841:                      "a unicode surrogate pair",
    #####:  842:                      token, current);
        -:  843:  }
    #####:  844:  return true;
        -:  845:}
        -:  846:
    #####:  847:bool Reader::decodeUnicodeEscapeSequence(Token& token, Location& current,
        -:  848:                                         Location end,
        -:  849:                                         unsigned int& ret_unicode) {
    #####:  850:  if (end - current < 4)
    #####:  851:    return addError(
        -:  852:        "Bad unicode escape sequence in string: four digits expected.", token,
    #####:  853:        current);
    #####:  854:  int unicode = 0;
    #####:  855:  for (int index = 0; index < 4; ++index) {
    #####:  856:    Char c = *current++;
    #####:  857:    unicode *= 16;
    #####:  858:    if (c >= '0' && c <= '9')
    #####:  859:      unicode += c - '0';
    #####:  860:    else if (c >= 'a' && c <= 'f')
    #####:  861:      unicode += c - 'a' + 10;
    #####:  862:    else if (c >= 'A' && c <= 'F')
    #####:  863:      unicode += c - 'A' + 10;
        -:  864:    else
    #####:  865:      return addError(
        -:  866:          "Bad unicode escape sequence in string: hexadecimal digit expected.",
    #####:  867:          token, current);
        -:  868:  }
    #####:  869:  ret_unicode = static_cast<unsigned int>(unicode);
    #####:  870:  return true;
        -:  871:}
        -:  872:
    #####:  873:bool Reader::addError(const String& message, Token& token, Location extra) {
    #####:  874:  ErrorInfo info;
    #####:  875:  info.token_ = token;
    #####:  876:  info.message_ = message;
    #####:  877:  info.extra_ = extra;
    #####:  878:  errors_.push_back(info);
    #####:  879:  return false;
        -:  880:}
        -:  881:
    #####:  882:bool Reader::recoverFromError(TokenType skipUntilToken) {
    #####:  883:  size_t const errorCount = errors_.size();
        -:  884:  Token skip;
        -:  885:  for (;;) {
    #####:  886:    if (!readToken(skip))
    #####:  887:      errors_.resize(errorCount); // discard errors caused by recovery
    #####:  888:    if (skip.type_ == skipUntilToken || skip.type_ == tokenEndOfStream)
        -:  889:      break;
        -:  890:  }
    #####:  891:  errors_.resize(errorCount);
    #####:  892:  return false;
        -:  893:}
        -:  894:
    #####:  895:bool Reader::addErrorAndRecover(const String& message, Token& token,
        -:  896:                                TokenType skipUntilToken) {
    #####:  897:  addError(message, token);
    #####:  898:  return recoverFromError(skipUntilToken);
        -:  899:}
        -:  900:
    #####:  901:Value& Reader::currentValue() { return *(nodes_.top()); }
        -:  902:
    #####:  903:Reader::Char Reader::getNextChar() {
    #####:  904:  if (current_ == end_)
    #####:  905:    return 0;
    #####:  906:  return *current_++;
        -:  907:}
        -:  908:
    #####:  909:void Reader::getLocationLineAndColumn(Location location, int& line,
        -:  910:                                      int& column) const {
    #####:  911:  Location current = begin_;
    #####:  912:  Location lastLineStart = current;
    #####:  913:  line = 0;
    #####:  914:  while (current < location && current != end_) {
    #####:  915:    Char c = *current++;
    #####:  916:    if (c == '\r') {
    #####:  917:      if (*current == '\n')
    #####:  918:        ++current;
    #####:  919:      lastLineStart = current;
    #####:  920:      ++line;
    #####:  921:    } else if (c == '\n') {
    #####:  922:      lastLineStart = current;
    #####:  923:      ++line;
        -:  924:    }
        -:  925:  }
        -:  926:  // column & line start at 1
    #####:  927:  column = int(location - lastLineStart) + 1;
    #####:  928:  ++line;
    #####:  929:}
        -:  930:
    #####:  931:String Reader::getLocationLineAndColumn(Location location) const {
        -:  932:  int line, column;
    #####:  933:  getLocationLineAndColumn(location, line, column);
        -:  934:  char buffer[18 + 16 + 16 + 1];
    #####:  935:  jsoncpp_snprintf(buffer, sizeof(buffer), "Line %d, Column %d", line, column);
    #####:  936:  return buffer;
        -:  937:}
        -:  938:
        -:  939:// Deprecated. Preserved for backward compatibility
    #####:  940:String Reader::getFormatedErrorMessages() const {
    #####:  941:  return getFormattedErrorMessages();
        -:  942:}
        -:  943:
    #####:  944:String Reader::getFormattedErrorMessages() const {
    #####:  945:  String formattedMessage;
    #####:  946:  for (const auto& error : errors_) {
        -:  947:    formattedMessage +=
    #####:  948:        "* " + getLocationLineAndColumn(error.token_.start_) + "\n";
    #####:  949:    formattedMessage += "  " + error.message_ + "\n";
    #####:  950:    if (error.extra_)
        -:  951:      formattedMessage +=
    #####:  952:          "See " + getLocationLineAndColumn(error.extra_) + " for detail.\n";
        -:  953:  }
    #####:  954:  return formattedMessage;
        -:  955:}
        -:  956:
    #####:  957:std::vector<Reader::StructuredError> Reader::getStructuredErrors() const {
    #####:  958:  std::vector<Reader::StructuredError> allErrors;
    #####:  959:  for (const auto& error : errors_) {
    #####:  960:    Reader::StructuredError structured;
    #####:  961:    structured.offset_start = error.token_.start_ - begin_;
    #####:  962:    structured.offset_limit = error.token_.end_ - begin_;
    #####:  963:    structured.message = error.message_;
    #####:  964:    allErrors.push_back(structured);
        -:  965:  }
    #####:  966:  return allErrors;
        -:  967:}
        -:  968:
    #####:  969:bool Reader::pushError(const Value& value, const String& message) {
    #####:  970:  ptrdiff_t const length = end_ - begin_;
    #####:  971:  if (value.getOffsetStart() > length || value.getOffsetLimit() > length)
    #####:  972:    return false;
        -:  973:  Token token;
    #####:  974:  token.type_ = tokenError;
    #####:  975:  token.start_ = begin_ + value.getOffsetStart();
    #####:  976:  token.end_ = begin_ + value.getOffsetLimit();
    #####:  977:  ErrorInfo info;
    #####:  978:  info.token_ = token;
    #####:  979:  info.message_ = message;
    #####:  980:  info.extra_ = nullptr;
    #####:  981:  errors_.push_back(info);
    #####:  982:  return true;
        -:  983:}
        -:  984:
    #####:  985:bool Reader::pushError(const Value& value, const String& message,
        -:  986:                       const Value& extra) {
    #####:  987:  ptrdiff_t const length = end_ - begin_;
    #####:  988:  if (value.getOffsetStart() > length || value.getOffsetLimit() > length ||
    #####:  989:      extra.getOffsetLimit() > length)
    #####:  990:    return false;
        -:  991:  Token token;
    #####:  992:  token.type_ = tokenError;
    #####:  993:  token.start_ = begin_ + value.getOffsetStart();
    #####:  994:  token.end_ = begin_ + value.getOffsetLimit();
    #####:  995:  ErrorInfo info;
    #####:  996:  info.token_ = token;
    #####:  997:  info.message_ = message;
    #####:  998:  info.extra_ = begin_ + extra.getOffsetStart();
    #####:  999:  errors_.push_back(info);
    #####: 1000:  return true;
        -: 1001:}
        -: 1002:
    #####: 1003:bool Reader::good() const { return errors_.empty(); }
        -: 1004:
        -: 1005:// Originally copied from the Features class (now deprecated), used internally
        -: 1006:// for features implementation.
        -: 1007:class OurFeatures {
        -: 1008:public:
        -: 1009:  static OurFeatures all();
        -: 1010:  bool allowComments_;
        -: 1011:  bool allowTrailingCommas_;
        -: 1012:  bool strictRoot_;
        -: 1013:  bool allowDroppedNullPlaceholders_;
        -: 1014:  bool allowNumericKeys_;
        -: 1015:  bool allowSingleQuotes_;
        -: 1016:  bool failIfExtra_;
        -: 1017:  bool rejectDupKeys_;
        -: 1018:  bool allowSpecialFloats_;
        -: 1019:  bool skipBom_;
        -: 1020:  size_t stackLimit_;
        -: 1021:}; // OurFeatures
        -: 1022:
        5: 1023:OurFeatures OurFeatures::all() { return {}; }
        -: 1024:
        -: 1025:// Implementation of class Reader
        -: 1026:// ////////////////////////////////
        -: 1027:
        -: 1028:// Originally copied from the Reader class (now deprecated), used internally
        -: 1029:// for implementing JSON reading.
        -: 1030:class OurReader {
        -: 1031:public:
        -: 1032:  using Char = char;
        -: 1033:  using Location = const Char*;
        -: 1034:  struct StructuredError {
        -: 1035:    ptrdiff_t offset_start;
        -: 1036:    ptrdiff_t offset_limit;
        -: 1037:    String message;
        -: 1038:  };
        -: 1039:
        -: 1040:  explicit OurReader(OurFeatures const& features);
        -: 1041:  bool parse(const char* beginDoc, const char* endDoc, Value& root,
        -: 1042:             bool collectComments = true);
        -: 1043:  String getFormattedErrorMessages() const;
        -: 1044:  std::vector<StructuredError> getStructuredErrors() const;
        -: 1045:
        -: 1046:private:
        -: 1047:  OurReader(OurReader const&);      // no impl
        -: 1048:  void operator=(OurReader const&); // no impl
        -: 1049:
        -: 1050:  enum TokenType {
        -: 1051:    tokenEndOfStream = 0,
        -: 1052:    tokenObjectBegin,
        -: 1053:    tokenObjectEnd,
        -: 1054:    tokenArrayBegin,
        -: 1055:    tokenArrayEnd,
        -: 1056:    tokenString,
        -: 1057:    tokenNumber,
        -: 1058:    tokenTrue,
        -: 1059:    tokenFalse,
        -: 1060:    tokenNull,
        -: 1061:    tokenNaN,
        -: 1062:    tokenPosInf,
        -: 1063:    tokenNegInf,
        -: 1064:    tokenArraySeparator,
        -: 1065:    tokenMemberSeparator,
        -: 1066:    tokenComment,
        -: 1067:    tokenError
        -: 1068:  };
        -: 1069:
        -: 1070:  class Token {
        -: 1071:  public:
        -: 1072:    TokenType type_;
        -: 1073:    Location start_;
        -: 1074:    Location end_;
        -: 1075:  };
        -: 1076:
        -: 1077:  class ErrorInfo {
        -: 1078:  public:
        -: 1079:    Token token_;
        -: 1080:    String message_;
        -: 1081:    Location extra_;
        -: 1082:  };
        -: 1083:
        -: 1084:  using Errors = std::deque<ErrorInfo>;
        -: 1085:
        -: 1086:  bool readToken(Token& token);
        -: 1087:  void skipSpaces();
        -: 1088:  void skipBom(bool skipBom);
        -: 1089:  bool match(const Char* pattern, int patternLength);
        -: 1090:  bool readComment();
        -: 1091:  bool readCStyleComment(bool* containsNewLineResult);
        -: 1092:  bool readCppStyleComment();
        -: 1093:  bool readString();
        -: 1094:  bool readStringSingleQuote();
        -: 1095:  bool readNumber(bool checkInf);
        -: 1096:  bool readValue();
        -: 1097:  bool readObject(Token& token);
        -: 1098:  bool readArray(Token& token);
        -: 1099:  bool decodeNumber(Token& token);
        -: 1100:  bool decodeNumber(Token& token, Value& decoded);
        -: 1101:  bool decodeString(Token& token);
        -: 1102:  bool decodeString(Token& token, String& decoded);
        -: 1103:  bool decodeDouble(Token& token);
        -: 1104:  bool decodeDouble(Token& token, Value& decoded);
        -: 1105:  bool decodeUnicodeCodePoint(Token& token, Location& current, Location end,
        -: 1106:                              unsigned int& unicode);
        -: 1107:  bool decodeUnicodeEscapeSequence(Token& token, Location& current,
        -: 1108:                                   Location end, unsigned int& unicode);
        -: 1109:  bool addError(const String& message, Token& token, Location extra = nullptr);
        -: 1110:  bool recoverFromError(TokenType skipUntilToken);
        -: 1111:  bool addErrorAndRecover(const String& message, Token& token,
        -: 1112:                          TokenType skipUntilToken);
        -: 1113:  void skipUntilSpace();
        -: 1114:  Value& currentValue();
        -: 1115:  Char getNextChar();
        -: 1116:  void getLocationLineAndColumn(Location location, int& line,
        -: 1117:                                int& column) const;
        -: 1118:  String getLocationLineAndColumn(Location location) const;
        -: 1119:  void addComment(Location begin, Location end, CommentPlacement placement);
        -: 1120:  void skipCommentTokens(Token& token);
        -: 1121:
        -: 1122:  static String normalizeEOL(Location begin, Location end);
        -: 1123:  static bool containsNewLine(Location begin, Location end);
        -: 1124:
        -: 1125:  using Nodes = std::stack<Value*>;
        -: 1126:
        -: 1127:  Nodes nodes_{};
        -: 1128:  Errors errors_{};
        -: 1129:  String document_{};
        -: 1130:  Location begin_ = nullptr;
        -: 1131:  Location end_ = nullptr;
        -: 1132:  Location current_ = nullptr;
        -: 1133:  Location lastValueEnd_ = nullptr;
        -: 1134:  Value* lastValue_ = nullptr;
        -: 1135:  bool lastValueHasAComment_ = false;
        -: 1136:  String commentsBefore_{};
        -: 1137:
        -: 1138:  OurFeatures const features_;
        -: 1139:  bool collectComments_ = false;
        -: 1140:}; // OurReader
        -: 1141:
        -: 1142:// complete copy of Read impl, for OurReader
        -: 1143:
    #####: 1144:bool OurReader::containsNewLine(OurReader::Location begin,
        -: 1145:                                OurReader::Location end) {
    #####: 1146:  return std::any_of(begin, end, [](char b) { return b == '\n' || b == '\r'; });
        -: 1147:}
        -: 1148:
        5: 1149:OurReader::OurReader(OurFeatures const& features) : features_(features) {}
        -: 1150:
        5: 1151:bool OurReader::parse(const char* beginDoc, const char* endDoc, Value& root,
        -: 1152:                      bool collectComments) {
        5: 1153:  if (!features_.allowComments_) {
    #####: 1154:    collectComments = false;
        -: 1155:  }
        -: 1156:
        5: 1157:  begin_ = beginDoc;
        5: 1158:  end_ = endDoc;
        5: 1159:  collectComments_ = collectComments;
        5: 1160:  current_ = begin_;
        5: 1161:  lastValueEnd_ = nullptr;
        5: 1162:  lastValue_ = nullptr;
        5: 1163:  commentsBefore_.clear();
        5: 1164:  errors_.clear();
       5*: 1165:  while (!nodes_.empty())
    #####: 1166:    nodes_.pop();
        5: 1167:  nodes_.push(&root);
        -: 1168:
        -: 1169:  // skip byte order mark if it exists at the beginning of the UTF-8 text.
        5: 1170:  skipBom(features_.skipBom_);
        5: 1171:  bool successful = readValue();
        5: 1172:  nodes_.pop();
        -: 1173:  Token token;
        5: 1174:  skipCommentTokens(token);
       5*: 1175:  if (features_.failIfExtra_ && (token.type_ != tokenEndOfStream)) {
    #####: 1176:    addError("Extra non-whitespace after JSON value.", token);
    #####: 1177:    return false;
        -: 1178:  }
       5*: 1179:  if (collectComments_ && !commentsBefore_.empty())
    #####: 1180:    root.setComment(commentsBefore_, commentAfter);
        5: 1181:  if (features_.strictRoot_) {
    #####: 1182:    if (!root.isArray() && !root.isObject()) {
        -: 1183:      // Set error location to start of doc, ideally should be first token found
        -: 1184:      // in doc
    #####: 1185:      token.type_ = tokenError;
    #####: 1186:      token.start_ = beginDoc;
    #####: 1187:      token.end_ = endDoc;
    #####: 1188:      addError(
        -: 1189:          "A valid JSON document must be either an array or an object value.",
        -: 1190:          token);
    #####: 1191:      return false;
        -: 1192:    }
        -: 1193:  }
        5: 1194:  return successful;
        -: 1195:}
        -: 1196:
      467: 1197:bool OurReader::readValue() {
        -: 1198:  //  To preserve the old behaviour we cast size_t to int.
      467: 1199:  if (nodes_.size() > features_.stackLimit_)
    #####: 1200:    throwRuntimeError("Exceeded stackLimit in readValue().");
        -: 1201:  Token token;
      467: 1202:  skipCommentTokens(token);
      467: 1203:  bool successful = true;
        -: 1204:
     467*: 1205:  if (collectComments_ && !commentsBefore_.empty()) {
    #####: 1206:    currentValue().setComment(commentsBefore_, commentBefore);
    #####: 1207:    commentsBefore_.clear();
        -: 1208:  }
        -: 1209:
      467: 1210:  switch (token.type_) {
       99: 1211:  case tokenObjectBegin:
       99: 1212:    successful = readObject(token);
       99: 1213:    currentValue().setOffsetLimit(current_ - begin_);
       99: 1214:    break;
        5: 1215:  case tokenArrayBegin:
        5: 1216:    successful = readArray(token);
        5: 1217:    currentValue().setOffsetLimit(current_ - begin_);
        5: 1218:    break;
      264: 1219:  case tokenNumber:
      264: 1220:    successful = decodeNumber(token);
      264: 1221:    break;
       99: 1222:  case tokenString:
       99: 1223:    successful = decodeString(token);
       99: 1224:    break;
    #####: 1225:  case tokenTrue: {
    #####: 1226:    Value v(true);
    #####: 1227:    currentValue().swapPayload(v);
    #####: 1228:    currentValue().setOffsetStart(token.start_ - begin_);
    #####: 1229:    currentValue().setOffsetLimit(token.end_ - begin_);
    #####: 1230:  } break;
    #####: 1231:  case tokenFalse: {
    #####: 1232:    Value v(false);
    #####: 1233:    currentValue().swapPayload(v);
    #####: 1234:    currentValue().setOffsetStart(token.start_ - begin_);
    #####: 1235:    currentValue().setOffsetLimit(token.end_ - begin_);
    #####: 1236:  } break;
    #####: 1237:  case tokenNull: {
    #####: 1238:    Value v;
    #####: 1239:    currentValue().swapPayload(v);
    #####: 1240:    currentValue().setOffsetStart(token.start_ - begin_);
    #####: 1241:    currentValue().setOffsetLimit(token.end_ - begin_);
    #####: 1242:  } break;
    #####: 1243:  case tokenNaN: {
    #####: 1244:    Value v(std::numeric_limits<double>::quiet_NaN());
    #####: 1245:    currentValue().swapPayload(v);
    #####: 1246:    currentValue().setOffsetStart(token.start_ - begin_);
    #####: 1247:    currentValue().setOffsetLimit(token.end_ - begin_);
    #####: 1248:  } break;
    #####: 1249:  case tokenPosInf: {
    #####: 1250:    Value v(std::numeric_limits<double>::infinity());
    #####: 1251:    currentValue().swapPayload(v);
    #####: 1252:    currentValue().setOffsetStart(token.start_ - begin_);
    #####: 1253:    currentValue().setOffsetLimit(token.end_ - begin_);
    #####: 1254:  } break;
    #####: 1255:  case tokenNegInf: {
    #####: 1256:    Value v(-std::numeric_limits<double>::infinity());
    #####: 1257:    currentValue().swapPayload(v);
    #####: 1258:    currentValue().setOffsetStart(token.start_ - begin_);
    #####: 1259:    currentValue().setOffsetLimit(token.end_ - begin_);
    #####: 1260:  } break;
    #####: 1261:  case tokenArraySeparator:
        -: 1262:  case tokenObjectEnd:
        -: 1263:  case tokenArrayEnd:
    #####: 1264:    if (features_.allowDroppedNullPlaceholders_) {
        -: 1265:      // "Un-read" the current token and mark the current value as a null
        -: 1266:      // token.
    #####: 1267:      current_--;
    #####: 1268:      Value v;
    #####: 1269:      currentValue().swapPayload(v);
    #####: 1270:      currentValue().setOffsetStart(current_ - begin_ - 1);
    #####: 1271:      currentValue().setOffsetLimit(current_ - begin_);
    #####: 1272:      break;
        -: 1273:    } // else, fall through ...
        -: 1274:  default:
    #####: 1275:    currentValue().setOffsetStart(token.start_ - begin_);
    #####: 1276:    currentValue().setOffsetLimit(token.end_ - begin_);
    #####: 1277:    return addError("Syntax error: value, object or array expected.", token);
        -: 1278:  }
        -: 1279:
      467: 1280:  if (collectComments_) {
      467: 1281:    lastValueEnd_ = current_;
      467: 1282:    lastValueHasAComment_ = false;
      467: 1283:    lastValue_ = &currentValue();
        -: 1284:  }
        -: 1285:
      467: 1286:  return successful;
        -: 1287:}
        -: 1288:
      472: 1289:void OurReader::skipCommentTokens(Token& token) {
      472: 1290:  if (features_.allowComments_) {
    #####: 1291:    do {
      472: 1292:      readToken(token);
      472: 1293:    } while (token.type_ == tokenComment);
        -: 1294:  } else {
    #####: 1295:    readToken(token);
        -: 1296:  }
      472: 1297:}
        -: 1298:
     1792: 1299:bool OurReader::readToken(Token& token) {
     1792: 1300:  skipSpaces();
     1792: 1301:  token.start_ = current_;
     1792: 1302:  Char c = getNextChar();
     1792: 1303:  bool ok = true;
     1792: 1304:  switch (c) {
       99: 1305:  case '{':
       99: 1306:    token.type_ = tokenObjectBegin;
       99: 1307:    break;
       99: 1308:  case '}':
       99: 1309:    token.type_ = tokenObjectEnd;
       99: 1310:    break;
        5: 1311:  case '[':
        5: 1312:    token.type_ = tokenArrayBegin;
        5: 1313:    break;
        5: 1314:  case ']':
        5: 1315:    token.type_ = tokenArrayEnd;
        5: 1316:    break;
      528: 1317:  case '"':
      528: 1318:    token.type_ = tokenString;
      528: 1319:    ok = readString();
      528: 1320:    break;
    #####: 1321:  case '\'':
    #####: 1322:    if (features_.allowSingleQuotes_) {
    #####: 1323:      token.type_ = tokenString;
    #####: 1324:      ok = readStringSingleQuote();
        -: 1325:    } else {
        -: 1326:      // If we don't allow single quotes, this is a failure case.
    #####: 1327:      ok = false;
        -: 1328:    }
    #####: 1329:    break;
    #####: 1330:  case '/':
    #####: 1331:    token.type_ = tokenComment;
    #####: 1332:    ok = readComment();
    #####: 1333:    break;
      264: 1334:  case '0':
        -: 1335:  case '1':
        -: 1336:  case '2':
        -: 1337:  case '3':
        -: 1338:  case '4':
        -: 1339:  case '5':
        -: 1340:  case '6':
        -: 1341:  case '7':
        -: 1342:  case '8':
        -: 1343:  case '9':
      264: 1344:    token.type_ = tokenNumber;
      264: 1345:    readNumber(false);
      264: 1346:    break;
    #####: 1347:  case '-':
    #####: 1348:    if (readNumber(true)) {
    #####: 1349:      token.type_ = tokenNumber;
        -: 1350:    } else {
    #####: 1351:      token.type_ = tokenNegInf;
    #####: 1352:      ok = features_.allowSpecialFloats_ && match("nfinity", 7);
        -: 1353:    }
    #####: 1354:    break;
    #####: 1355:  case '+':
    #####: 1356:    if (readNumber(true)) {
    #####: 1357:      token.type_ = tokenNumber;
        -: 1358:    } else {
    #####: 1359:      token.type_ = tokenPosInf;
    #####: 1360:      ok = features_.allowSpecialFloats_ && match("nfinity", 7);
        -: 1361:    }
    #####: 1362:    break;
    #####: 1363:  case 't':
    #####: 1364:    token.type_ = tokenTrue;
    #####: 1365:    ok = match("rue", 3);
    #####: 1366:    break;
    #####: 1367:  case 'f':
    #####: 1368:    token.type_ = tokenFalse;
    #####: 1369:    ok = match("alse", 4);
    #####: 1370:    break;
    #####: 1371:  case 'n':
    #####: 1372:    token.type_ = tokenNull;
    #####: 1373:    ok = match("ull", 3);
    #####: 1374:    break;
    #####: 1375:  case 'N':
    #####: 1376:    if (features_.allowSpecialFloats_) {
    #####: 1377:      token.type_ = tokenNaN;
    #####: 1378:      ok = match("aN", 2);
        -: 1379:    } else {
    #####: 1380:      ok = false;
        -: 1381:    }
    #####: 1382:    break;
    #####: 1383:  case 'I':
    #####: 1384:    if (features_.allowSpecialFloats_) {
    #####: 1385:      token.type_ = tokenPosInf;
    #####: 1386:      ok = match("nfinity", 7);
        -: 1387:    } else {
    #####: 1388:      ok = false;
        -: 1389:    }
    #####: 1390:    break;
      358: 1391:  case ',':
      358: 1392:    token.type_ = tokenArraySeparator;
      358: 1393:    break;
      429: 1394:  case ':':
      429: 1395:    token.type_ = tokenMemberSeparator;
      429: 1396:    break;
        5: 1397:  case 0:
        5: 1398:    token.type_ = tokenEndOfStream;
        5: 1399:    break;
    #####: 1400:  default:
    #####: 1401:    ok = false;
    #####: 1402:    break;
        -: 1403:  }
     1792: 1404:  if (!ok)
    #####: 1405:    token.type_ = tokenError;
     1792: 1406:  token.end_ = current_;
     1792: 1407:  return ok;
        -: 1408:}
        -: 1409:
     4767: 1410:void OurReader::skipSpaces() {
     4767: 1411:  while (current_ != end_) {
     4762: 1412:    Char c = *current_;
     4762: 1413:    if (c == ' ' || c == '\t' || c == '\r' || c == '\n')
     2942: 1414:      ++current_;
        -: 1415:    else
        -: 1416:      break;
        -: 1417:  }
     1825: 1418:}
        -: 1419:
        5: 1420:void OurReader::skipBom(bool skipBom) {
        -: 1421:  // The default behavior is to skip BOM.
        5: 1422:  if (skipBom) {
        5: 1423:    if ((end_ - begin_) >= 3 && strncmp(begin_, "\xEF\xBB\xBF", 3) == 0) {
    #####: 1424:      begin_ += 3;
    #####: 1425:      current_ = begin_;
        -: 1426:    }
        -: 1427:  }
        5: 1428:}
        -: 1429:
    #####: 1430:bool OurReader::match(const Char* pattern, int patternLength) {
    #####: 1431:  if (end_ - current_ < patternLength)
    #####: 1432:    return false;
    #####: 1433:  int index = patternLength;
    #####: 1434:  while (index--)
    #####: 1435:    if (current_[index] != pattern[index])
    #####: 1436:      return false;
    #####: 1437:  current_ += patternLength;
    #####: 1438:  return true;
        -: 1439:}
        -: 1440:
    #####: 1441:bool OurReader::readComment() {
    #####: 1442:  const Location commentBegin = current_ - 1;
    #####: 1443:  const Char c = getNextChar();
    #####: 1444:  bool successful = false;
    #####: 1445:  bool cStyleWithEmbeddedNewline = false;
        -: 1446:
    #####: 1447:  const bool isCStyleComment = (c == '*');
    #####: 1448:  const bool isCppStyleComment = (c == '/');
    #####: 1449:  if (isCStyleComment) {
    #####: 1450:    successful = readCStyleComment(&cStyleWithEmbeddedNewline);
    #####: 1451:  } else if (isCppStyleComment) {
    #####: 1452:    successful = readCppStyleComment();
        -: 1453:  }
        -: 1454:
    #####: 1455:  if (!successful)
    #####: 1456:    return false;
        -: 1457:
    #####: 1458:  if (collectComments_) {
    #####: 1459:    CommentPlacement placement = commentBefore;
        -: 1460:
    #####: 1461:    if (!lastValueHasAComment_) {
    #####: 1462:      if (lastValueEnd_ && !containsNewLine(lastValueEnd_, commentBegin)) {
    #####: 1463:        if (isCppStyleComment || !cStyleWithEmbeddedNewline) {
    #####: 1464:          placement = commentAfterOnSameLine;
    #####: 1465:          lastValueHasAComment_ = true;
        -: 1466:        }
        -: 1467:      }
        -: 1468:    }
        -: 1469:
    #####: 1470:    addComment(commentBegin, current_, placement);
        -: 1471:  }
    #####: 1472:  return true;
        -: 1473:}
        -: 1474:
    #####: 1475:String OurReader::normalizeEOL(OurReader::Location begin,
        -: 1476:                               OurReader::Location end) {
    #####: 1477:  String normalized;
    #####: 1478:  normalized.reserve(static_cast<size_t>(end - begin));
    #####: 1479:  OurReader::Location current = begin;
    #####: 1480:  while (current != end) {
    #####: 1481:    char c = *current++;
    #####: 1482:    if (c == '\r') {
    #####: 1483:      if (current != end && *current == '\n')
        -: 1484:        // convert dos EOL
    #####: 1485:        ++current;
        -: 1486:      // convert Mac EOL
    #####: 1487:      normalized += '\n';
        -: 1488:    } else {
    #####: 1489:      normalized += c;
        -: 1490:    }
        -: 1491:  }
    #####: 1492:  return normalized;
        -: 1493:}
        -: 1494:
    #####: 1495:void OurReader::addComment(Location begin, Location end,
        -: 1496:                           CommentPlacement placement) {
    #####: 1497:  assert(collectComments_);
    #####: 1498:  const String& normalized = normalizeEOL(begin, end);
    #####: 1499:  if (placement == commentAfterOnSameLine) {
    #####: 1500:    assert(lastValue_ != nullptr);
    #####: 1501:    lastValue_->setComment(normalized, placement);
        -: 1502:  } else {
    #####: 1503:    commentsBefore_ += normalized;
        -: 1504:  }
    #####: 1505:}
        -: 1506:
    #####: 1507:bool OurReader::readCStyleComment(bool* containsNewLineResult) {
    #####: 1508:  *containsNewLineResult = false;
        -: 1509:
    #####: 1510:  while ((current_ + 1) < end_) {
    #####: 1511:    Char c = getNextChar();
    #####: 1512:    if (c == '*' && *current_ == '/')
    #####: 1513:      break;
    #####: 1514:    if (c == '\n')
    #####: 1515:      *containsNewLineResult = true;
        -: 1516:  }
        -: 1517:
    #####: 1518:  return getNextChar() == '/';
        -: 1519:}
        -: 1520:
    #####: 1521:bool OurReader::readCppStyleComment() {
    #####: 1522:  while (current_ != end_) {
    #####: 1523:    Char c = getNextChar();
    #####: 1524:    if (c == '\n')
    #####: 1525:      break;
    #####: 1526:    if (c == '\r') {
        -: 1527:      // Consume DOS EOL. It will be normalized in addComment.
    #####: 1528:      if (current_ != end_ && *current_ == '\n')
    #####: 1529:        getNextChar();
        -: 1530:      // Break on Moc OS 9 EOL.
    #####: 1531:      break;
        -: 1532:    }
        -: 1533:  }
    #####: 1534:  return true;
        -: 1535:}
        -: 1536:
      264: 1537:bool OurReader::readNumber(bool checkInf) {
      264: 1538:  Location p = current_;
     264*: 1539:  if (checkInf && p != end_ && *p == 'I') {
    #####: 1540:    current_ = ++p;
    #####: 1541:    return false;
        -: 1542:  }
      264: 1543:  char c = '0'; // stopgap for already consumed character
        -: 1544:  // integral part
      998: 1545:  while (c >= '0' && c <= '9')
     734*: 1546:    c = (current_ = p) < end_ ? *p++ : '\0';
        -: 1547:  // fractional part
      264: 1548:  if (c == '.') {
    #####: 1549:    c = (current_ = p) < end_ ? *p++ : '\0';
    #####: 1550:    while (c >= '0' && c <= '9')
    #####: 1551:      c = (current_ = p) < end_ ? *p++ : '\0';
        -: 1552:  }
        -: 1553:  // exponential part
      264: 1554:  if (c == 'e' || c == 'E') {
    #####: 1555:    c = (current_ = p) < end_ ? *p++ : '\0';
    #####: 1556:    if (c == '+' || c == '-')
    #####: 1557:      c = (current_ = p) < end_ ? *p++ : '\0';
    #####: 1558:    while (c >= '0' && c <= '9')
    #####: 1559:      c = (current_ = p) < end_ ? *p++ : '\0';
        -: 1560:  }
      264: 1561:  return true;
        -: 1562:}
      528: 1563:bool OurReader::readString() {
      528: 1564:  Char c = 0;
     3414: 1565:  while (current_ != end_) {
     3414: 1566:    c = getNextChar();
     3414: 1567:    if (c == '\\')
    #####: 1568:      getNextChar();
     3414: 1569:    else if (c == '"')
      528: 1570:      break;
        -: 1571:  }
      528: 1572:  return c == '"';
        -: 1573:}
        -: 1574:
    #####: 1575:bool OurReader::readStringSingleQuote() {
    #####: 1576:  Char c = 0;
    #####: 1577:  while (current_ != end_) {
    #####: 1578:    c = getNextChar();
    #####: 1579:    if (c == '\\')
    #####: 1580:      getNextChar();
    #####: 1581:    else if (c == '\'')
    #####: 1582:      break;
        -: 1583:  }
    #####: 1584:  return c == '\'';
        -: 1585:}
        -: 1586:
       99: 1587:bool OurReader::readObject(Token& token) {
        -: 1588:  Token tokenName;
      198: 1589:  String name;
      198: 1590:  Value init(objectValue);
       99: 1591:  currentValue().swapPayload(init);
       99: 1592:  currentValue().setOffsetStart(token.start_ - begin_);
      429: 1593:  while (readToken(tokenName)) {
      429: 1594:    bool initialTokenOk = true;
     429*: 1595:    while (tokenName.type_ == tokenComment && initialTokenOk)
    #####: 1596:      initialTokenOk = readToken(tokenName);
      429: 1597:    if (!initialTokenOk)
    #####: 1598:      break;
     429*: 1599:    if (tokenName.type_ == tokenObjectEnd &&
    #####: 1600:        (name.empty() ||
    #####: 1601:         features_.allowTrailingCommas_)) // empty object or trailing comma
      99*: 1602:      return true;
      429: 1603:    name.clear();
      429: 1604:    if (tokenName.type_ == tokenString) {
      429: 1605:      if (!decodeString(tokenName, name))
    #####: 1606:        return recoverFromError(tokenObjectEnd);
    #####: 1607:    } else if (tokenName.type_ == tokenNumber && features_.allowNumericKeys_) {
    #####: 1608:      Value numberName;
    #####: 1609:      if (!decodeNumber(tokenName, numberName))
    #####: 1610:        return recoverFromError(tokenObjectEnd);
    #####: 1611:      name = numberName.asString();
        -: 1612:    } else {
        -: 1613:      break;
        -: 1614:    }
      429: 1615:    if (name.length() >= (1U << 30))
    #####: 1616:      throwRuntimeError("keylength >= 2^30");
     429*: 1617:    if (features_.rejectDupKeys_ && currentValue().isMember(name)) {
    #####: 1618:      String msg = "Duplicate key: '" + name + "'";
    #####: 1619:      return addErrorAndRecover(msg, tokenName, tokenObjectEnd);
        -: 1620:    }
        -: 1621:
        -: 1622:    Token colon;
     429*: 1623:    if (!readToken(colon) || colon.type_ != tokenMemberSeparator) {
    #####: 1624:      return addErrorAndRecover("Missing ':' after object member name", colon,
    #####: 1625:                                tokenObjectEnd);
        -: 1626:    }
      429: 1627:    Value& value = currentValue()[name];
      429: 1628:    nodes_.push(&value);
      429: 1629:    bool ok = readValue();
      429: 1630:    nodes_.pop();
      429: 1631:    if (!ok) // error already set
    #####: 1632:      return recoverFromError(tokenObjectEnd);
        -: 1633:
        -: 1634:    Token comma;
     858*: 1635:    if (!readToken(comma) ||
     429*: 1636:        (comma.type_ != tokenObjectEnd && comma.type_ != tokenArraySeparator &&
    #####: 1637:         comma.type_ != tokenComment)) {
    #####: 1638:      return addErrorAndRecover("Missing ',' or '}' in object declaration",
    #####: 1639:                                comma, tokenObjectEnd);
        -: 1640:    }
      429: 1641:    bool finalizeTokenOk = true;
     429*: 1642:    while (comma.type_ == tokenComment && finalizeTokenOk)
    #####: 1643:      finalizeTokenOk = readToken(comma);
      429: 1644:    if (comma.type_ == tokenObjectEnd)
       99: 1645:      return true;
        -: 1646:  }
    #####: 1647:  return addErrorAndRecover("Missing '}' or object member name", tokenName,
    #####: 1648:                            tokenObjectEnd);
        -: 1649:}
        -: 1650:
        5: 1651:bool OurReader::readArray(Token& token) {
       10: 1652:  Value init(arrayValue);
        5: 1653:  currentValue().swapPayload(init);
        5: 1654:  currentValue().setOffsetStart(token.start_ - begin_);
        5: 1655:  int index = 0;
        -: 1656:  for (;;) {
       33: 1657:    skipSpaces();
      33*: 1658:    if (current_ != end_ && *current_ == ']' &&
    #####: 1659:        (index == 0 ||
    #####: 1660:         (features_.allowTrailingCommas_ &&
    #####: 1661:          !features_.allowDroppedNullPlaceholders_))) // empty array or trailing
        -: 1662:                                                      // comma
        -: 1663:    {
        -: 1664:      Token endArray;
    #####: 1665:      readToken(endArray);
    #####: 1666:      return true;
        -: 1667:    }
       33: 1668:    Value& value = currentValue()[index++];
       33: 1669:    nodes_.push(&value);
       33: 1670:    bool ok = readValue();
       33: 1671:    nodes_.pop();
       33: 1672:    if (!ok) // error already set
    #####: 1673:      return recoverFromError(tokenArrayEnd);
        -: 1674:
        -: 1675:    Token currentToken;
        -: 1676:    // Accept Comment after last item in the array.
       33: 1677:    ok = readToken(currentToken);
      33*: 1678:    while (currentToken.type_ == tokenComment && ok) {
    #####: 1679:      ok = readToken(currentToken);
        -: 1680:    }
      38*: 1681:    bool badTokenType = (currentToken.type_ != tokenArraySeparator &&
        5: 1682:                         currentToken.type_ != tokenArrayEnd);
       33: 1683:    if (!ok || badTokenType) {
    #####: 1684:      return addErrorAndRecover("Missing ',' or ']' in array declaration",
    #####: 1685:                                currentToken, tokenArrayEnd);
        -: 1686:    }
       33: 1687:    if (currentToken.type_ == tokenArrayEnd)
        5: 1688:      break;
       28: 1689:  }
        5: 1690:  return true;
        -: 1691:}
        -: 1692:
      264: 1693:bool OurReader::decodeNumber(Token& token) {
      528: 1694:  Value decoded;
      264: 1695:  if (!decodeNumber(token, decoded))
    #####: 1696:    return false;
      264: 1697:  currentValue().swapPayload(decoded);
      264: 1698:  currentValue().setOffsetStart(token.start_ - begin_);
      264: 1699:  currentValue().setOffsetLimit(token.end_ - begin_);
      264: 1700:  return true;
        -: 1701:}
        -: 1702:
      264: 1703:bool OurReader::decodeNumber(Token& token, Value& decoded) {
        -: 1704:  // Attempts to parse the number as an integer. If the number is
        -: 1705:  // larger than the maximum supported value of an integer then
        -: 1706:  // we decode the number as a double.
      264: 1707:  Location current = token.start_;
      264: 1708:  const bool isNegative = *current == '-';
      264: 1709:  if (isNegative) {
    #####: 1710:    ++current;
        -: 1711:  }
        -: 1712:
        -: 1713:  // We assume we can represent the largest and smallest integer types as
        -: 1714:  // unsigned integers with separate sign. This is only true if they can fit
        -: 1715:  // into an unsigned integer.
        -: 1716:  static_assert(Value::maxLargestInt <= Value::maxLargestUInt,
        -: 1717:                "Int must be smaller than UInt");
        -: 1718:
        -: 1719:  // We need to convert minLargestInt into a positive number. The easiest way
        -: 1720:  // to do this conversion is to assume our "threshold" value of minLargestInt
        -: 1721:  // divided by 10 can fit in maxLargestInt when absolute valued. This should
        -: 1722:  // be a safe assumption.
        -: 1723:  static_assert(Value::minLargestInt <= -Value::maxLargestInt,
        -: 1724:                "The absolute value of minLargestInt must be greater than or "
        -: 1725:                "equal to maxLargestInt");
        -: 1726:  static_assert(Value::minLargestInt / 10 >= -Value::maxLargestInt,
        -: 1727:                "The absolute value of minLargestInt must be only 1 magnitude "
        -: 1728:                "larger than maxLargest Int");
        -: 1729:
        -: 1730:  static constexpr Value::LargestUInt positive_threshold =
        -: 1731:      Value::maxLargestUInt / 10;
        -: 1732:  static constexpr Value::UInt positive_last_digit = Value::maxLargestUInt % 10;
        -: 1733:
        -: 1734:  // For the negative values, we have to be more careful. Since typically
        -: 1735:  // -Value::minLargestInt will cause an overflow, we first divide by 10 and
        -: 1736:  // then take the inverse. This assumes that minLargestInt is only a single
        -: 1737:  // power of 10 different in magnitude, which we check above. For the last
        -: 1738:  // digit, we take the modulus before negating for the same reason.
        -: 1739:  static constexpr auto negative_threshold =
        -: 1740:      Value::LargestUInt(-(Value::minLargestInt / 10));
        -: 1741:  static constexpr auto negative_last_digit =
        -: 1742:      Value::UInt(-(Value::minLargestInt % 10));
        -: 1743:
      264: 1744:  const Value::LargestUInt threshold =
     264*: 1745:      isNegative ? negative_threshold : positive_threshold;
      264: 1746:  const Value::UInt max_last_digit =
     264*: 1747:      isNegative ? negative_last_digit : positive_last_digit;
        -: 1748:
      264: 1749:  Value::LargestUInt value = 0;
      998: 1750:  while (current < token.end_) {
      734: 1751:    Char c = *current++;
      734: 1752:    if (c < '0' || c > '9')
    #####: 1753:      return decodeDouble(token, decoded);
        -: 1754:
      734: 1755:    const auto digit(static_cast<Value::UInt>(c - '0'));
      734: 1756:    if (value >= threshold) {
        -: 1757:      // We've hit or exceeded the max value divided by 10 (rounded down). If
        -: 1758:      // a) we've only just touched the limit, meaing value == threshold,
        -: 1759:      // b) this is the last digit, or
        -: 1760:      // c) it's small enough to fit in that rounding delta, we're okay.
        -: 1761:      // Otherwise treat this number as a double to avoid overflow.
    #####: 1762:      if (value > threshold || current != token.end_ ||
        -: 1763:          digit > max_last_digit) {
    #####: 1764:        return decodeDouble(token, decoded);
        -: 1765:      }
        -: 1766:    }
      734: 1767:    value = value * 10 + digit;
        -: 1768:  }
        -: 1769:
      264: 1770:  if (isNegative) {
        -: 1771:    // We use the same magnitude assumption here, just in case.
    #####: 1772:    const auto last_digit = static_cast<Value::UInt>(value % 10);
    #####: 1773:    decoded = -Value::LargestInt(value / 10) * 10 - last_digit;
      264: 1774:  } else if (value <= Value::LargestUInt(Value::maxLargestInt)) {
      264: 1775:    decoded = Value::LargestInt(value);
        -: 1776:  } else {
    #####: 1777:    decoded = value;
        -: 1778:  }
        -: 1779:
      264: 1780:  return true;
        -: 1781:}
        -: 1782:
    #####: 1783:bool OurReader::decodeDouble(Token& token) {
    #####: 1784:  Value decoded;
    #####: 1785:  if (!decodeDouble(token, decoded))
    #####: 1786:    return false;
    #####: 1787:  currentValue().swapPayload(decoded);
    #####: 1788:  currentValue().setOffsetStart(token.start_ - begin_);
    #####: 1789:  currentValue().setOffsetLimit(token.end_ - begin_);
    #####: 1790:  return true;
        -: 1791:}
        -: 1792:
    #####: 1793:bool OurReader::decodeDouble(Token& token, Value& decoded) {
    #####: 1794:  double value = 0;
    #####: 1795:  const String buffer(token.start_, token.end_);
    #####: 1796:  IStringStream is(buffer);
    #####: 1797:  if (!(is >> value)) {
    #####: 1798:    return addError(
    #####: 1799:        "'" + String(token.start_, token.end_) + "' is not a number.", token);
        -: 1800:  }
    #####: 1801:  decoded = value;
    #####: 1802:  return true;
        -: 1803:}
        -: 1804:
       99: 1805:bool OurReader::decodeString(Token& token) {
      198: 1806:  String decoded_string;
       99: 1807:  if (!decodeString(token, decoded_string))
    #####: 1808:    return false;
       99: 1809:  Value decoded(decoded_string);
       99: 1810:  currentValue().swapPayload(decoded);
       99: 1811:  currentValue().setOffsetStart(token.start_ - begin_);
       99: 1812:  currentValue().setOffsetLimit(token.end_ - begin_);
       99: 1813:  return true;
        -: 1814:}
        -: 1815:
      528: 1816:bool OurReader::decodeString(Token& token, String& decoded) {
      528: 1817:  decoded.reserve(static_cast<size_t>(token.end_ - token.start_ - 2));
      528: 1818:  Location current = token.start_ + 1; // skip '"'
      528: 1819:  Location end = token.end_ - 1;       // do not include '"'
     3414: 1820:  while (current != end) {
     2886: 1821:    Char c = *current++;
     2886: 1822:    if (c == '"')
    #####: 1823:      break;
     2886: 1824:    if (c == '\\') {
    #####: 1825:      if (current == end)
    #####: 1826:        return addError("Empty escape sequence in string", token, current);
    #####: 1827:      Char escape = *current++;
    #####: 1828:      switch (escape) {
    #####: 1829:      case '"':
    #####: 1830:        decoded += '"';
    #####: 1831:        break;
    #####: 1832:      case '/':
    #####: 1833:        decoded += '/';
    #####: 1834:        break;
    #####: 1835:      case '\\':
    #####: 1836:        decoded += '\\';
    #####: 1837:        break;
    #####: 1838:      case 'b':
    #####: 1839:        decoded += '\b';
    #####: 1840:        break;
    #####: 1841:      case 'f':
    #####: 1842:        decoded += '\f';
    #####: 1843:        break;
    #####: 1844:      case 'n':
    #####: 1845:        decoded += '\n';
    #####: 1846:        break;
    #####: 1847:      case 'r':
    #####: 1848:        decoded += '\r';
    #####: 1849:        break;
    #####: 1850:      case 't':
    #####: 1851:        decoded += '\t';
    #####: 1852:        break;
    #####: 1853:      case 'u': {
        -: 1854:        unsigned int unicode;
    #####: 1855:        if (!decodeUnicodeCodePoint(token, current, end, unicode))
    #####: 1856:          return false;
    #####: 1857:        decoded += codePointToUTF8(unicode);
    #####: 1858:      } break;
    #####: 1859:      default:
    #####: 1860:        return addError("Bad escape sequence in string", token, current);
        -: 1861:      }
        -: 1862:    } else {
     2886: 1863:      decoded += c;
        -: 1864:    }
        -: 1865:  }
      528: 1866:  return true;
        -: 1867:}
        -: 1868:
    #####: 1869:bool OurReader::decodeUnicodeCodePoint(Token& token, Location& current,
        -: 1870:                                       Location end, unsigned int& unicode) {
        -: 1871:
    #####: 1872:  if (!decodeUnicodeEscapeSequence(token, current, end, unicode))
    #####: 1873:    return false;
    #####: 1874:  if (unicode >= 0xD800 && unicode <= 0xDBFF) {
        -: 1875:    // surrogate pairs
    #####: 1876:    if (end - current < 6)
    #####: 1877:      return addError(
        -: 1878:          "additional six characters expected to parse unicode surrogate pair.",
    #####: 1879:          token, current);
    #####: 1880:    if (*(current++) == '\\' && *(current++) == 'u') {
        -: 1881:      unsigned int surrogatePair;
    #####: 1882:      if (decodeUnicodeEscapeSequence(token, current, end, surrogatePair)) {
    #####: 1883:        unicode = 0x10000 + ((unicode & 0x3FF) << 10) + (surrogatePair & 0x3FF);
        -: 1884:      } else
    #####: 1885:        return false;
        -: 1886:    } else
    #####: 1887:      return addError("expecting another \\u token to begin the second half of "
        -: 1888:                      "a unicode surrogate pair",
    #####: 1889:                      token, current);
        -: 1890:  }
    #####: 1891:  return true;
        -: 1892:}
        -: 1893:
    #####: 1894:bool OurReader::decodeUnicodeEscapeSequence(Token& token, Location& current,
        -: 1895:                                            Location end,
        -: 1896:                                            unsigned int& ret_unicode) {
    #####: 1897:  if (end - current < 4)
    #####: 1898:    return addError(
        -: 1899:        "Bad unicode escape sequence in string: four digits expected.", token,
    #####: 1900:        current);
    #####: 1901:  int unicode = 0;
    #####: 1902:  for (int index = 0; index < 4; ++index) {
    #####: 1903:    Char c = *current++;
    #####: 1904:    unicode *= 16;
    #####: 1905:    if (c >= '0' && c <= '9')
    #####: 1906:      unicode += c - '0';
    #####: 1907:    else if (c >= 'a' && c <= 'f')
    #####: 1908:      unicode += c - 'a' + 10;
    #####: 1909:    else if (c >= 'A' && c <= 'F')
    #####: 1910:      unicode += c - 'A' + 10;
        -: 1911:    else
    #####: 1912:      return addError(
        -: 1913:          "Bad unicode escape sequence in string: hexadecimal digit expected.",
    #####: 1914:          token, current);
        -: 1915:  }
    #####: 1916:  ret_unicode = static_cast<unsigned int>(unicode);
    #####: 1917:  return true;
        -: 1918:}
        -: 1919:
    #####: 1920:bool OurReader::addError(const String& message, Token& token, Location extra) {
    #####: 1921:  ErrorInfo info;
    #####: 1922:  info.token_ = token;
    #####: 1923:  info.message_ = message;
    #####: 1924:  info.extra_ = extra;
    #####: 1925:  errors_.push_back(info);
    #####: 1926:  return false;
        -: 1927:}
        -: 1928:
    #####: 1929:bool OurReader::recoverFromError(TokenType skipUntilToken) {
    #####: 1930:  size_t errorCount = errors_.size();
        -: 1931:  Token skip;
        -: 1932:  for (;;) {
    #####: 1933:    if (!readToken(skip))
    #####: 1934:      errors_.resize(errorCount); // discard errors caused by recovery
    #####: 1935:    if (skip.type_ == skipUntilToken || skip.type_ == tokenEndOfStream)
        -: 1936:      break;
        -: 1937:  }
    #####: 1938:  errors_.resize(errorCount);
    #####: 1939:  return false;
        -: 1940:}
        -: 1941:
    #####: 1942:bool OurReader::addErrorAndRecover(const String& message, Token& token,
        -: 1943:                                   TokenType skipUntilToken) {
    #####: 1944:  addError(message, token);
    #####: 1945:  return recoverFromError(skipUntilToken);
        -: 1946:}
        -: 1947:
     2330: 1948:Value& OurReader::currentValue() { return *(nodes_.top()); }
        -: 1949:
     5206: 1950:OurReader::Char OurReader::getNextChar() {
     5206: 1951:  if (current_ == end_)
        5: 1952:    return 0;
     5201: 1953:  return *current_++;
        -: 1954:}
        -: 1955:
    #####: 1956:void OurReader::getLocationLineAndColumn(Location location, int& line,
        -: 1957:                                         int& column) const {
    #####: 1958:  Location current = begin_;
    #####: 1959:  Location lastLineStart = current;
    #####: 1960:  line = 0;
    #####: 1961:  while (current < location && current != end_) {
    #####: 1962:    Char c = *current++;
    #####: 1963:    if (c == '\r') {
    #####: 1964:      if (*current == '\n')
    #####: 1965:        ++current;
    #####: 1966:      lastLineStart = current;
    #####: 1967:      ++line;
    #####: 1968:    } else if (c == '\n') {
    #####: 1969:      lastLineStart = current;
    #####: 1970:      ++line;
        -: 1971:    }
        -: 1972:  }
        -: 1973:  // column & line start at 1
    #####: 1974:  column = int(location - lastLineStart) + 1;
    #####: 1975:  ++line;
    #####: 1976:}
        -: 1977:
    #####: 1978:String OurReader::getLocationLineAndColumn(Location location) const {
        -: 1979:  int line, column;
    #####: 1980:  getLocationLineAndColumn(location, line, column);
        -: 1981:  char buffer[18 + 16 + 16 + 1];
    #####: 1982:  jsoncpp_snprintf(buffer, sizeof(buffer), "Line %d, Column %d", line, column);
    #####: 1983:  return buffer;
        -: 1984:}
        -: 1985:
        5: 1986:String OurReader::getFormattedErrorMessages() const {
        5: 1987:  String formattedMessage;
       5*: 1988:  for (const auto& error : errors_) {
        -: 1989:    formattedMessage +=
    #####: 1990:        "* " + getLocationLineAndColumn(error.token_.start_) + "\n";
    #####: 1991:    formattedMessage += "  " + error.message_ + "\n";
    #####: 1992:    if (error.extra_)
        -: 1993:      formattedMessage +=
    #####: 1994:          "See " + getLocationLineAndColumn(error.extra_) + " for detail.\n";
        -: 1995:  }
        5: 1996:  return formattedMessage;
        -: 1997:}
        -: 1998:
    #####: 1999:std::vector<OurReader::StructuredError> OurReader::getStructuredErrors() const {
    #####: 2000:  std::vector<OurReader::StructuredError> allErrors;
    #####: 2001:  for (const auto& error : errors_) {
    #####: 2002:    OurReader::StructuredError structured;
    #####: 2003:    structured.offset_start = error.token_.start_ - begin_;
    #####: 2004:    structured.offset_limit = error.token_.end_ - begin_;
    #####: 2005:    structured.message = error.message_;
    #####: 2006:    allErrors.push_back(structured);
        -: 2007:  }
    #####: 2008:  return allErrors;
        -: 2009:}
        -: 2010:
        -: 2011:class OurCharReader : public CharReader {
        -: 2012:  bool const collectComments_;
        -: 2013:  OurReader reader_;
        -: 2014:
        -: 2015:public:
        5: 2016:  OurCharReader(bool collectComments, OurFeatures const& features)
        5: 2017:      : collectComments_(collectComments), reader_(features) {}
        5: 2018:  bool parse(char const* beginDoc, char const* endDoc, Value* root,
        -: 2019:             String* errs) override {
        5: 2020:    bool ok = reader_.parse(beginDoc, endDoc, *root, collectComments_);
        5: 2021:    if (errs) {
        5: 2022:      *errs = reader_.getFormattedErrorMessages();
        -: 2023:    }
        5: 2024:    return ok;
        -: 2025:  }
        -: 2026:};
        -: 2027:
        5: 2028:CharReaderBuilder::CharReaderBuilder() { setDefaults(&settings_); }
        -: 2029:CharReaderBuilder::~CharReaderBuilder() = default;
        5: 2030:CharReader* CharReaderBuilder::newCharReader() const {
        5: 2031:  bool collectComments = settings_["collectComments"].asBool();
        5: 2032:  OurFeatures features = OurFeatures::all();
        5: 2033:  features.allowComments_ = settings_["allowComments"].asBool();
        5: 2034:  features.allowTrailingCommas_ = settings_["allowTrailingCommas"].asBool();
        5: 2035:  features.strictRoot_ = settings_["strictRoot"].asBool();
        5: 2036:  features.allowDroppedNullPlaceholders_ =
        5: 2037:      settings_["allowDroppedNullPlaceholders"].asBool();
        5: 2038:  features.allowNumericKeys_ = settings_["allowNumericKeys"].asBool();
        5: 2039:  features.allowSingleQuotes_ = settings_["allowSingleQuotes"].asBool();
        -: 2040:
        -: 2041:  // Stack limit is always a size_t, so we get this as an unsigned int
        -: 2042:  // regardless of it we have 64-bit integer support enabled.
        5: 2043:  features.stackLimit_ = static_cast<size_t>(settings_["stackLimit"].asUInt());
        5: 2044:  features.failIfExtra_ = settings_["failIfExtra"].asBool();
        5: 2045:  features.rejectDupKeys_ = settings_["rejectDupKeys"].asBool();
        5: 2046:  features.allowSpecialFloats_ = settings_["allowSpecialFloats"].asBool();
        5: 2047:  features.skipBom_ = settings_["skipBom"].asBool();
        5: 2048:  return new OurCharReader(collectComments, features);
        -: 2049:}
        -: 2050:
    #####: 2051:bool CharReaderBuilder::validate(Json::Value* invalid) const {
        -: 2052:  static const auto& valid_keys = *new std::set<String>{
        -: 2053:      "collectComments",
        -: 2054:      "allowComments",
        -: 2055:      "allowTrailingCommas",
        -: 2056:      "strictRoot",
        -: 2057:      "allowDroppedNullPlaceholders",
        -: 2058:      "allowNumericKeys",
        -: 2059:      "allowSingleQuotes",
        -: 2060:      "stackLimit",
        -: 2061:      "failIfExtra",
        -: 2062:      "rejectDupKeys",
        -: 2063:      "allowSpecialFloats",
        -: 2064:      "skipBom",
    #####: 2065:  };
    #####: 2066:  for (auto si = settings_.begin(); si != settings_.end(); ++si) {
    #####: 2067:    auto key = si.name();
    #####: 2068:    if (valid_keys.count(key))
    #####: 2069:      continue;
    #####: 2070:    if (invalid)
    #####: 2071:      (*invalid)[std::move(key)] = *si;
        -: 2072:    else
    #####: 2073:      return false;
        -: 2074:  }
    #####: 2075:  return invalid ? invalid->empty() : true;
        -: 2076:}
        -: 2077:
    #####: 2078:Value& CharReaderBuilder::operator[](const String& key) {
    #####: 2079:  return settings_[key];
        -: 2080:}
        -: 2081:// static
    #####: 2082:void CharReaderBuilder::strictMode(Json::Value* settings) {
        -: 2083:  //! [CharReaderBuilderStrictMode]
    #####: 2084:  (*settings)["allowComments"] = false;
    #####: 2085:  (*settings)["allowTrailingCommas"] = false;
    #####: 2086:  (*settings)["strictRoot"] = true;
    #####: 2087:  (*settings)["allowDroppedNullPlaceholders"] = false;
    #####: 2088:  (*settings)["allowNumericKeys"] = false;
    #####: 2089:  (*settings)["allowSingleQuotes"] = false;
    #####: 2090:  (*settings)["stackLimit"] = 1000;
    #####: 2091:  (*settings)["failIfExtra"] = true;
    #####: 2092:  (*settings)["rejectDupKeys"] = true;
    #####: 2093:  (*settings)["allowSpecialFloats"] = false;
    #####: 2094:  (*settings)["skipBom"] = true;
        -: 2095:  //! [CharReaderBuilderStrictMode]
    #####: 2096:}
        -: 2097:// static
        5: 2098:void CharReaderBuilder::setDefaults(Json::Value* settings) {
        -: 2099:  //! [CharReaderBuilderDefaults]
        5: 2100:  (*settings)["collectComments"] = true;
        5: 2101:  (*settings)["allowComments"] = true;
        5: 2102:  (*settings)["allowTrailingCommas"] = true;
        5: 2103:  (*settings)["strictRoot"] = false;
        5: 2104:  (*settings)["allowDroppedNullPlaceholders"] = false;
        5: 2105:  (*settings)["allowNumericKeys"] = false;
        5: 2106:  (*settings)["allowSingleQuotes"] = false;
        5: 2107:  (*settings)["stackLimit"] = 1000;
        5: 2108:  (*settings)["failIfExtra"] = false;
        5: 2109:  (*settings)["rejectDupKeys"] = false;
        5: 2110:  (*settings)["allowSpecialFloats"] = false;
        5: 2111:  (*settings)["skipBom"] = true;
        -: 2112:  //! [CharReaderBuilderDefaults]
        5: 2113:}
        -: 2114:
        -: 2115://////////////////////////////////
        -: 2116:// global functions
        -: 2117:
        5: 2118:bool parseFromStream(CharReader::Factory const& fact, IStream& sin, Value* root,
        -: 2119:                     String* errs) {
       10: 2120:  OStringStream ssin;
        5: 2121:  ssin << sin.rdbuf();
       10: 2122:  String doc = ssin.str();
        5: 2123:  char const* begin = doc.data();
        5: 2124:  char const* end = begin + doc.size();
        -: 2125:  // Note that we do not actually need a null-terminator.
       10: 2126:  CharReaderPtr const reader(fact.newCharReader());
       10: 2127:  return reader->parse(begin, end, root, errs);
        -: 2128:}
        -: 2129:
        5: 2130:IStream& operator>>(IStream& sin, Value& root) {
       10: 2131:  CharReaderBuilder b;
        5: 2132:  String errs;
        5: 2133:  bool ok = parseFromStream(b, sin, &root, &errs);
        5: 2134:  if (!ok) {
    #####: 2135:    throwRuntimeError(errs);
        -: 2136:  }
       10: 2137:  return sin;
        -: 2138:}
        -: 2139:
        -: 2140:} // namespace Json
        -: 2141:
        -: 2142:// //////////////////////////////////////////////////////////////////////
        -: 2143:// End of content of file: src/lib_json/json_reader.cpp
        -: 2144:// //////////////////////////////////////////////////////////////////////
        -: 2145:
        -: 2146:
        -: 2147:
        -: 2148:
        -: 2149:
        -: 2150:
        -: 2151:// //////////////////////////////////////////////////////////////////////
        -: 2152:// Beginning of content of file: src/lib_json/json_valueiterator.inl
        -: 2153:// //////////////////////////////////////////////////////////////////////
        -: 2154:
        -: 2155:// Copyright 2007-2010 Baptiste Lepilleur and The JsonCpp Authors
        -: 2156:// Distributed under MIT license, or public domain if desired and
        -: 2157:// recognized in your jurisdiction.
        -: 2158:// See file LICENSE for detail or copy at http://jsoncpp.sourceforge.net/LICENSE
        -: 2159:
        -: 2160:// included by json_value.cpp
        -: 2161:
        -: 2162:namespace Json {
        -: 2163:
        -: 2164:// //////////////////////////////////////////////////////////////////
        -: 2165:// //////////////////////////////////////////////////////////////////
        -: 2166:// //////////////////////////////////////////////////////////////////
        -: 2167:// class ValueIteratorBase
        -: 2168:// //////////////////////////////////////////////////////////////////
        -: 2169:// //////////////////////////////////////////////////////////////////
        -: 2170:// //////////////////////////////////////////////////////////////////
        -: 2171:
    #####: 2172:ValueIteratorBase::ValueIteratorBase() : current_() {}
        -: 2173:
    #####: 2174:ValueIteratorBase::ValueIteratorBase(
    #####: 2175:    const Value::ObjectValues::iterator& current)
    #####: 2176:    : current_(current), isNull_(false) {}
        -: 2177:
    #####: 2178:Value& ValueIteratorBase::deref() { return current_->second; }
    #####: 2179:const Value& ValueIteratorBase::deref() const { return current_->second; }
        -: 2180:
    #####: 2181:void ValueIteratorBase::increment() { ++current_; }
        -: 2182:
    #####: 2183:void ValueIteratorBase::decrement() { --current_; }
        -: 2184:
        -: 2185:ValueIteratorBase::difference_type
    #####: 2186:ValueIteratorBase::computeDistance(const SelfType& other) const {
        -: 2187:  // Iterator for null value are initialized using the default
        -: 2188:  // constructor, which initialize current_ to the default
        -: 2189:  // std::map::iterator. As begin() and end() are two instance
        -: 2190:  // of the default std::map::iterator, they can not be compared.
        -: 2191:  // To allow this, we handle this comparison specifically.
    #####: 2192:  if (isNull_ && other.isNull_) {
    #####: 2193:    return 0;
        -: 2194:  }
        -: 2195:
        -: 2196:  // Usage of std::distance is not portable (does not compile with Sun Studio 12
        -: 2197:  // RogueWave STL,
        -: 2198:  // which is the one used by default).
        -: 2199:  // Using a portable hand-made version for non random iterator instead:
        -: 2200:  //   return difference_type( std::distance( current_, other.current_ ) );
    #####: 2201:  difference_type myDistance = 0;
    #####: 2202:  for (Value::ObjectValues::iterator it = current_; it != other.current_;
    #####: 2203:       ++it) {
    #####: 2204:    ++myDistance;
        -: 2205:  }
    #####: 2206:  return myDistance;
        -: 2207:}
        -: 2208:
    #####: 2209:bool ValueIteratorBase::isEqual(const SelfType& other) const {
    #####: 2210:  if (isNull_) {
    #####: 2211:    return other.isNull_;
        -: 2212:  }
    #####: 2213:  return current_ == other.current_;
        -: 2214:}
        -: 2215:
    #####: 2216:void ValueIteratorBase::copy(const SelfType& other) {
    #####: 2217:  current_ = other.current_;
    #####: 2218:  isNull_ = other.isNull_;
    #####: 2219:}
        -: 2220:
    #####: 2221:Value ValueIteratorBase::key() const {
    #####: 2222:  const Value::CZString czstring = (*current_).first;
    #####: 2223:  if (czstring.data()) {
    #####: 2224:    if (czstring.isStaticString())
    #####: 2225:      return Value(StaticString(czstring.data()));
    #####: 2226:    return Value(czstring.data(), czstring.data() + czstring.length());
        -: 2227:  }
    #####: 2228:  return Value(czstring.index());
        -: 2229:}
        -: 2230:
    #####: 2231:UInt ValueIteratorBase::index() const {
    #####: 2232:  const Value::CZString czstring = (*current_).first;
    #####: 2233:  if (!czstring.data())
    #####: 2234:    return czstring.index();
    #####: 2235:  return Value::UInt(-1);
        -: 2236:}
        -: 2237:
    #####: 2238:String ValueIteratorBase::name() const {
        -: 2239:  char const* keey;
        -: 2240:  char const* end;
    #####: 2241:  keey = memberName(&end);
    #####: 2242:  if (!keey)
    #####: 2243:    return String();
    #####: 2244:  return String(keey, end);
        -: 2245:}
        -: 2246:
    #####: 2247:char const* ValueIteratorBase::memberName() const {
    #####: 2248:  const char* cname = (*current_).first.data();
    #####: 2249:  return cname ? cname : "";
        -: 2250:}
        -: 2251:
    #####: 2252:char const* ValueIteratorBase::memberName(char const** end) const {
    #####: 2253:  const char* cname = (*current_).first.data();
    #####: 2254:  if (!cname) {
    #####: 2255:    *end = nullptr;
    #####: 2256:    return nullptr;
        -: 2257:  }
    #####: 2258:  *end = cname + (*current_).first.length();
    #####: 2259:  return cname;
        -: 2260:}
        -: 2261:
        -: 2262:// //////////////////////////////////////////////////////////////////
        -: 2263:// //////////////////////////////////////////////////////////////////
        -: 2264:// //////////////////////////////////////////////////////////////////
        -: 2265:// class ValueConstIterator
        -: 2266:// //////////////////////////////////////////////////////////////////
        -: 2267:// //////////////////////////////////////////////////////////////////
        -: 2268:// //////////////////////////////////////////////////////////////////
        -: 2269:
        -: 2270:ValueConstIterator::ValueConstIterator() = default;
        -: 2271:
    #####: 2272:ValueConstIterator::ValueConstIterator(
    #####: 2273:    const Value::ObjectValues::iterator& current)
    #####: 2274:    : ValueIteratorBase(current) {}
        -: 2275:
    #####: 2276:ValueConstIterator::ValueConstIterator(ValueIterator const& other)
    #####: 2277:    : ValueIteratorBase(other) {}
        -: 2278:
    #####: 2279:ValueConstIterator& ValueConstIterator::
        -: 2280:operator=(const ValueIteratorBase& other) {
    #####: 2281:  copy(other);
    #####: 2282:  return *this;
        -: 2283:}
        -: 2284:
        -: 2285:// //////////////////////////////////////////////////////////////////
        -: 2286:// //////////////////////////////////////////////////////////////////
        -: 2287:// //////////////////////////////////////////////////////////////////
        -: 2288:// class ValueIterator
        -: 2289:// //////////////////////////////////////////////////////////////////
        -: 2290:// //////////////////////////////////////////////////////////////////
        -: 2291:// //////////////////////////////////////////////////////////////////
        -: 2292:
        -: 2293:ValueIterator::ValueIterator() = default;
        -: 2294:
    #####: 2295:ValueIterator::ValueIterator(const Value::ObjectValues::iterator& current)
    #####: 2296:    : ValueIteratorBase(current) {}
        -: 2297:
    #####: 2298:ValueIterator::ValueIterator(const ValueConstIterator& other)
    #####: 2299:    : ValueIteratorBase(other) {
    #####: 2300:  throwRuntimeError("ConstIterator to Iterator should never be allowed.");
        -: 2301:}
        -: 2302:
        -: 2303:ValueIterator::ValueIterator(const ValueIterator& other) = default;
        -: 2304:
    #####: 2305:ValueIterator& ValueIterator::operator=(const SelfType& other) {
    #####: 2306:  copy(other);
    #####: 2307:  return *this;
        -: 2308:}
        -: 2309:
        -: 2310:} // namespace Json
        -: 2311:
        -: 2312:// //////////////////////////////////////////////////////////////////////
        -: 2313:// End of content of file: src/lib_json/json_valueiterator.inl
        -: 2314:// //////////////////////////////////////////////////////////////////////
        -: 2315:
        -: 2316:
        -: 2317:
        -: 2318:
        -: 2319:
        -: 2320:
        -: 2321:// //////////////////////////////////////////////////////////////////////
        -: 2322:// Beginning of content of file: src/lib_json/json_value.cpp
        -: 2323:// //////////////////////////////////////////////////////////////////////
        -: 2324:
        -: 2325:// Copyright 2011 Baptiste Lepilleur and The JsonCpp Authors
        -: 2326:// Distributed under MIT license, or public domain if desired and
        -: 2327:// recognized in your jurisdiction.
        -: 2328:// See file LICENSE for detail or copy at http://jsoncpp.sourceforge.net/LICENSE
        -: 2329:
        -: 2330:#if !defined(JSON_IS_AMALGAMATION)
        -: 2331:#include <json/assertions.h>
        -: 2332:#include <json/value.h>
        -: 2333:#include <json/writer.h>
        -: 2334:#endif // if !defined(JSON_IS_AMALGAMATION)
        -: 2335:#include <algorithm>
        -: 2336:#include <cassert>
        -: 2337:#include <cmath>
        -: 2338:#include <cstddef>
        -: 2339:#include <cstring>
        -: 2340:#include <iostream>
        -: 2341:#include <sstream>
        -: 2342:#include <utility>
        -: 2343:
        -: 2344:// Provide implementation equivalent of std::snprintf for older _MSC compilers
        -: 2345:#if defined(_MSC_VER) && _MSC_VER < 1900
        -: 2346:#include <stdarg.h>
        -: 2347:static int msvc_pre1900_c99_vsnprintf(char* outBuf, size_t size,
        -: 2348:                                      const char* format, va_list ap) {
        -: 2349:  int count = -1;
        -: 2350:  if (size != 0)
        -: 2351:    count = _vsnprintf_s(outBuf, size, _TRUNCATE, format, ap);
        -: 2352:  if (count == -1)
        -: 2353:    count = _vscprintf(format, ap);
        -: 2354:  return count;
        -: 2355:}
        -: 2356:
        -: 2357:int JSON_API msvc_pre1900_c99_snprintf(char* outBuf, size_t size,
        -: 2358:                                       const char* format, ...) {
        -: 2359:  va_list ap;
        -: 2360:  va_start(ap, format);
        -: 2361:  const int count = msvc_pre1900_c99_vsnprintf(outBuf, size, format, ap);
        -: 2362:  va_end(ap);
        -: 2363:  return count;
        -: 2364:}
        -: 2365:#endif
        -: 2366:
        -: 2367:// Disable warning C4702 : unreachable code
        -: 2368:#if defined(_MSC_VER)
        -: 2369:#pragma warning(disable : 4702)
        -: 2370:#endif
        -: 2371:
        -: 2372:#define JSON_ASSERT_UNREACHABLE assert(false)
        -: 2373:
        -: 2374:namespace Json {
        -: 2375:template <typename T>
     1499: 2376:static std::unique_ptr<T> cloneUnique(const std::unique_ptr<T>& p) {
     1499: 2377:  std::unique_ptr<T> r;
     1499: 2378:  if (p) {
    #####: 2379:    r = std::unique_ptr<T>(new T(*p));
        -: 2380:  }
     1499: 2381:  return r;
        -: 2382:}
        -: 2383:
        -: 2384:// This is a walkaround to avoid the static initialization of Value::null.
        -: 2385:// kNull must be word-aligned to avoid crashing on ARM.  We use an alignment of
        -: 2386:// 8 (instead of 4) as a bit of future-proofing.
        -: 2387:#if defined(__ARMEL__)
        -: 2388:#define ALIGNAS(byte_alignment) __attribute__((aligned(byte_alignment)))
        -: 2389:#else
        -: 2390:#define ALIGNAS(byte_alignment)
        -: 2391:#endif
        -: 2392:
        -: 2393:// static
      561: 2394:Value const& Value::nullSingleton() {
      561: 2395:  static Value const nullStatic;
      561: 2396:  return nullStatic;
        -: 2397:}
        -: 2398:
        -: 2399:#if JSON_USE_NULLREF
        -: 2400:// for backwards compatibility, we'll leave these global references around, but
        -: 2401:// DO NOT use them in JSONCPP library code any more!
        -: 2402:// static
        -: 2403:Value const& Value::null = Value::nullSingleton();
        -: 2404:
        -: 2405:// static
        -: 2406:Value const& Value::nullRef = Value::nullSingleton();
        -: 2407:#endif
        -: 2408:
        -: 2409:#if !defined(JSON_USE_INT64_DOUBLE_CONVERSION)
        -: 2410:template <typename T, typename U>
        -: 2411:static inline bool InRange(double d, T min, U max) {
        -: 2412:  // The casts can lose precision, but we are looking only for
        -: 2413:  // an approximate range. Might fail on edge cases though. ~cdunn
        -: 2414:  return d >= static_cast<double>(min) && d <= static_cast<double>(max);
        -: 2415:}
        -: 2416:#else  // if !defined(JSON_USE_INT64_DOUBLE_CONVERSION)
    #####: 2417:static inline double integerToDouble(Json::UInt64 value) {
    #####: 2418:  return static_cast<double>(Int64(value / 2)) * 2.0 +
    #####: 2419:         static_cast<double>(Int64(value & 1));
        -: 2420:}
        -: 2421:
    #####: 2422:template <typename T> static inline double integerToDouble(T value) {
    #####: 2423:  return static_cast<double>(value);
        -: 2424:}
------------------
_ZN4JsonL15integerToDoubleIlEEdT_:
    #####: 2422:template <typename T> static inline double integerToDouble(T value) {
    #####: 2423:  return static_cast<double>(value);
        -: 2424:}
------------------
_ZN4JsonL15integerToDoubleIjEEdT_:
    #####: 2422:template <typename T> static inline double integerToDouble(T value) {
    #####: 2423:  return static_cast<double>(value);
        -: 2424:}
------------------
_ZN4JsonL15integerToDoubleIiEEdT_:
    #####: 2422:template <typename T> static inline double integerToDouble(T value) {
    #####: 2423:  return static_cast<double>(value);
        -: 2424:}
------------------
        -: 2425:
        -: 2426:template <typename T, typename U>
    #####: 2427:static inline bool InRange(double d, T min, U max) {
    #####: 2428:  return d >= integerToDouble(min) && d <= integerToDouble(max);
        -: 2429:}
------------------
_ZN4JsonL7InRangeIimEEbdT_T0_:
    #####: 2427:static inline bool InRange(double d, T min, U max) {
    #####: 2428:  return d >= integerToDouble(min) && d <= integerToDouble(max);
        -: 2429:}
------------------
_ZN4JsonL7InRangeIllEEbdT_T0_:
    #####: 2427:static inline bool InRange(double d, T min, U max) {
    #####: 2428:  return d >= integerToDouble(min) && d <= integerToDouble(max);
        -: 2429:}
------------------
_ZN4JsonL7InRangeIijEEbdT_T0_:
    #####: 2427:static inline bool InRange(double d, T min, U max) {
    #####: 2428:  return d >= integerToDouble(min) && d <= integerToDouble(max);
        -: 2429:}
------------------
_ZN4JsonL7InRangeIiiEEbdT_T0_:
    #####: 2427:static inline bool InRange(double d, T min, U max) {
    #####: 2428:  return d >= integerToDouble(min) && d <= integerToDouble(max);
        -: 2429:}
------------------
        -: 2430:#endif // if !defined(JSON_USE_INT64_DOUBLE_CONVERSION)
        -: 2431:
        -: 2432:/** Duplicates the specified string value.
        -: 2433: * @param value Pointer to the string to duplicate. Must be zero-terminated if
        -: 2434: *              length is "unknown".
        -: 2435: * @param length Length of the value. if equals to unknown, then it will be
        -: 2436: *               computed using strlen(value).
        -: 2437: * @return Pointer on the duplicate instance of string.
        -: 2438: */
     1403: 2439:static inline char* duplicateStringValue(const char* value, size_t length) {
        -: 2440:  // Avoid an integer overflow in the call to malloc below by limiting length
        -: 2441:  // to a sane value.
     1403: 2442:  if (length >= static_cast<size_t>(Value::maxInt))
    #####: 2443:    length = Value::maxInt - 1;
        -: 2444:
     1403: 2445:  auto newString = static_cast<char*>(malloc(length + 1));
     1403: 2446:  if (newString == nullptr) {
    #####: 2447:    throwRuntimeError("in Json::Value::duplicateStringValue(): "
        -: 2448:                      "Failed to allocate string value buffer");
        -: 2449:  }
     1403: 2450:  memcpy(newString, value, length);
     1403: 2451:  newString[length] = 0;
     1403: 2452:  return newString;
        -: 2453:}
        -: 2454:
        -: 2455:/* Record the length as a prefix.
        -: 2456: */
      195: 2457:static inline char* duplicateAndPrefixStringValue(const char* value,
        -: 2458:                                                  unsigned int length) {
        -: 2459:  // Avoid an integer overflow in the call to malloc below by limiting length
        -: 2460:  // to a sane value.
     195*: 2461:  JSON_ASSERT_MESSAGE(length <= static_cast<unsigned>(Value::maxInt) -
        -: 2462:                                    sizeof(unsigned) - 1U,
        -: 2463:                      "in Json::Value::duplicateAndPrefixStringValue(): "
        -: 2464:                      "length too big for prefixing");
      195: 2465:  size_t actualLength = sizeof(length) + length + 1;
      195: 2466:  auto newString = static_cast<char*>(malloc(actualLength));
      195: 2467:  if (newString == nullptr) {
    #####: 2468:    throwRuntimeError("in Json::Value::duplicateAndPrefixStringValue(): "
        -: 2469:                      "Failed to allocate string value buffer");
        -: 2470:  }
      195: 2471:  *reinterpret_cast<unsigned*>(newString) = length;
      195: 2472:  memcpy(newString + sizeof(unsigned), value, length);
      195: 2473:  newString[actualLength - 1U] =
        -: 2474:      0; // to avoid buffer over-run accidents by users later
      195: 2475:  return newString;
        -: 2476:}
      191: 2477:inline static void decodePrefixedString(bool isPrefixed, char const* prefixed,
        -: 2478:                                        unsigned* length, char const** value) {
      191: 2479:  if (!isPrefixed) {
    #####: 2480:    *length = static_cast<unsigned>(strlen(prefixed));
    #####: 2481:    *value = prefixed;
        -: 2482:  } else {
      191: 2483:    *length = *reinterpret_cast<unsigned const*>(prefixed);
      191: 2484:    *value = prefixed + sizeof(unsigned);
        -: 2485:  }
      191: 2486:}
        -: 2487:/** Free the string duplicated by
        -: 2488: * duplicateStringValue()/duplicateAndPrefixStringValue().
        -: 2489: */
        -: 2490:#if JSONCPP_USING_SECURE_MEMORY
        -: 2491:static inline void releasePrefixedStringValue(char* value) {
        -: 2492:  unsigned length = 0;
        -: 2493:  char const* valueDecoded;
        -: 2494:  decodePrefixedString(true, value, &length, &valueDecoded);
        -: 2495:  size_t const size = sizeof(unsigned) + length + 1U;
        -: 2496:  memset(value, 0, size);
        -: 2497:  free(value);
        -: 2498:}
        -: 2499:static inline void releaseStringValue(char* value, unsigned length) {
        -: 2500:  // length==0 => we allocated the strings memory
        -: 2501:  size_t size = (length == 0) ? strlen(value) : length;
        -: 2502:  memset(value, 0, size);
        -: 2503:  free(value);
        -: 2504:}
        -: 2505:#else  // !JSONCPP_USING_SECURE_MEMORY
      195: 2506:static inline void releasePrefixedStringValue(char* value) { free(value); }
     1403: 2507:static inline void releaseStringValue(char* value, unsigned) { free(value); }
        -: 2508:#endif // JSONCPP_USING_SECURE_MEMORY
        -: 2509:
        -: 2510:} // namespace Json
        -: 2511:
        -: 2512:// //////////////////////////////////////////////////////////////////
        -: 2513:// //////////////////////////////////////////////////////////////////
        -: 2514:// //////////////////////////////////////////////////////////////////
        -: 2515:// ValueInternals...
        -: 2516:// //////////////////////////////////////////////////////////////////
        -: 2517:// //////////////////////////////////////////////////////////////////
        -: 2518:// //////////////////////////////////////////////////////////////////
        -: 2519:#if !defined(JSON_IS_AMALGAMATION)
        -: 2520:
        -: 2521:#include "json_valueiterator.inl"
        -: 2522:#endif // if !defined(JSON_IS_AMALGAMATION)
        -: 2523:
        -: 2524:namespace Json {
        -: 2525:
        -: 2526:#if JSON_USE_EXCEPTION
    #####: 2527:Exception::Exception(String msg) : msg_(std::move(msg)) {}
        -: 2528:Exception::~Exception() noexcept = default;
    #####: 2529:char const* Exception::what() const noexcept { return msg_.c_str(); }
    #####: 2530:RuntimeError::RuntimeError(String const& msg) : Exception(msg) {}
    #####: 2531:LogicError::LogicError(String const& msg) : Exception(msg) {}
    #####: 2532:JSONCPP_NORETURN void throwRuntimeError(String const& msg) {
    #####: 2533:  throw RuntimeError(msg);
        -: 2534:}
    #####: 2535:JSONCPP_NORETURN void throwLogicError(String const& msg) {
    #####: 2536:  throw LogicError(msg);
        -: 2537:}
        -: 2538:#else // !JSON_USE_EXCEPTION
        -: 2539:JSONCPP_NORETURN void throwRuntimeError(String const& msg) {
        -: 2540:  std::cerr << msg << std::endl;
        -: 2541:  abort();
        -: 2542:}
        -: 2543:JSONCPP_NORETURN void throwLogicError(String const& msg) {
        -: 2544:  std::cerr << msg << std::endl;
        -: 2545:  abort();
        -: 2546:}
        -: 2547:#endif
        -: 2548:
        -: 2549:// //////////////////////////////////////////////////////////////////
        -: 2550:// //////////////////////////////////////////////////////////////////
        -: 2551:// //////////////////////////////////////////////////////////////////
        -: 2552:// class Value::CZString
        -: 2553:// //////////////////////////////////////////////////////////////////
        -: 2554:// //////////////////////////////////////////////////////////////////
        -: 2555:// //////////////////////////////////////////////////////////////////
        -: 2556:
        -: 2557:// Notes: policy_ indicates if the string was allocated when
        -: 2558:// a string is stored.
        -: 2559:
      155: 2560:Value::CZString::CZString(ArrayIndex index) : cstr_(nullptr), index_(index) {}
        -: 2561:
     1039: 2562:Value::CZString::CZString(char const* str, unsigned length,
     1039: 2563:                          DuplicationPolicy allocate)
     1039: 2564:    : cstr_(str) {
        -: 2565:  // allocate != duplicate
     1039: 2566:  storage_.policy_ = allocate & 0x3;
     1039: 2567:  storage_.length_ = length & 0x3FFFFFFF;
     1039: 2568:}
        -: 2569:
     1489: 2570:Value::CZString::CZString(const CZString& other) {
     1463: 2571:  cstr_ = (other.storage_.policy_ != noDuplication && other.cstr_ != nullptr
     2952: 2572:               ? duplicateStringValue(other.cstr_, other.storage_.length_)
        -: 2573:               : other.cstr_);
     1489: 2574:  storage_.policy_ =
        -: 2575:      static_cast<unsigned>(
     1489: 2576:          other.cstr_
     1403: 2577:              ? (static_cast<DuplicationPolicy>(other.storage_.policy_) ==
        -: 2578:                         noDuplication
     1403: 2579:                     ? noDuplication
        -: 2580:                     : duplicate)
     1489: 2581:              : static_cast<DuplicationPolicy>(other.storage_.policy_)) &
        -: 2582:      3U;
     1489: 2583:  storage_.length_ = other.storage_.length_;
     1489: 2584:}
        -: 2585:
    #####: 2586:Value::CZString::CZString(CZString&& other)
    #####: 2587:    : cstr_(other.cstr_), index_(other.index_) {
    #####: 2588:  other.cstr_ = nullptr;
    #####: 2589:}
        -: 2590:
     5366: 2591:Value::CZString::~CZString() {
     2683: 2592:  if (cstr_ && storage_.policy_ == duplicate) {
     1403: 2593:    releaseStringValue(const_cast<char*>(cstr_),
     1403: 2594:                       storage_.length_ + 1U); // +1 for null terminating
        -: 2595:                                               // character for sake of
        -: 2596:                                               // completeness but not actually
        -: 2597:                                               // necessary
        -: 2598:  }
     2683: 2599:}
        -: 2600:
    #####: 2601:void Value::CZString::swap(CZString& other) {
    #####: 2602:  std::swap(cstr_, other.cstr_);
    #####: 2603:  std::swap(index_, other.index_);
    #####: 2604:}
        -: 2605:
    #####: 2606:Value::CZString& Value::CZString::operator=(const CZString& other) {
    #####: 2607:  cstr_ = other.cstr_;
    #####: 2608:  index_ = other.index_;
    #####: 2609:  return *this;
        -: 2610:}
        -: 2611:
    #####: 2612:Value::CZString& Value::CZString::operator=(CZString&& other) {
    #####: 2613:  cstr_ = other.cstr_;
    #####: 2614:  index_ = other.index_;
    #####: 2615:  other.cstr_ = nullptr;
    #####: 2616:  return *this;
        -: 2617:}
        -: 2618:
     3974: 2619:bool Value::CZString::operator<(const CZString& other) const {
     3974: 2620:  if (!cstr_)
      534: 2621:    return index_ < other.index_;
        -: 2622:  // return strcmp(cstr_, other.cstr_) < 0;
        -: 2623:  // Assume both are strings.
     3440: 2624:  unsigned this_len = this->storage_.length_;
     3440: 2625:  unsigned other_len = other.storage_.length_;
     3440: 2626:  unsigned min_len = std::min<unsigned>(this_len, other_len);
    3440*: 2627:  JSON_ASSERT(this->cstr_ && other.cstr_);
     3440: 2628:  int comp = memcmp(this->cstr_, other.cstr_, min_len);
     3440: 2629:  if (comp < 0)
     1881: 2630:    return true;
     1559: 2631:  if (comp > 0)
      693: 2632:    return false;
      866: 2633:  return (this_len < other_len);
        -: 2634:}
        -: 2635:
      333: 2636:bool Value::CZString::operator==(const CZString& other) const {
      333: 2637:  if (!cstr_)
       95: 2638:    return index_ == other.index_;
        -: 2639:  // return strcmp(cstr_, other.cstr_) == 0;
        -: 2640:  // Assume both are strings.
      238: 2641:  unsigned this_len = this->storage_.length_;
      238: 2642:  unsigned other_len = other.storage_.length_;
      238: 2643:  if (this_len != other_len)
       64: 2644:    return false;
     174*: 2645:  JSON_ASSERT(this->cstr_ && other.cstr_);
      174: 2646:  int comp = memcmp(this->cstr_, other.cstr_, this_len);
      174: 2647:  return comp == 0;
        -: 2648:}
        -: 2649:
       13: 2650:ArrayIndex Value::CZString::index() const { return index_; }
        -: 2651:
        -: 2652:// const char* Value::CZString::c_str() const { return cstr_; }
      260: 2653:const char* Value::CZString::data() const { return cstr_; }
      260: 2654:unsigned Value::CZString::length() const { return storage_.length_; }
    #####: 2655:bool Value::CZString::isStaticString() const {
    #####: 2656:  return storage_.policy_ == noDuplication;
        -: 2657:}
        -: 2658:
        -: 2659:// //////////////////////////////////////////////////////////////////
        -: 2660:// //////////////////////////////////////////////////////////////////
        -: 2661:// //////////////////////////////////////////////////////////////////
        -: 2662:// class Value::Value
        -: 2663:// //////////////////////////////////////////////////////////////////
        -: 2664:// //////////////////////////////////////////////////////////////////
        -: 2665:// //////////////////////////////////////////////////////////////////
        -: 2666:
        -: 2667:/*! \internal Default constructor initialization must be equivalent to:
        -: 2668: * memset( this, 0, sizeof(Value) )
        -: 2669: * This optimization is used in ValueInternalMap fast allocator.
        -: 2670: */
      396: 2671:Value::Value(ValueType type) {
        -: 2672:  static char const emptyString[] = "";
      396: 2673:  initBasic(type);
      396: 2674:  switch (type) {
      280: 2675:  case nullValue:
      280: 2676:    break;
    #####: 2677:  case intValue:
        -: 2678:  case uintValue:
    #####: 2679:    value_.int_ = 0;
    #####: 2680:    break;
    #####: 2681:  case realValue:
    #####: 2682:    value_.real_ = 0.0;
    #####: 2683:    break;
    #####: 2684:  case stringValue:
        -: 2685:    // allocated_ == false, so this is safe.
    #####: 2686:    value_.string_ = const_cast<char*>(static_cast<char const*>(emptyString));
    #####: 2687:    break;
      116: 2688:  case arrayValue:
        -: 2689:  case objectValue:
      116: 2690:    value_.map_ = new ObjectValues();
      116: 2691:    break;
    #####: 2692:  case booleanValue:
    #####: 2693:    value_.bool_ = false;
    #####: 2694:    break;
    #####: 2695:  default:
    #####: 2696:    JSON_ASSERT_UNREACHABLE;
        -: 2697:  }
      396: 2698:}
        -: 2699:
       23: 2700:Value::Value(Int value) {
       23: 2701:  initBasic(intValue);
       23: 2702:  value_.int_ = value;
       23: 2703:}
        -: 2704:
    #####: 2705:Value::Value(UInt value) {
    #####: 2706:  initBasic(uintValue);
    #####: 2707:  value_.uint_ = value;
    #####: 2708:}
        -: 2709:#if defined(JSON_HAS_INT64)
      264: 2710:Value::Value(Int64 value) {
      264: 2711:  initBasic(intValue);
      264: 2712:  value_.int_ = value;
      264: 2713:}
    #####: 2714:Value::Value(UInt64 value) {
    #####: 2715:  initBasic(uintValue);
    #####: 2716:  value_.uint_ = value;
    #####: 2717:}
        -: 2718:#endif // defined(JSON_HAS_INT64)
        -: 2719:
    #####: 2720:Value::Value(double value) {
    #####: 2721:  initBasic(realValue);
    #####: 2722:  value_.real_ = value;
    #####: 2723:}
        -: 2724:
        9: 2725:Value::Value(const char* value) {
        9: 2726:  initBasic(stringValue, true);
       9*: 2727:  JSON_ASSERT_MESSAGE(value != nullptr,
        -: 2728:                      "Null Value Passed to Value Constructor");
       18: 2729:  value_.string_ = duplicateAndPrefixStringValue(
        9: 2730:      value, static_cast<unsigned>(strlen(value)));
        9: 2731:}
        -: 2732:
    #####: 2733:Value::Value(const char* begin, const char* end) {
    #####: 2734:  initBasic(stringValue, true);
    #####: 2735:  value_.string_ =
    #####: 2736:      duplicateAndPrefixStringValue(begin, static_cast<unsigned>(end - begin));
    #####: 2737:}
        -: 2738:
      105: 2739:Value::Value(const String& value) {
      105: 2740:  initBasic(stringValue, true);
      105: 2741:  value_.string_ = duplicateAndPrefixStringValue(
      105: 2742:      value.data(), static_cast<unsigned>(value.length()));
      105: 2743:}
        -: 2744:
    #####: 2745:Value::Value(const StaticString& value) {
    #####: 2746:  initBasic(stringValue);
    #####: 2747:  value_.string_ = const_cast<char*>(value.c_str());
    #####: 2748:}
        -: 2749:
       67: 2750:Value::Value(bool value) {
       67: 2751:  initBasic(booleanValue);
       67: 2752:  value_.bool_ = value;
       67: 2753:}
        -: 2754:
     1499: 2755:Value::Value(const Value& other) {
     1499: 2756:  dupPayload(other);
     1499: 2757:  dupMeta(other);
     1499: 2758:}
        -: 2759:
        7: 2760:Value::Value(Value&& other) {
        7: 2761:  initBasic(nullValue);
        7: 2762:  swap(other);
        7: 2763:}
        -: 2764:
     7110: 2765:Value::~Value() {
     2370: 2766:  releasePayload();
     2370: 2767:  value_.uint_ = 0;
     2370: 2768:}
        -: 2769:
    #####: 2770:Value& Value::operator=(const Value& other) {
    #####: 2771:  Value(other).swap(*this);
    #####: 2772:  return *this;
        -: 2773:}
        -: 2774:
      381: 2775:Value& Value::operator=(Value&& other) {
      381: 2776:  other.swap(*this);
      381: 2777:  return *this;
        -: 2778:}
        -: 2779:
      855: 2780:void Value::swapPayload(Value& other) {
      855: 2781:  std::swap(bits_, other.bits_);
      855: 2782:  std::swap(value_, other.value_);
      855: 2783:}
        -: 2784:
    #####: 2785:void Value::copyPayload(const Value& other) {
    #####: 2786:  releasePayload();
    #####: 2787:  dupPayload(other);
    #####: 2788:}
        -: 2789:
      388: 2790:void Value::swap(Value& other) {
      388: 2791:  swapPayload(other);
      388: 2792:  std::swap(comments_, other.comments_);
      388: 2793:  std::swap(start_, other.start_);
      388: 2794:  std::swap(limit_, other.limit_);
      388: 2795:}
        -: 2796:
    #####: 2797:void Value::copy(const Value& other) {
    #####: 2798:  copyPayload(other);
    #####: 2799:  dupMeta(other);
    #####: 2800:}
        -: 2801:
     9838: 2802:ValueType Value::type() const {
     9838: 2803:  return static_cast<ValueType>(bits_.value_type_);
        -: 2804:}
        -: 2805:
    #####: 2806:int Value::compare(const Value& other) const {
    #####: 2807:  if (*this < other)
    #####: 2808:    return -1;
    #####: 2809:  if (*this > other)
    #####: 2810:    return 1;
    #####: 2811:  return 0;
        -: 2812:}
        -: 2813:
    #####: 2814:bool Value::operator<(const Value& other) const {
    #####: 2815:  int typeDelta = type() - other.type();
    #####: 2816:  if (typeDelta)
    #####: 2817:    return typeDelta < 0;
    #####: 2818:  switch (type()) {
    #####: 2819:  case nullValue:
    #####: 2820:    return false;
    #####: 2821:  case intValue:
    #####: 2822:    return value_.int_ < other.value_.int_;
    #####: 2823:  case uintValue:
    #####: 2824:    return value_.uint_ < other.value_.uint_;
    #####: 2825:  case realValue:
    #####: 2826:    return value_.real_ < other.value_.real_;
    #####: 2827:  case booleanValue:
    #####: 2828:    return value_.bool_ < other.value_.bool_;
    #####: 2829:  case stringValue: {
    #####: 2830:    if ((value_.string_ == nullptr) || (other.value_.string_ == nullptr)) {
    #####: 2831:      return other.value_.string_ != nullptr;
        -: 2832:    }
        -: 2833:    unsigned this_len;
        -: 2834:    unsigned other_len;
        -: 2835:    char const* this_str;
        -: 2836:    char const* other_str;
    #####: 2837:    decodePrefixedString(this->isAllocated(), this->value_.string_, &this_len,
        -: 2838:                         &this_str);
    #####: 2839:    decodePrefixedString(other.isAllocated(), other.value_.string_, &other_len,
        -: 2840:                         &other_str);
    #####: 2841:    unsigned min_len = std::min<unsigned>(this_len, other_len);
    #####: 2842:    JSON_ASSERT(this_str && other_str);
    #####: 2843:    int comp = memcmp(this_str, other_str, min_len);
    #####: 2844:    if (comp < 0)
    #####: 2845:      return true;
    #####: 2846:    if (comp > 0)
    #####: 2847:      return false;
    #####: 2848:    return (this_len < other_len);
        -: 2849:  }
    #####: 2850:  case arrayValue:
        -: 2851:  case objectValue: {
    #####: 2852:    auto thisSize = value_.map_->size();
    #####: 2853:    auto otherSize = other.value_.map_->size();
    #####: 2854:    if (thisSize != otherSize)
    #####: 2855:      return thisSize < otherSize;
    #####: 2856:    return (*value_.map_) < (*other.value_.map_);
        -: 2857:  }
    #####: 2858:  default:
    #####: 2859:    JSON_ASSERT_UNREACHABLE;
        -: 2860:  }
        -: 2861:  return false; // unreachable
        -: 2862:}
        -: 2863:
    #####: 2864:bool Value::operator<=(const Value& other) const { return !(other < *this); }
        -: 2865:
    #####: 2866:bool Value::operator>=(const Value& other) const { return !(*this < other); }
        -: 2867:
    #####: 2868:bool Value::operator>(const Value& other) const { return other < *this; }
        -: 2869:
    #####: 2870:bool Value::operator==(const Value& other) const {
    #####: 2871:  if (type() != other.type())
    #####: 2872:    return false;
    #####: 2873:  switch (type()) {
    #####: 2874:  case nullValue:
    #####: 2875:    return true;
    #####: 2876:  case intValue:
    #####: 2877:    return value_.int_ == other.value_.int_;
    #####: 2878:  case uintValue:
    #####: 2879:    return value_.uint_ == other.value_.uint_;
    #####: 2880:  case realValue:
    #####: 2881:    return value_.real_ == other.value_.real_;
    #####: 2882:  case booleanValue:
    #####: 2883:    return value_.bool_ == other.value_.bool_;
    #####: 2884:  case stringValue: {
    #####: 2885:    if ((value_.string_ == nullptr) || (other.value_.string_ == nullptr)) {
    #####: 2886:      return (value_.string_ == other.value_.string_);
        -: 2887:    }
        -: 2888:    unsigned this_len;
        -: 2889:    unsigned other_len;
        -: 2890:    char const* this_str;
        -: 2891:    char const* other_str;
    #####: 2892:    decodePrefixedString(this->isAllocated(), this->value_.string_, &this_len,
        -: 2893:                         &this_str);
    #####: 2894:    decodePrefixedString(other.isAllocated(), other.value_.string_, &other_len,
        -: 2895:                         &other_str);
    #####: 2896:    if (this_len != other_len)
    #####: 2897:      return false;
    #####: 2898:    JSON_ASSERT(this_str && other_str);
    #####: 2899:    int comp = memcmp(this_str, other_str, this_len);
    #####: 2900:    return comp == 0;
        -: 2901:  }
    #####: 2902:  case arrayValue:
        -: 2903:  case objectValue:
    #####: 2904:    return value_.map_->size() == other.value_.map_->size() &&
    #####: 2905:           (*value_.map_) == (*other.value_.map_);
    #####: 2906:  default:
    #####: 2907:    JSON_ASSERT_UNREACHABLE;
        -: 2908:  }
        -: 2909:  return false; // unreachable
        -: 2910:}
        -: 2911:
    #####: 2912:bool Value::operator!=(const Value& other) const { return !(*this == other); }
        -: 2913:
    #####: 2914:const char* Value::asCString() const {
    #####: 2915:  JSON_ASSERT_MESSAGE(type() == stringValue,
        -: 2916:                      "in Json::Value::asCString(): requires stringValue");
    #####: 2917:  if (value_.string_ == nullptr)
    #####: 2918:    return nullptr;
        -: 2919:  unsigned this_len;
        -: 2920:  char const* this_str;
    #####: 2921:  decodePrefixedString(this->isAllocated(), this->value_.string_, &this_len,
        -: 2922:                       &this_str);
    #####: 2923:  return this_str;
        -: 2924:}
        -: 2925:
        -: 2926:#if JSONCPP_USING_SECURE_MEMORY
        -: 2927:unsigned Value::getCStringLength() const {
        -: 2928:  JSON_ASSERT_MESSAGE(type() == stringValue,
        -: 2929:                      "in Json::Value::asCString(): requires stringValue");
        -: 2930:  if (value_.string_ == 0)
        -: 2931:    return 0;
        -: 2932:  unsigned this_len;
        -: 2933:  char const* this_str;
        -: 2934:  decodePrefixedString(this->isAllocated(), this->value_.string_, &this_len,
        -: 2935:                       &this_str);
        -: 2936:  return this_len;
        -: 2937:}
        -: 2938:#endif
        -: 2939:
       60: 2940:bool Value::getString(char const** begin, char const** end) const {
       60: 2941:  if (type() != stringValue)
    #####: 2942:    return false;
       60: 2943:  if (value_.string_ == nullptr)
    #####: 2944:    return false;
        -: 2945:  unsigned length;
       60: 2946:  decodePrefixedString(this->isAllocated(), this->value_.string_, &length,
        -: 2947:                       begin);
       60: 2948:  *end = *begin + length;
       60: 2949:  return true;
        -: 2950:}
        -: 2951:
       56: 2952:String Value::asString() const {
       56: 2953:  switch (type()) {
    #####: 2954:  case nullValue:
    #####: 2955:    return "";
       50: 2956:  case stringValue: {
       50: 2957:    if (value_.string_ == nullptr)
    #####: 2958:      return "";
        -: 2959:    unsigned this_len;
        -: 2960:    char const* this_str;
       50: 2961:    decodePrefixedString(this->isAllocated(), this->value_.string_, &this_len,
        -: 2962:                         &this_str);
       50: 2963:    return String(this_str, this_len);
        -: 2964:  }
    #####: 2965:  case booleanValue:
    #####: 2966:    return value_.bool_ ? "true" : "false";
        6: 2967:  case intValue:
        6: 2968:    return valueToString(value_.int_);
    #####: 2969:  case uintValue:
    #####: 2970:    return valueToString(value_.uint_);
    #####: 2971:  case realValue:
    #####: 2972:    return valueToString(value_.real_);
    #####: 2973:  default:
    #####: 2974:    JSON_FAIL_MESSAGE("Type is not convertible to string");
        -: 2975:  }
        -: 2976:}
        -: 2977:
       32: 2978:Value::Int Value::asInt() const {
       32: 2979:  switch (type()) {
       32: 2980:  case intValue:
      32*: 2981:    JSON_ASSERT_MESSAGE(isInt(), "LargestInt out of Int range");
       32: 2982:    return Int(value_.int_);
    #####: 2983:  case uintValue:
    #####: 2984:    JSON_ASSERT_MESSAGE(isInt(), "LargestUInt out of Int range");
    #####: 2985:    return Int(value_.uint_);
    #####: 2986:  case realValue:
    #####: 2987:    JSON_ASSERT_MESSAGE(InRange(value_.real_, minInt, maxInt),
        -: 2988:                        "double out of Int range");
    #####: 2989:    return Int(value_.real_);
    #####: 2990:  case nullValue:
    #####: 2991:    return 0;
    #####: 2992:  case booleanValue:
    #####: 2993:    return value_.bool_ ? 1 : 0;
    #####: 2994:  default:
    #####: 2995:    break;
        -: 2996:  }
    #####: 2997:  JSON_FAIL_MESSAGE("Value is not convertible to Int.");
        -: 2998:}
        -: 2999:
        8: 3000:Value::UInt Value::asUInt() const {
        8: 3001:  switch (type()) {
        8: 3002:  case intValue:
       8*: 3003:    JSON_ASSERT_MESSAGE(isUInt(), "LargestInt out of UInt range");
        8: 3004:    return UInt(value_.int_);
    #####: 3005:  case uintValue:
    #####: 3006:    JSON_ASSERT_MESSAGE(isUInt(), "LargestUInt out of UInt range");
    #####: 3007:    return UInt(value_.uint_);
    #####: 3008:  case realValue:
    #####: 3009:    JSON_ASSERT_MESSAGE(InRange(value_.real_, 0, maxUInt),
        -: 3010:                        "double out of UInt range");
    #####: 3011:    return UInt(value_.real_);
    #####: 3012:  case nullValue:
    #####: 3013:    return 0;
    #####: 3014:  case booleanValue:
    #####: 3015:    return value_.bool_ ? 1 : 0;
    #####: 3016:  default:
    #####: 3017:    break;
        -: 3018:  }
    #####: 3019:  JSON_FAIL_MESSAGE("Value is not convertible to UInt.");
        -: 3020:}
        -: 3021:
        -: 3022:#if defined(JSON_HAS_INT64)
        -: 3023:
      160: 3024:Value::Int64 Value::asInt64() const {
      160: 3025:  switch (type()) {
      160: 3026:  case intValue:
      160: 3027:    return Int64(value_.int_);
    #####: 3028:  case uintValue:
    #####: 3029:    JSON_ASSERT_MESSAGE(isInt64(), "LargestUInt out of Int64 range");
    #####: 3030:    return Int64(value_.uint_);
    #####: 3031:  case realValue:
    #####: 3032:    JSON_ASSERT_MESSAGE(InRange(value_.real_, minInt64, maxInt64),
        -: 3033:                        "double out of Int64 range");
    #####: 3034:    return Int64(value_.real_);
    #####: 3035:  case nullValue:
    #####: 3036:    return 0;
    #####: 3037:  case booleanValue:
    #####: 3038:    return value_.bool_ ? 1 : 0;
    #####: 3039:  default:
    #####: 3040:    break;
        -: 3041:  }
    #####: 3042:  JSON_FAIL_MESSAGE("Value is not convertible to Int64.");
        -: 3043:}
        -: 3044:
    #####: 3045:Value::UInt64 Value::asUInt64() const {
    #####: 3046:  switch (type()) {
    #####: 3047:  case intValue:
    #####: 3048:    JSON_ASSERT_MESSAGE(isUInt64(), "LargestInt out of UInt64 range");
    #####: 3049:    return UInt64(value_.int_);
    #####: 3050:  case uintValue:
    #####: 3051:    return UInt64(value_.uint_);
    #####: 3052:  case realValue:
    #####: 3053:    JSON_ASSERT_MESSAGE(InRange(value_.real_, 0, maxUInt64),
        -: 3054:                        "double out of UInt64 range");
    #####: 3055:    return UInt64(value_.real_);
    #####: 3056:  case nullValue:
    #####: 3057:    return 0;
    #####: 3058:  case booleanValue:
    #####: 3059:    return value_.bool_ ? 1 : 0;
    #####: 3060:  default:
    #####: 3061:    break;
        -: 3062:  }
    #####: 3063:  JSON_FAIL_MESSAGE("Value is not convertible to UInt64.");
        -: 3064:}
        -: 3065:#endif // if defined(JSON_HAS_INT64)
        -: 3066:
      160: 3067:LargestInt Value::asLargestInt() const {
        -: 3068:#if defined(JSON_NO_INT64)
        -: 3069:  return asInt();
        -: 3070:#else
      160: 3071:  return asInt64();
        -: 3072:#endif
        -: 3073:}
        -: 3074:
    #####: 3075:LargestUInt Value::asLargestUInt() const {
        -: 3076:#if defined(JSON_NO_INT64)
        -: 3077:  return asUInt();
        -: 3078:#else
    #####: 3079:  return asUInt64();
        -: 3080:#endif
        -: 3081:}
        -: 3082:
    #####: 3083:double Value::asDouble() const {
    #####: 3084:  switch (type()) {
    #####: 3085:  case intValue:
    #####: 3086:    return static_cast<double>(value_.int_);
    #####: 3087:  case uintValue:
        -: 3088:#if !defined(JSON_USE_INT64_DOUBLE_CONVERSION)
        -: 3089:    return static_cast<double>(value_.uint_);
        -: 3090:#else  // if !defined(JSON_USE_INT64_DOUBLE_CONVERSION)
    #####: 3091:    return integerToDouble(value_.uint_);
        -: 3092:#endif // if !defined(JSON_USE_INT64_DOUBLE_CONVERSION)
    #####: 3093:  case realValue:
    #####: 3094:    return value_.real_;
    #####: 3095:  case nullValue:
    #####: 3096:    return 0.0;
    #####: 3097:  case booleanValue:
    #####: 3098:    return value_.bool_ ? 1.0 : 0.0;
    #####: 3099:  default:
    #####: 3100:    break;
        -: 3101:  }
    #####: 3102:  JSON_FAIL_MESSAGE("Value is not convertible to double.");
        -: 3103:}
        -: 3104:
    #####: 3105:float Value::asFloat() const {
    #####: 3106:  switch (type()) {
    #####: 3107:  case intValue:
    #####: 3108:    return static_cast<float>(value_.int_);
    #####: 3109:  case uintValue:
        -: 3110:#if !defined(JSON_USE_INT64_DOUBLE_CONVERSION)
        -: 3111:    return static_cast<float>(value_.uint_);
        -: 3112:#else  // if !defined(JSON_USE_INT64_DOUBLE_CONVERSION)
        -: 3113:    // This can fail (silently?) if the value is bigger than MAX_FLOAT.
    #####: 3114:    return static_cast<float>(integerToDouble(value_.uint_));
        -: 3115:#endif // if !defined(JSON_USE_INT64_DOUBLE_CONVERSION)
    #####: 3116:  case realValue:
    #####: 3117:    return static_cast<float>(value_.real_);
    #####: 3118:  case nullValue:
    #####: 3119:    return 0.0;
    #####: 3120:  case booleanValue:
    #####: 3121:    return value_.bool_ ? 1.0F : 0.0F;
    #####: 3122:  default:
    #####: 3123:    break;
        -: 3124:  }
    #####: 3125:  JSON_FAIL_MESSAGE("Value is not convertible to float.");
        -: 3126:}
        -: 3127:
       67: 3128:bool Value::asBool() const {
       67: 3129:  switch (type()) {
       67: 3130:  case booleanValue:
       67: 3131:    return value_.bool_;
    #####: 3132:  case nullValue:
    #####: 3133:    return false;
    #####: 3134:  case intValue:
    #####: 3135:    return value_.int_ != 0;
    #####: 3136:  case uintValue:
    #####: 3137:    return value_.uint_ != 0;
    #####: 3138:  case realValue: {
        -: 3139:    // According to JavaScript language zero or NaN is regarded as false
    #####: 3140:    const auto value_classification = std::fpclassify(value_.real_);
    #####: 3141:    return value_classification != FP_ZERO && value_classification != FP_NAN;
        -: 3142:  }
    #####: 3143:  default:
    #####: 3144:    break;
        -: 3145:  }
    #####: 3146:  JSON_FAIL_MESSAGE("Value is not convertible to bool.");
        -: 3147:}
        -: 3148:
    #####: 3149:bool Value::isConvertibleTo(ValueType other) const {
    #####: 3150:  switch (other) {
    #####: 3151:  case nullValue:
    #####: 3152:    return (isNumeric() && asDouble() == 0.0) ||
    #####: 3153:           (type() == booleanValue && !value_.bool_) ||
    #####: 3154:           (type() == stringValue && asString().empty()) ||
    #####: 3155:           (type() == arrayValue && value_.map_->empty()) ||
    #####: 3156:           (type() == objectValue && value_.map_->empty()) ||
    #####: 3157:           type() == nullValue;
    #####: 3158:  case intValue:
    #####: 3159:    return isInt() ||
    #####: 3160:           (type() == realValue && InRange(value_.real_, minInt, maxInt)) ||
    #####: 3161:           type() == booleanValue || type() == nullValue;
    #####: 3162:  case uintValue:
    #####: 3163:    return isUInt() ||
    #####: 3164:           (type() == realValue && InRange(value_.real_, 0, maxUInt)) ||
    #####: 3165:           type() == booleanValue || type() == nullValue;
    #####: 3166:  case realValue:
    #####: 3167:    return isNumeric() || type() == booleanValue || type() == nullValue;
    #####: 3168:  case booleanValue:
    #####: 3169:    return isNumeric() || type() == booleanValue || type() == nullValue;
    #####: 3170:  case stringValue:
    #####: 3171:    return isNumeric() || type() == booleanValue || type() == stringValue ||
    #####: 3172:           type() == nullValue;
    #####: 3173:  case arrayValue:
    #####: 3174:    return type() == arrayValue || type() == nullValue;
    #####: 3175:  case objectValue:
    #####: 3176:    return type() == objectValue || type() == nullValue;
        -: 3177:  }
    #####: 3178:  JSON_ASSERT_UNREACHABLE;
        -: 3179:  return false;
        -: 3180:}
        -: 3181:
        -: 3182:/// Number of values in array or object
       14: 3183:ArrayIndex Value::size() const {
       14: 3184:  switch (type()) {
    #####: 3185:  case nullValue:
        -: 3186:  case intValue:
        -: 3187:  case uintValue:
        -: 3188:  case realValue:
        -: 3189:  case booleanValue:
        -: 3190:  case stringValue:
    #####: 3191:    return 0;
       14: 3192:  case arrayValue: // size of the array is highest index + 1
       14: 3193:    if (!value_.map_->empty()) {
       13: 3194:      ObjectValues::const_iterator itLast = value_.map_->end();
       13: 3195:      --itLast;
       13: 3196:      return (*itLast).first.index() + 1;
        -: 3197:    }
        1: 3198:    return 0;
    #####: 3199:  case objectValue:
    #####: 3200:    return ArrayIndex(value_.map_->size());
        -: 3201:  }
    #####: 3202:  JSON_ASSERT_UNREACHABLE;
        -: 3203:  return 0; // unreachable;
        -: 3204:}
        -: 3205:
    #####: 3206:bool Value::empty() const {
    #####: 3207:  if (isNull() || isArray() || isObject())
    #####: 3208:    return size() == 0U;
    #####: 3209:  return false;
        -: 3210:}
        -: 3211:
    #####: 3212:Value::operator bool() const { return !isNull(); }
        -: 3213:
    #####: 3214:void Value::clear() {
    #####: 3215:  JSON_ASSERT_MESSAGE(type() == nullValue || type() == arrayValue ||
        -: 3216:                          type() == objectValue,
        -: 3217:                      "in Json::Value::clear(): requires complex value");
    #####: 3218:  start_ = 0;
    #####: 3219:  limit_ = 0;
    #####: 3220:  switch (type()) {
    #####: 3221:  case arrayValue:
        -: 3222:  case objectValue:
    #####: 3223:    value_.map_->clear();
    #####: 3224:    break;
    #####: 3225:  default:
    #####: 3226:    break;
        -: 3227:  }
    #####: 3228:}
        -: 3229:
    #####: 3230:void Value::resize(ArrayIndex newSize) {
    #####: 3231:  JSON_ASSERT_MESSAGE(type() == nullValue || type() == arrayValue,
        -: 3232:                      "in Json::Value::resize(): requires arrayValue");
    #####: 3233:  if (type() == nullValue)
    #####: 3234:    *this = Value(arrayValue);
    #####: 3235:  ArrayIndex oldSize = size();
    #####: 3236:  if (newSize == 0)
    #####: 3237:    clear();
    #####: 3238:  else if (newSize > oldSize)
    #####: 3239:    this->operator[](newSize - 1);
        -: 3240:  else {
    #####: 3241:    for (ArrayIndex index = newSize; index < oldSize; ++index) {
    #####: 3242:      value_.map_->erase(index);
        -: 3243:    }
    #####: 3244:    JSON_ASSERT(size() == newSize);
        -: 3245:  }
    #####: 3246:}
        -: 3247:
      128: 3248:Value& Value::operator[](ArrayIndex index) {
     128*: 3249:  JSON_ASSERT_MESSAGE(
        -: 3250:      type() == nullValue || type() == arrayValue,
        -: 3251:      "in Json::Value::operator[](ArrayIndex): requires arrayValue");
      128: 3252:  if (type() == nullValue)
    #####: 3253:    *this = Value(arrayValue);
      256: 3254:  CZString key(index);
      128: 3255:  auto it = value_.map_->lower_bound(key);
      128: 3256:  if (it != value_.map_->end() && (*it).first == key)
       95: 3257:    return (*it).second;
        -: 3258:
       33: 3259:  ObjectValues::value_type defaultValue(key, nullSingleton());
       33: 3260:  it = value_.map_->insert(it, defaultValue);
       33: 3261:  return (*it).second;
        -: 3262:}
        -: 3263:
      128: 3264:Value& Value::operator[](int index) {
     128*: 3265:  JSON_ASSERT_MESSAGE(
        -: 3266:      index >= 0,
        -: 3267:      "in Json::Value::operator[](int index): index cannot be negative");
      128: 3268:  return (*this)[ArrayIndex(index)];
        -: 3269:}
        -: 3270:
       20: 3271:const Value& Value::operator[](ArrayIndex index) const {
      20*: 3272:  JSON_ASSERT_MESSAGE(
        -: 3273:      type() == nullValue || type() == arrayValue,
        -: 3274:      "in Json::Value::operator[](ArrayIndex)const: requires arrayValue");
       20: 3275:  if (type() == nullValue)
    #####: 3276:    return nullSingleton();
       40: 3277:  CZString key(index);
       20: 3278:  ObjectValues::const_iterator it = value_.map_->find(key);
       20: 3279:  if (it == value_.map_->end())
    #####: 3280:    return nullSingleton();
       20: 3281:  return (*it).second;
        -: 3282:}
        -: 3283:
    #####: 3284:const Value& Value::operator[](int index) const {
    #####: 3285:  JSON_ASSERT_MESSAGE(
        -: 3286:      index >= 0,
        -: 3287:      "in Json::Value::operator[](int index) const: index cannot be negative");
    #####: 3288:  return (*this)[ArrayIndex(index)];
        -: 3289:}
        -: 3290:
      871: 3291:void Value::initBasic(ValueType type, bool allocated) {
      871: 3292:  setType(type);
      871: 3293:  setIsAllocated(allocated);
      871: 3294:  comments_ = Comments{};
      871: 3295:  start_ = 0;
      871: 3296:  limit_ = 0;
      871: 3297:}
        -: 3298:
     1499: 3299:void Value::dupPayload(const Value& other) {
     1499: 3300:  setType(other.type());
     1499: 3301:  setIsAllocated(false);
     1499: 3302:  switch (type()) {
     1334: 3303:  case nullValue:
        -: 3304:  case intValue:
        -: 3305:  case uintValue:
        -: 3306:  case realValue:
        -: 3307:  case booleanValue:
     1334: 3308:    value_ = other.value_;
     1334: 3309:    break;
       81: 3310:  case stringValue:
      81*: 3311:    if (other.value_.string_ && other.isAllocated()) {
        -: 3312:      unsigned len;
        -: 3313:      char const* str;
       81: 3314:      decodePrefixedString(other.isAllocated(), other.value_.string_, &len,
        -: 3315:                           &str);
       81: 3316:      value_.string_ = duplicateAndPrefixStringValue(str, len);
       81: 3317:      setIsAllocated(true);
        -: 3318:    } else {
    #####: 3319:      value_.string_ = other.value_.string_;
        -: 3320:    }
       81: 3321:    break;
       84: 3322:  case arrayValue:
        -: 3323:  case objectValue:
       84: 3324:    value_.map_ = new ObjectValues(*other.value_.map_);
       84: 3325:    break;
    #####: 3326:  default:
    #####: 3327:    JSON_ASSERT_UNREACHABLE;
        -: 3328:  }
     1499: 3329:}
        -: 3330:
     2370: 3331:void Value::releasePayload() {
     2370: 3332:  switch (type()) {
     1975: 3333:  case nullValue:
        -: 3334:  case intValue:
        -: 3335:  case uintValue:
        -: 3336:  case realValue:
        -: 3337:  case booleanValue:
     1975: 3338:    break;
      195: 3339:  case stringValue:
      195: 3340:    if (isAllocated())
      195: 3341:      releasePrefixedStringValue(value_.string_);
      195: 3342:    break;
      200: 3343:  case arrayValue:
        -: 3344:  case objectValue:
      200: 3345:    delete value_.map_;
      200: 3346:    break;
    #####: 3347:  default:
    #####: 3348:    JSON_ASSERT_UNREACHABLE;
        -: 3349:  }
     2370: 3350:}
        -: 3351:
     1499: 3352:void Value::dupMeta(const Value& other) {
     1499: 3353:  comments_ = other.comments_;
     1499: 3354:  start_ = other.start_;
     1499: 3355:  limit_ = other.limit_;
     1499: 3356:}
        -: 3357:
        -: 3358:// Access an object value by name, create a null member if it does not exist.
        -: 3359:// @pre Type of '*this' is object or null.
        -: 3360:// @param key is null-terminated.
    #####: 3361:Value& Value::resolveReference(const char* key) {
    #####: 3362:  JSON_ASSERT_MESSAGE(
        -: 3363:      type() == nullValue || type() == objectValue,
        -: 3364:      "in Json::Value::resolveReference(): requires objectValue");
    #####: 3365:  if (type() == nullValue)
    #####: 3366:    *this = Value(objectValue);
    #####: 3367:  CZString actualKey(key, static_cast<unsigned>(strlen(key)),
    #####: 3368:                     CZString::noDuplication); // NOTE!
    #####: 3369:  auto it = value_.map_->lower_bound(actualKey);
    #####: 3370:  if (it != value_.map_->end() && (*it).first == actualKey)
    #####: 3371:    return (*it).second;
        -: 3372:
    #####: 3373:  ObjectValues::value_type defaultValue(actualKey, nullSingleton());
    #####: 3374:  it = value_.map_->insert(it, defaultValue);
    #####: 3375:  Value& value = (*it).second;
    #####: 3376:  return value;
        -: 3377:}
        -: 3378:
        -: 3379:// @param key is not null-terminated.
      695: 3380:Value& Value::resolveReference(char const* key, char const* end) {
     695*: 3381:  JSON_ASSERT_MESSAGE(
        -: 3382:      type() == nullValue || type() == objectValue,
        -: 3383:      "in Json::Value::resolveReference(key, end): requires objectValue");
      695: 3384:  if (type() == nullValue)
       11: 3385:    *this = Value(objectValue);
      695: 3386:  CZString actualKey(key, static_cast<unsigned>(end - key),
     1390: 3387:                     CZString::duplicateOnCopy);
      695: 3388:  auto it = value_.map_->lower_bound(actualKey);
      695: 3389:  if (it != value_.map_->end() && (*it).first == actualKey)
      169: 3390:    return (*it).second;
        -: 3391:
      526: 3392:  ObjectValues::value_type defaultValue(actualKey, nullSingleton());
      526: 3393:  it = value_.map_->insert(it, defaultValue);
      526: 3394:  Value& value = (*it).second;
      526: 3395:  return value;
        -: 3396:}
        -: 3397:
    #####: 3398:Value Value::get(ArrayIndex index, const Value& defaultValue) const {
    #####: 3399:  const Value* value = &((*this)[index]);
    #####: 3400:  return value == &nullSingleton() ? defaultValue : *value;
        -: 3401:}
        -: 3402:
    #####: 3403:bool Value::isValidIndex(ArrayIndex index) const { return index < size(); }
        -: 3404:
      344: 3405:Value const* Value::find(char const* begin, char const* end) const {
     344*: 3406:  JSON_ASSERT_MESSAGE(type() == nullValue || type() == objectValue,
        -: 3407:                      "in Json::Value::find(begin, end): requires "
        -: 3408:                      "objectValue or nullValue");
      344: 3409:  if (type() == nullValue)
    #####: 3410:    return nullptr;
      344: 3411:  CZString actualKey(begin, static_cast<unsigned>(end - begin),
      688: 3412:                     CZString::noDuplication);
      344: 3413:  ObjectValues::const_iterator it = value_.map_->find(actualKey);
      344: 3414:  if (it == value_.map_->end())
    #####: 3415:    return nullptr;
      344: 3416:  return &(*it).second;
        -: 3417:}
    #####: 3418:Value* Value::demand(char const* begin, char const* end) {
    #####: 3419:  JSON_ASSERT_MESSAGE(type() == nullValue || type() == objectValue,
        -: 3420:                      "in Json::Value::demand(begin, end): requires "
        -: 3421:                      "objectValue or nullValue");
    #####: 3422:  return &resolveReference(begin, end);
        -: 3423:}
       84: 3424:const Value& Value::operator[](const char* key) const {
       84: 3425:  Value const* found = find(key, key + strlen(key));
       84: 3426:  if (!found)
    #####: 3427:    return nullSingleton();
       84: 3428:  return *found;
        -: 3429:}
      260: 3430:Value const& Value::operator[](const String& key) const {
      260: 3431:  Value const* found = find(key.data(), key.data() + key.length());
      260: 3432:  if (!found)
    #####: 3433:    return nullSingleton();
      260: 3434:  return *found;
        -: 3435:}
        -: 3436:
      266: 3437:Value& Value::operator[](const char* key) {
      266: 3438:  return resolveReference(key, key + strlen(key));
        -: 3439:}
        -: 3440:
      429: 3441:Value& Value::operator[](const String& key) {
      429: 3442:  return resolveReference(key.data(), key.data() + key.length());
        -: 3443:}
        -: 3444:
    #####: 3445:Value& Value::operator[](const StaticString& key) {
    #####: 3446:  return resolveReference(key.c_str());
        -: 3447:}
        -: 3448:
        7: 3449:Value& Value::append(const Value& value) { return append(Value(value)); }
        -: 3450:
        7: 3451:Value& Value::append(Value&& value) {
       7*: 3452:  JSON_ASSERT_MESSAGE(type() == nullValue || type() == arrayValue,
        -: 3453:                      "in Json::Value::append: requires arrayValue");
        7: 3454:  if (type() == nullValue) {
        1: 3455:    *this = Value(arrayValue);
        -: 3456:  }
        7: 3457:  return this->value_.map_->emplace(size(), std::move(value)).first->second;
        -: 3458:}
        -: 3459:
    #####: 3460:bool Value::insert(ArrayIndex index, const Value& newValue) {
    #####: 3461:  return insert(index, Value(newValue));
        -: 3462:}
        -: 3463:
    #####: 3464:bool Value::insert(ArrayIndex index, Value&& newValue) {
    #####: 3465:  JSON_ASSERT_MESSAGE(type() == nullValue || type() == arrayValue,
        -: 3466:                      "in Json::Value::insert: requires arrayValue");
    #####: 3467:  ArrayIndex length = size();
    #####: 3468:  if (index > length) {
    #####: 3469:    return false;
        -: 3470:  }
    #####: 3471:  for (ArrayIndex i = length; i > index; i--) {
    #####: 3472:    (*this)[i] = std::move((*this)[i - 1]);
        -: 3473:  }
    #####: 3474:  (*this)[index] = std::move(newValue);
    #####: 3475:  return true;
        -: 3476:}
        -: 3477:
    #####: 3478:Value Value::get(char const* begin, char const* end,
        -: 3479:                 Value const& defaultValue) const {
    #####: 3480:  Value const* found = find(begin, end);
    #####: 3481:  return !found ? defaultValue : *found;
        -: 3482:}
    #####: 3483:Value Value::get(char const* key, Value const& defaultValue) const {
    #####: 3484:  return get(key, key + strlen(key), defaultValue);
        -: 3485:}
    #####: 3486:Value Value::get(String const& key, Value const& defaultValue) const {
    #####: 3487:  return get(key.data(), key.data() + key.length(), defaultValue);
        -: 3488:}
        -: 3489:
    #####: 3490:bool Value::removeMember(const char* begin, const char* end, Value* removed) {
    #####: 3491:  if (type() != objectValue) {
    #####: 3492:    return false;
        -: 3493:  }
    #####: 3494:  CZString actualKey(begin, static_cast<unsigned>(end - begin),
    #####: 3495:                     CZString::noDuplication);
    #####: 3496:  auto it = value_.map_->find(actualKey);
    #####: 3497:  if (it == value_.map_->end())
    #####: 3498:    return false;
    #####: 3499:  if (removed)
    #####: 3500:    *removed = std::move(it->second);
    #####: 3501:  value_.map_->erase(it);
    #####: 3502:  return true;
        -: 3503:}
    #####: 3504:bool Value::removeMember(const char* key, Value* removed) {
    #####: 3505:  return removeMember(key, key + strlen(key), removed);
        -: 3506:}
    #####: 3507:bool Value::removeMember(String const& key, Value* removed) {
    #####: 3508:  return removeMember(key.data(), key.data() + key.length(), removed);
        -: 3509:}
    #####: 3510:void Value::removeMember(const char* key) {
    #####: 3511:  JSON_ASSERT_MESSAGE(type() == nullValue || type() == objectValue,
        -: 3512:                      "in Json::Value::removeMember(): requires objectValue");
    #####: 3513:  if (type() == nullValue)
    #####: 3514:    return;
        -: 3515:
    #####: 3516:  CZString actualKey(key, unsigned(strlen(key)), CZString::noDuplication);
    #####: 3517:  value_.map_->erase(actualKey);
        -: 3518:}
    #####: 3519:void Value::removeMember(const String& key) { removeMember(key.c_str()); }
        -: 3520:
    #####: 3521:bool Value::removeIndex(ArrayIndex index, Value* removed) {
    #####: 3522:  if (type() != arrayValue) {
    #####: 3523:    return false;
        -: 3524:  }
    #####: 3525:  CZString key(index);
    #####: 3526:  auto it = value_.map_->find(key);
    #####: 3527:  if (it == value_.map_->end()) {
    #####: 3528:    return false;
        -: 3529:  }
    #####: 3530:  if (removed)
    #####: 3531:    *removed = it->second;
    #####: 3532:  ArrayIndex oldSize = size();
        -: 3533:  // shift left all items left, into the place of the "removed"
    #####: 3534:  for (ArrayIndex i = index; i < (oldSize - 1); ++i) {
    #####: 3535:    CZString keey(i);
    #####: 3536:    (*value_.map_)[keey] = (*this)[i + 1];
        -: 3537:  }
        -: 3538:  // erase the last one ("leftover")
    #####: 3539:  CZString keyLast(oldSize - 1);
    #####: 3540:  auto itLast = value_.map_->find(keyLast);
    #####: 3541:  value_.map_->erase(itLast);
    #####: 3542:  return true;
        -: 3543:}
        -: 3544:
    #####: 3545:bool Value::isMember(char const* begin, char const* end) const {
    #####: 3546:  Value const* value = find(begin, end);
    #####: 3547:  return nullptr != value;
        -: 3548:}
    #####: 3549:bool Value::isMember(char const* key) const {
    #####: 3550:  return isMember(key, key + strlen(key));
        -: 3551:}
    #####: 3552:bool Value::isMember(String const& key) const {
    #####: 3553:  return isMember(key.data(), key.data() + key.length());
        -: 3554:}
        -: 3555:
       60: 3556:Value::Members Value::getMemberNames() const {
      60*: 3557:  JSON_ASSERT_MESSAGE(
        -: 3558:      type() == nullValue || type() == objectValue,
        -: 3559:      "in Json::Value::getMemberNames(), value must be objectValue");
       60: 3560:  if (type() == nullValue)
    #####: 3561:    return Value::Members();
      120: 3562:  Members members;
       60: 3563:  members.reserve(value_.map_->size());
       60: 3564:  ObjectValues::const_iterator it = value_.map_->begin();
       60: 3565:  ObjectValues::const_iterator itEnd = value_.map_->end();
      320: 3566:  for (; it != itEnd; ++it) {
      260: 3567:    members.push_back(String((*it).first.data(), (*it).first.length()));
        -: 3568:  }
       60: 3569:  return members;
        -: 3570:}
        -: 3571:
    #####: 3572:static bool IsIntegral(double d) {
        -: 3573:  double integral_part;
    #####: 3574:  return modf(d, &integral_part) == 0.0;
        -: 3575:}
        -: 3576:
    #####: 3577:bool Value::isNull() const { return type() == nullValue; }
        -: 3578:
    #####: 3579:bool Value::isBool() const { return type() == booleanValue; }
        -: 3580:
       32: 3581:bool Value::isInt() const {
       32: 3582:  switch (type()) {
       32: 3583:  case intValue:
        -: 3584:#if defined(JSON_HAS_INT64)
      32*: 3585:    return value_.int_ >= minInt && value_.int_ <= maxInt;
        -: 3586:#else
        -: 3587:    return true;
        -: 3588:#endif
    #####: 3589:  case uintValue:
    #####: 3590:    return value_.uint_ <= UInt(maxInt);
    #####: 3591:  case realValue:
    #####: 3592:    return value_.real_ >= minInt && value_.real_ <= maxInt &&
    #####: 3593:           IsIntegral(value_.real_);
    #####: 3594:  default:
    #####: 3595:    break;
        -: 3596:  }
    #####: 3597:  return false;
        -: 3598:}
        -: 3599:
        8: 3600:bool Value::isUInt() const {
        8: 3601:  switch (type()) {
        8: 3602:  case intValue:
        -: 3603:#if defined(JSON_HAS_INT64)
       8*: 3604:    return value_.int_ >= 0 && LargestUInt(value_.int_) <= LargestUInt(maxUInt);
        -: 3605:#else
        -: 3606:    return value_.int_ >= 0;
        -: 3607:#endif
    #####: 3608:  case uintValue:
        -: 3609:#if defined(JSON_HAS_INT64)
    #####: 3610:    return value_.uint_ <= maxUInt;
        -: 3611:#else
        -: 3612:    return true;
        -: 3613:#endif
    #####: 3614:  case realValue:
    #####: 3615:    return value_.real_ >= 0 && value_.real_ <= maxUInt &&
    #####: 3616:           IsIntegral(value_.real_);
    #####: 3617:  default:
    #####: 3618:    break;
        -: 3619:  }
    #####: 3620:  return false;
        -: 3621:}
        -: 3622:
    #####: 3623:bool Value::isInt64() const {
        -: 3624:#if defined(JSON_HAS_INT64)
    #####: 3625:  switch (type()) {
    #####: 3626:  case intValue:
    #####: 3627:    return true;
    #####: 3628:  case uintValue:
    #####: 3629:    return value_.uint_ <= UInt64(maxInt64);
    #####: 3630:  case realValue:
        -: 3631:    // Note that maxInt64 (= 2^63 - 1) is not exactly representable as a
        -: 3632:    // double, so double(maxInt64) will be rounded up to 2^63. Therefore we
        -: 3633:    // require the value to be strictly less than the limit.
    #####: 3634:    return value_.real_ >= double(minInt64) &&
    #####: 3635:           value_.real_ < double(maxInt64) && IsIntegral(value_.real_);
    #####: 3636:  default:
    #####: 3637:    break;
        -: 3638:  }
        -: 3639:#endif // JSON_HAS_INT64
    #####: 3640:  return false;
        -: 3641:}
        -: 3642:
    #####: 3643:bool Value::isUInt64() const {
        -: 3644:#if defined(JSON_HAS_INT64)
    #####: 3645:  switch (type()) {
    #####: 3646:  case intValue:
    #####: 3647:    return value_.int_ >= 0;
    #####: 3648:  case uintValue:
    #####: 3649:    return true;
    #####: 3650:  case realValue:
        -: 3651:    // Note that maxUInt64 (= 2^64 - 1) is not exactly representable as a
        -: 3652:    // double, so double(maxUInt64) will be rounded up to 2^64. Therefore we
        -: 3653:    // require the value to be strictly less than the limit.
    #####: 3654:    return value_.real_ >= 0 && value_.real_ < maxUInt64AsDouble &&
    #####: 3655:           IsIntegral(value_.real_);
    #####: 3656:  default:
    #####: 3657:    break;
        -: 3658:  }
        -: 3659:#endif // JSON_HAS_INT64
    #####: 3660:  return false;
        -: 3661:}
        -: 3662:
    #####: 3663:bool Value::isIntegral() const {
    #####: 3664:  switch (type()) {
    #####: 3665:  case intValue:
        -: 3666:  case uintValue:
    #####: 3667:    return true;
    #####: 3668:  case realValue:
        -: 3669:#if defined(JSON_HAS_INT64)
        -: 3670:    // Note that maxUInt64 (= 2^64 - 1) is not exactly representable as a
        -: 3671:    // double, so double(maxUInt64) will be rounded up to 2^64. Therefore we
        -: 3672:    // require the value to be strictly less than the limit.
    #####: 3673:    return value_.real_ >= double(minInt64) &&
    #####: 3674:           value_.real_ < maxUInt64AsDouble && IsIntegral(value_.real_);
        -: 3675:#else
        -: 3676:    return value_.real_ >= minInt && value_.real_ <= maxUInt &&
        -: 3677:           IsIntegral(value_.real_);
        -: 3678:#endif // JSON_HAS_INT64
    #####: 3679:  default:
    #####: 3680:    break;
        -: 3681:  }
    #####: 3682:  return false;
        -: 3683:}
        -: 3684:
    #####: 3685:bool Value::isDouble() const {
    #####: 3686:  return type() == intValue || type() == uintValue || type() == realValue;
        -: 3687:}
        -: 3688:
    #####: 3689:bool Value::isNumeric() const { return isDouble(); }
        -: 3690:
    #####: 3691:bool Value::isString() const { return type() == stringValue; }
        -: 3692:
    #####: 3693:bool Value::isArray() const { return type() == arrayValue; }
        -: 3694:
    #####: 3695:bool Value::isObject() const { return type() == objectValue; }
        -: 3696:
    #####: 3697:Value::Comments::Comments(const Comments& that)
    #####: 3698:    : ptr_{cloneUnique(that.ptr_)} {}
        -: 3699:
      388: 3700:Value::Comments::Comments(Comments&& that) : ptr_{std::move(that.ptr_)} {}
        -: 3701:
     1499: 3702:Value::Comments& Value::Comments::operator=(const Comments& that) {
     1499: 3703:  ptr_ = cloneUnique(that.ptr_);
     1499: 3704:  return *this;
        -: 3705:}
        -: 3706:
     1647: 3707:Value::Comments& Value::Comments::operator=(Comments&& that) {
     1647: 3708:  ptr_ = std::move(that.ptr_);
     1647: 3709:  return *this;
        -: 3710:}
        -: 3711:
      849: 3712:bool Value::Comments::has(CommentPlacement slot) const {
     849*: 3713:  return ptr_ && !(*ptr_)[slot].empty();
        -: 3714:}
        -: 3715:
    #####: 3716:String Value::Comments::get(CommentPlacement slot) const {
    #####: 3717:  if (!ptr_)
    #####: 3718:    return {};
    #####: 3719:  return (*ptr_)[slot];
        -: 3720:}
        -: 3721:
    #####: 3722:void Value::Comments::set(CommentPlacement slot, String comment) {
    #####: 3723:  if (!ptr_) {
    #####: 3724:    ptr_ = std::unique_ptr<Array>(new Array());
        -: 3725:  }
        -: 3726:  // check comments array boundry.
    #####: 3727:  if (slot < CommentPlacement::numberOfCommentPlacement) {
    #####: 3728:    (*ptr_)[slot] = std::move(comment);
        -: 3729:  }
    #####: 3730:}
        -: 3731:
    #####: 3732:void Value::setComment(String comment, CommentPlacement placement) {
    #####: 3733:  if (!comment.empty() && (comment.back() == '\n')) {
        -: 3734:    // Always discard trailing newline, to aid indentation.
    #####: 3735:    comment.pop_back();
        -: 3736:  }
    #####: 3737:  JSON_ASSERT(!comment.empty());
    #####: 3738:  JSON_ASSERT_MESSAGE(
        -: 3739:      comment[0] == '\0' || comment[0] == '/',
        -: 3740:      "in Json::Value::setComment(): Comments must start with /");
    #####: 3741:  comments_.set(placement, std::move(comment));
    #####: 3742:}
        -: 3743:
      849: 3744:bool Value::hasComment(CommentPlacement placement) const {
      849: 3745:  return comments_.has(placement);
        -: 3746:}
        -: 3747:
    #####: 3748:String Value::getComment(CommentPlacement placement) const {
    #####: 3749:  return comments_.get(placement);
        -: 3750:}
        -: 3751:
      467: 3752:void Value::setOffsetStart(ptrdiff_t start) { start_ = start; }
        -: 3753:
      467: 3754:void Value::setOffsetLimit(ptrdiff_t limit) { limit_ = limit; }
        -: 3755:
    #####: 3756:ptrdiff_t Value::getOffsetStart() const { return start_; }
        -: 3757:
    #####: 3758:ptrdiff_t Value::getOffsetLimit() const { return limit_; }
        -: 3759:
    #####: 3760:String Value::toStyledString() const {
    #####: 3761:  StreamWriterBuilder builder;
        -: 3762:
    #####: 3763:  String out = this->hasComment(commentBefore) ? "\n" : "";
    #####: 3764:  out += Json::writeString(builder, *this);
    #####: 3765:  out += '\n';
        -: 3766:
    #####: 3767:  return out;
        -: 3768:}
        -: 3769:
    #####: 3770:Value::const_iterator Value::begin() const {
    #####: 3771:  switch (type()) {
    #####: 3772:  case arrayValue:
        -: 3773:  case objectValue:
    #####: 3774:    if (value_.map_)
    #####: 3775:      return const_iterator(value_.map_->begin());
    #####: 3776:    break;
    #####: 3777:  default:
    #####: 3778:    break;
        -: 3779:  }
    #####: 3780:  return {};
        -: 3781:}
        -: 3782:
    #####: 3783:Value::const_iterator Value::end() const {
    #####: 3784:  switch (type()) {
    #####: 3785:  case arrayValue:
        -: 3786:  case objectValue:
    #####: 3787:    if (value_.map_)
    #####: 3788:      return const_iterator(value_.map_->end());
    #####: 3789:    break;
    #####: 3790:  default:
    #####: 3791:    break;
        -: 3792:  }
    #####: 3793:  return {};
        -: 3794:}
        -: 3795:
    #####: 3796:Value::iterator Value::begin() {
    #####: 3797:  switch (type()) {
    #####: 3798:  case arrayValue:
        -: 3799:  case objectValue:
    #####: 3800:    if (value_.map_)
    #####: 3801:      return iterator(value_.map_->begin());
    #####: 3802:    break;
    #####: 3803:  default:
    #####: 3804:    break;
        -: 3805:  }
    #####: 3806:  return iterator();
        -: 3807:}
        -: 3808:
    #####: 3809:Value::iterator Value::end() {
    #####: 3810:  switch (type()) {
    #####: 3811:  case arrayValue:
        -: 3812:  case objectValue:
    #####: 3813:    if (value_.map_)
    #####: 3814:      return iterator(value_.map_->end());
    #####: 3815:    break;
    #####: 3816:  default:
    #####: 3817:    break;
        -: 3818:  }
    #####: 3819:  return iterator();
        -: 3820:}
        -: 3821:
        -: 3822:// class PathArgument
        -: 3823:// //////////////////////////////////////////////////////////////////
        -: 3824:
        -: 3825:PathArgument::PathArgument() = default;
        -: 3826:
    #####: 3827:PathArgument::PathArgument(ArrayIndex index)
    #####: 3828:    : index_(index), kind_(kindIndex) {}
        -: 3829:
    #####: 3830:PathArgument::PathArgument(const char* key) : key_(key), kind_(kindKey) {}
        -: 3831:
    #####: 3832:PathArgument::PathArgument(String key) : key_(std::move(key)), kind_(kindKey) {}
        -: 3833:
        -: 3834:// class Path
        -: 3835:// //////////////////////////////////////////////////////////////////
        -: 3836:
    #####: 3837:Path::Path(const String& path, const PathArgument& a1, const PathArgument& a2,
        -: 3838:           const PathArgument& a3, const PathArgument& a4,
    #####: 3839:           const PathArgument& a5) {
    #####: 3840:  InArgs in;
    #####: 3841:  in.reserve(5);
    #####: 3842:  in.push_back(&a1);
    #####: 3843:  in.push_back(&a2);
    #####: 3844:  in.push_back(&a3);
    #####: 3845:  in.push_back(&a4);
    #####: 3846:  in.push_back(&a5);
    #####: 3847:  makePath(path, in);
    #####: 3848:}
        -: 3849:
    #####: 3850:void Path::makePath(const String& path, const InArgs& in) {
    #####: 3851:  const char* current = path.c_str();
    #####: 3852:  const char* end = current + path.length();
    #####: 3853:  auto itInArg = in.begin();
    #####: 3854:  while (current != end) {
    #####: 3855:    if (*current == '[') {
    #####: 3856:      ++current;
    #####: 3857:      if (*current == '%')
    #####: 3858:        addPathInArg(path, in, itInArg, PathArgument::kindIndex);
        -: 3859:      else {
    #####: 3860:        ArrayIndex index = 0;
    #####: 3861:        for (; current != end && *current >= '0' && *current <= '9'; ++current)
    #####: 3862:          index = index * 10 + ArrayIndex(*current - '0');
    #####: 3863:        args_.push_back(index);
        -: 3864:      }
    #####: 3865:      if (current == end || *++current != ']')
    #####: 3866:        invalidPath(path, int(current - path.c_str()));
    #####: 3867:    } else if (*current == '%') {
    #####: 3868:      addPathInArg(path, in, itInArg, PathArgument::kindKey);
    #####: 3869:      ++current;
    #####: 3870:    } else if (*current == '.' || *current == ']') {
    #####: 3871:      ++current;
        -: 3872:    } else {
    #####: 3873:      const char* beginName = current;
    #####: 3874:      while (current != end && !strchr("[.", *current))
    #####: 3875:        ++current;
    #####: 3876:      args_.push_back(String(beginName, current));
        -: 3877:    }
        -: 3878:  }
    #####: 3879:}
        -: 3880:
    #####: 3881:void Path::addPathInArg(const String& /*path*/, const InArgs& in,
        -: 3882:                        InArgs::const_iterator& itInArg,
        -: 3883:                        PathArgument::Kind kind) {
    #####: 3884:  if (itInArg == in.end()) {
        -: 3885:    // Error: missing argument %d
    #####: 3886:  } else if ((*itInArg)->kind_ != kind) {
        -: 3887:    // Error: bad argument type
        -: 3888:  } else {
    #####: 3889:    args_.push_back(**itInArg++);
        -: 3890:  }
    #####: 3891:}
        -: 3892:
    #####: 3893:void Path::invalidPath(const String& /*path*/, int /*location*/) {
        -: 3894:  // Error: invalid path.
    #####: 3895:}
        -: 3896:
    #####: 3897:const Value& Path::resolve(const Value& root) const {
    #####: 3898:  const Value* node = &root;
    #####: 3899:  for (const auto& arg : args_) {
    #####: 3900:    if (arg.kind_ == PathArgument::kindIndex) {
    #####: 3901:      if (!node->isArray() || !node->isValidIndex(arg.index_)) {
        -: 3902:        // Error: unable to resolve path (array value expected at position... )
    #####: 3903:        return Value::nullSingleton();
        -: 3904:      }
    #####: 3905:      node = &((*node)[arg.index_]);
    #####: 3906:    } else if (arg.kind_ == PathArgument::kindKey) {
    #####: 3907:      if (!node->isObject()) {
        -: 3908:        // Error: unable to resolve path (object value expected at position...)
    #####: 3909:        return Value::nullSingleton();
        -: 3910:      }
    #####: 3911:      node = &((*node)[arg.key_]);
    #####: 3912:      if (node == &Value::nullSingleton()) {
        -: 3913:        // Error: unable to resolve path (object has no member named '' at
        -: 3914:        // position...)
    #####: 3915:        return Value::nullSingleton();
        -: 3916:      }
        -: 3917:    }
        -: 3918:  }
    #####: 3919:  return *node;
        -: 3920:}
        -: 3921:
    #####: 3922:Value Path::resolve(const Value& root, const Value& defaultValue) const {
    #####: 3923:  const Value* node = &root;
    #####: 3924:  for (const auto& arg : args_) {
    #####: 3925:    if (arg.kind_ == PathArgument::kindIndex) {
    #####: 3926:      if (!node->isArray() || !node->isValidIndex(arg.index_))
    #####: 3927:        return defaultValue;
    #####: 3928:      node = &((*node)[arg.index_]);
    #####: 3929:    } else if (arg.kind_ == PathArgument::kindKey) {
    #####: 3930:      if (!node->isObject())
    #####: 3931:        return defaultValue;
    #####: 3932:      node = &((*node)[arg.key_]);
    #####: 3933:      if (node == &Value::nullSingleton())
    #####: 3934:        return defaultValue;
        -: 3935:    }
        -: 3936:  }
    #####: 3937:  return *node;
        -: 3938:}
        -: 3939:
    #####: 3940:Value& Path::make(Value& root) const {
    #####: 3941:  Value* node = &root;
    #####: 3942:  for (const auto& arg : args_) {
    #####: 3943:    if (arg.kind_ == PathArgument::kindIndex) {
    #####: 3944:      if (!node->isArray()) {
        -: 3945:        // Error: node is not an array at position ...
        -: 3946:      }
    #####: 3947:      node = &((*node)[arg.index_]);
    #####: 3948:    } else if (arg.kind_ == PathArgument::kindKey) {
    #####: 3949:      if (!node->isObject()) {
        -: 3950:        // Error: node is not an object at position...
        -: 3951:      }
    #####: 3952:      node = &((*node)[arg.key_]);
        -: 3953:    }
        -: 3954:  }
    #####: 3955:  return *node;
        -: 3956:}
        -: 3957:
        -: 3958:} // namespace Json
        -: 3959:
        -: 3960:// //////////////////////////////////////////////////////////////////////
        -: 3961:// End of content of file: src/lib_json/json_value.cpp
        -: 3962:// //////////////////////////////////////////////////////////////////////
        -: 3963:
        -: 3964:
        -: 3965:
        -: 3966:
        -: 3967:
        -: 3968:
        -: 3969:// //////////////////////////////////////////////////////////////////////
        -: 3970:// Beginning of content of file: src/lib_json/json_writer.cpp
        -: 3971:// //////////////////////////////////////////////////////////////////////
        -: 3972:
        -: 3973:// Copyright 2011 Baptiste Lepilleur and The JsonCpp Authors
        -: 3974:// Distributed under MIT license, or public domain if desired and
        -: 3975:// recognized in your jurisdiction.
        -: 3976:// See file LICENSE for detail or copy at http://jsoncpp.sourceforge.net/LICENSE
        -: 3977:
        -: 3978:#if !defined(JSON_IS_AMALGAMATION)
        -: 3979:#include "json_tool.h"
        -: 3980:#include <json/writer.h>
        -: 3981:#endif // if !defined(JSON_IS_AMALGAMATION)
        -: 3982:#include <algorithm>
        -: 3983:#include <cassert>
        -: 3984:#include <cctype>
        -: 3985:#include <cstring>
        -: 3986:#include <iomanip>
        -: 3987:#include <memory>
        -: 3988:#include <set>
        -: 3989:#include <sstream>
        -: 3990:#include <utility>
        -: 3991:
        -: 3992:#if __cplusplus >= 201103L
        -: 3993:#include <cmath>
        -: 3994:#include <cstdio>
        -: 3995:
        -: 3996:#if !defined(isnan)
        -: 3997:#define isnan std::isnan
        -: 3998:#endif
        -: 3999:
        -: 4000:#if !defined(isfinite)
        -: 4001:#define isfinite std::isfinite
        -: 4002:#endif
        -: 4003:
        -: 4004:#else
        -: 4005:#include <cmath>
        -: 4006:#include <cstdio>
        -: 4007:
        -: 4008:#if defined(_MSC_VER)
        -: 4009:#if !defined(isnan)
        -: 4010:#include <float.h>
        -: 4011:#define isnan _isnan
        -: 4012:#endif
        -: 4013:
        -: 4014:#if !defined(isfinite)
        -: 4015:#include <float.h>
        -: 4016:#define isfinite _finite
        -: 4017:#endif
        -: 4018:
        -: 4019:#if !defined(_CRT_SECURE_CPP_OVERLOAD_STANDARD_NAMES)
        -: 4020:#define _CRT_SECURE_CPP_OVERLOAD_STANDARD_NAMES 1
        -: 4021:#endif //_CRT_SECURE_CPP_OVERLOAD_STANDARD_NAMES
        -: 4022:
        -: 4023:#endif //_MSC_VER
        -: 4024:
        -: 4025:#if defined(__sun) && defined(__SVR4) // Solaris
        -: 4026:#if !defined(isfinite)
        -: 4027:#include <ieeefp.h>
        -: 4028:#define isfinite finite
        -: 4029:#endif
        -: 4030:#endif
        -: 4031:
        -: 4032:#if defined(__hpux)
        -: 4033:#if !defined(isfinite)
        -: 4034:#if defined(__ia64) && !defined(finite)
        -: 4035:#define isfinite(x)                                                            \
        -: 4036:  ((sizeof(x) == sizeof(float) ? _Isfinitef(x) : _IsFinite(x)))
        -: 4037:#endif
        -: 4038:#endif
        -: 4039:#endif
        -: 4040:
        -: 4041:#if !defined(isnan)
        -: 4042:// IEEE standard states that NaN values will not compare to themselves
        -: 4043:#define isnan(x) (x != x)
        -: 4044:#endif
        -: 4045:
        -: 4046:#if !defined(__APPLE__)
        -: 4047:#if !defined(isfinite)
        -: 4048:#define isfinite finite
        -: 4049:#endif
        -: 4050:#endif
        -: 4051:#endif
        -: 4052:
        -: 4053:#if defined(_MSC_VER)
        -: 4054:// Disable warning about strdup being deprecated.
        -: 4055:#pragma warning(disable : 4996)
        -: 4056:#endif
        -: 4057:
        -: 4058:namespace Json {
        -: 4059:
        -: 4060:#if __cplusplus >= 201103L || (defined(_CPPLIB_VER) && _CPPLIB_VER >= 520)
        -: 4061:using StreamWriterPtr = std::unique_ptr<StreamWriter>;
        -: 4062:#else
        -: 4063:using StreamWriterPtr = std::auto_ptr<StreamWriter>;
        -: 4064:#endif
        -: 4065:
      166: 4066:String valueToString(LargestInt value) {
        -: 4067:  UIntToStringBuffer buffer;
      166: 4068:  char* current = buffer + sizeof(buffer);
      166: 4069:  if (value == Value::minLargestInt) {
    #####: 4070:    uintToString(LargestUInt(Value::maxLargestInt) + 1, current);
    #####: 4071:    *--current = '-';
      166: 4072:  } else if (value < 0) {
    #####: 4073:    uintToString(LargestUInt(-value), current);
    #####: 4074:    *--current = '-';
        -: 4075:  } else {
      166: 4076:    uintToString(LargestUInt(value), current);
        -: 4077:  }
     166*: 4078:  assert(current >= buffer);
      166: 4079:  return current;
        -: 4080:}
        -: 4081:
    #####: 4082:String valueToString(LargestUInt value) {
        -: 4083:  UIntToStringBuffer buffer;
    #####: 4084:  char* current = buffer + sizeof(buffer);
    #####: 4085:  uintToString(value, current);
    #####: 4086:  assert(current >= buffer);
    #####: 4087:  return current;
        -: 4088:}
        -: 4089:
        -: 4090:#if defined(JSON_HAS_INT64)
        -: 4091:
    #####: 4092:String valueToString(Int value) { return valueToString(LargestInt(value)); }
        -: 4093:
    #####: 4094:String valueToString(UInt value) { return valueToString(LargestUInt(value)); }
        -: 4095:
        -: 4096:#endif // # if defined(JSON_HAS_INT64)
        -: 4097:
        -: 4098:namespace {
    #####: 4099:String valueToString(double value, bool useSpecialFloats,
        -: 4100:                     unsigned int precision, PrecisionType precisionType) {
        -: 4101:  // Print into the buffer. We need not request the alternative representation
        -: 4102:  // that always has a decimal point because JSON doesn't distinguish the
        -: 4103:  // concepts of reals and integers.
    #####: 4104:  if (!isfinite(value)) {
        -: 4105:    static const char* const reps[2][3] = {{"NaN", "-Infinity", "Infinity"},
        -: 4106:                                           {"null", "-1e+9999", "1e+9999"}};
    #####: 4107:    return reps[useSpecialFloats ? 0 : 1]
    #####: 4108:               [isnan(value) ? 0 : (value < 0) ? 1 : 2];
        -: 4109:  }
        -: 4110:
    #####: 4111:  String buffer(size_t(36), '\0');
        -: 4112:  while (true) {
    #####: 4113:    int len = jsoncpp_snprintf(
    #####: 4114:        &*buffer.begin(), buffer.size(),
        -: 4115:        (precisionType == PrecisionType::significantDigits) ? "%.*g" : "%.*f",
        -: 4116:        precision, value);
    #####: 4117:    assert(len >= 0);
    #####: 4118:    auto wouldPrint = static_cast<size_t>(len);
    #####: 4119:    if (wouldPrint >= buffer.size()) {
    #####: 4120:      buffer.resize(wouldPrint + 1);
    #####: 4121:      continue;
        -: 4122:    }
    #####: 4123:    buffer.resize(wouldPrint);
    #####: 4124:    break;
    #####: 4125:  }
        -: 4126:
    #####: 4127:  buffer.erase(fixNumericLocale(buffer.begin(), buffer.end()), buffer.end());
        -: 4128:
        -: 4129:  // strip the zero padding from the right
    #####: 4130:  if (precisionType == PrecisionType::decimalPlaces) {
    #####: 4131:    buffer.erase(fixZerosInTheEnd(buffer.begin(), buffer.end()), buffer.end());
        -: 4132:  }
        -: 4133:
        -: 4134:  // try to ensure we preserve the fact that this was given to us as a double on
        -: 4135:  // input
    #####: 4136:  if (buffer.find('.') == buffer.npos && buffer.find('e') == buffer.npos) {
    #####: 4137:    buffer += ".0";
        -: 4138:  }
    #####: 4139:  return buffer;
        -: 4140:}
        -: 4141:} // namespace
        -: 4142:
    #####: 4143:String valueToString(double value, unsigned int precision,
        -: 4144:                     PrecisionType precisionType) {
    #####: 4145:  return valueToString(value, false, precision, precisionType);
        -: 4146:}
        -: 4147:
    #####: 4148:String valueToString(bool value) { return value ? "true" : "false"; }
        -: 4149:
      320: 4150:static bool doesAnyCharRequireEscaping(char const* s, size_t n) {
     320*: 4151:  assert(s || !n);
        -: 4152:
      320: 4153:  return std::any_of(s, s + n, [](unsigned char c) {
    1749*: 4154:    return c == '\\' || c == '"' || c < 0x20 || c > 0x7F;
      320: 4155:  });
        -: 4156:}
        -: 4157:
    #####: 4158:static unsigned int utf8ToCodepoint(const char*& s, const char* e) {
    #####: 4159:  const unsigned int REPLACEMENT_CHARACTER = 0xFFFD;
        -: 4160:
    #####: 4161:  unsigned int firstByte = static_cast<unsigned char>(*s);
        -: 4162:
    #####: 4163:  if (firstByte < 0x80)
    #####: 4164:    return firstByte;
        -: 4165:
    #####: 4166:  if (firstByte < 0xE0) {
    #####: 4167:    if (e - s < 2)
    #####: 4168:      return REPLACEMENT_CHARACTER;
        -: 4169:
    #####: 4170:    unsigned int calculated =
    #####: 4171:        ((firstByte & 0x1F) << 6) | (static_cast<unsigned int>(s[1]) & 0x3F);
    #####: 4172:    s += 1;
        -: 4173:    // oversized encoded characters are invalid
    #####: 4174:    return calculated < 0x80 ? REPLACEMENT_CHARACTER : calculated;
        -: 4175:  }
        -: 4176:
    #####: 4177:  if (firstByte < 0xF0) {
    #####: 4178:    if (e - s < 3)
    #####: 4179:      return REPLACEMENT_CHARACTER;
        -: 4180:
    #####: 4181:    unsigned int calculated = ((firstByte & 0x0F) << 12) |
    #####: 4182:                              ((static_cast<unsigned int>(s[1]) & 0x3F) << 6) |
    #####: 4183:                              (static_cast<unsigned int>(s[2]) & 0x3F);
    #####: 4184:    s += 2;
        -: 4185:    // surrogates aren't valid codepoints itself
        -: 4186:    // shouldn't be UTF-8 encoded
    #####: 4187:    if (calculated >= 0xD800 && calculated <= 0xDFFF)
    #####: 4188:      return REPLACEMENT_CHARACTER;
        -: 4189:    // oversized encoded characters are invalid
    #####: 4190:    return calculated < 0x800 ? REPLACEMENT_CHARACTER : calculated;
        -: 4191:  }
        -: 4192:
    #####: 4193:  if (firstByte < 0xF8) {
    #####: 4194:    if (e - s < 4)
    #####: 4195:      return REPLACEMENT_CHARACTER;
        -: 4196:
    #####: 4197:    unsigned int calculated = ((firstByte & 0x07) << 18) |
    #####: 4198:                              ((static_cast<unsigned int>(s[1]) & 0x3F) << 12) |
    #####: 4199:                              ((static_cast<unsigned int>(s[2]) & 0x3F) << 6) |
    #####: 4200:                              (static_cast<unsigned int>(s[3]) & 0x3F);
    #####: 4201:    s += 3;
        -: 4202:    // oversized encoded characters are invalid
    #####: 4203:    return calculated < 0x10000 ? REPLACEMENT_CHARACTER : calculated;
        -: 4204:  }
        -: 4205:
    #####: 4206:  return REPLACEMENT_CHARACTER;
        -: 4207:}
        -: 4208:
        -: 4209:static const char hex2[] = "000102030405060708090a0b0c0d0e0f"
        -: 4210:                           "101112131415161718191a1b1c1d1e1f"
        -: 4211:                           "202122232425262728292a2b2c2d2e2f"
        -: 4212:                           "303132333435363738393a3b3c3d3e3f"
        -: 4213:                           "404142434445464748494a4b4c4d4e4f"
        -: 4214:                           "505152535455565758595a5b5c5d5e5f"
        -: 4215:                           "606162636465666768696a6b6c6d6e6f"
        -: 4216:                           "707172737475767778797a7b7c7d7e7f"
        -: 4217:                           "808182838485868788898a8b8c8d8e8f"
        -: 4218:                           "909192939495969798999a9b9c9d9e9f"
        -: 4219:                           "a0a1a2a3a4a5a6a7a8a9aaabacadaeaf"
        -: 4220:                           "b0b1b2b3b4b5b6b7b8b9babbbcbdbebf"
        -: 4221:                           "c0c1c2c3c4c5c6c7c8c9cacbcccdcecf"
        -: 4222:                           "d0d1d2d3d4d5d6d7d8d9dadbdcdddedf"
        -: 4223:                           "e0e1e2e3e4e5e6e7e8e9eaebecedeeef"
        -: 4224:                           "f0f1f2f3f4f5f6f7f8f9fafbfcfdfeff";
        -: 4225:
    #####: 4226:static String toHex16Bit(unsigned int x) {
    #####: 4227:  const unsigned int hi = (x >> 8) & 0xff;
    #####: 4228:  const unsigned int lo = x & 0xff;
    #####: 4229:  String result(4, ' ');
    #####: 4230:  result[0] = hex2[2 * hi];
    #####: 4231:  result[1] = hex2[2 * hi + 1];
    #####: 4232:  result[2] = hex2[2 * lo];
    #####: 4233:  result[3] = hex2[2 * lo + 1];
    #####: 4234:  return result;
        -: 4235:}
        -: 4236:
    #####: 4237:static void appendRaw(String& result, unsigned ch) {
    #####: 4238:  result += static_cast<char>(ch);
    #####: 4239:}
        -: 4240:
    #####: 4241:static void appendHex(String& result, unsigned ch) {
    #####: 4242:  result.append("\\u").append(toHex16Bit(ch));
    #####: 4243:}
        -: 4244:
      320: 4245:static String valueToQuotedStringN(const char* value, unsigned length,
        -: 4246:                                   bool emitUTF8 = false) {
      320: 4247:  if (value == nullptr)
    #####: 4248:    return "";
        -: 4249:
      320: 4250:  if (!doesAnyCharRequireEscaping(value, length))
      640: 4251:    return String("\"") + value + "\"";
        -: 4252:  // We have to walk value and escape any special characters.
        -: 4253:  // Appending to String is not efficient, but this should be rare.
        -: 4254:  // (Note: forward slashes are *not* rare, but I am not escaping them.)
    #####: 4255:  String::size_type maxsize = length * 2 + 3; // allescaped+quotes+NULL
    #####: 4256:  String result;
    #####: 4257:  result.reserve(maxsize); // to avoid lots of mallocs
    #####: 4258:  result += "\"";
    #####: 4259:  char const* end = value + length;
    #####: 4260:  for (const char* c = value; c != end; ++c) {
    #####: 4261:    switch (*c) {
    #####: 4262:    case '\"':
    #####: 4263:      result += "\\\"";
    #####: 4264:      break;
    #####: 4265:    case '\\':
    #####: 4266:      result += "\\\\";
    #####: 4267:      break;
    #####: 4268:    case '\b':
    #####: 4269:      result += "\\b";
    #####: 4270:      break;
    #####: 4271:    case '\f':
    #####: 4272:      result += "\\f";
    #####: 4273:      break;
    #####: 4274:    case '\n':
    #####: 4275:      result += "\\n";
    #####: 4276:      break;
    #####: 4277:    case '\r':
    #####: 4278:      result += "\\r";
    #####: 4279:      break;
    #####: 4280:    case '\t':
    #####: 4281:      result += "\\t";
    #####: 4282:      break;
        -: 4283:    // case '/':
        -: 4284:    // Even though \/ is considered a legal escape in JSON, a bare
        -: 4285:    // slash is also legal, so I see no reason to escape it.
        -: 4286:    // (I hope I am not misunderstanding something.)
        -: 4287:    // blep notes: actually escaping \/ may be useful in javascript to avoid </
        -: 4288:    // sequence.
        -: 4289:    // Should add a flag to allow this compatibility mode and prevent this
        -: 4290:    // sequence from occurring.
    #####: 4291:    default: {
    #####: 4292:      if (emitUTF8) {
    #####: 4293:        unsigned codepoint = static_cast<unsigned char>(*c);
    #####: 4294:        if (codepoint < 0x20) {
    #####: 4295:          appendHex(result, codepoint);
        -: 4296:        } else {
    #####: 4297:          appendRaw(result, codepoint);
        -: 4298:        }
        -: 4299:      } else {
    #####: 4300:        unsigned codepoint = utf8ToCodepoint(c, end); // modifies `c`
    #####: 4301:        if (codepoint < 0x20) {
    #####: 4302:          appendHex(result, codepoint);
    #####: 4303:        } else if (codepoint < 0x80) {
    #####: 4304:          appendRaw(result, codepoint);
    #####: 4305:        } else if (codepoint < 0x10000) {
        -: 4306:          // Basic Multilingual Plane
    #####: 4307:          appendHex(result, codepoint);
        -: 4308:        } else {
        -: 4309:          // Extended Unicode. Encode 20 bits as a surrogate pair.
    #####: 4310:          codepoint -= 0x10000;
    #####: 4311:          appendHex(result, 0xd800 + ((codepoint >> 10) & 0x3ff));
    #####: 4312:          appendHex(result, 0xdc00 + (codepoint & 0x3ff));
        -: 4313:        }
        -: 4314:      }
    #####: 4315:    } break;
        -: 4316:    }
        -: 4317:  }
    #####: 4318:  result += "\"";
    #####: 4319:  return result;
        -: 4320:}
        -: 4321:
    #####: 4322:String valueToQuotedString(const char* value) {
    #####: 4323:  return valueToQuotedStringN(value, static_cast<unsigned int>(strlen(value)));
        -: 4324:}
        -: 4325:
        -: 4326:// Class Writer
        -: 4327:// //////////////////////////////////////////////////////////////////
        -: 4328:Writer::~Writer() = default;
        -: 4329:
        -: 4330:// Class FastWriter
        -: 4331:// //////////////////////////////////////////////////////////////////
        -: 4332:
        -: 4333:FastWriter::FastWriter()
        -: 4334:
        -: 4335:    = default;
        -: 4336:
    #####: 4337:void FastWriter::enableYAMLCompatibility() { yamlCompatibilityEnabled_ = true; }
        -: 4338:
    #####: 4339:void FastWriter::dropNullPlaceholders() { dropNullPlaceholders_ = true; }
        -: 4340:
    #####: 4341:void FastWriter::omitEndingLineFeed() { omitEndingLineFeed_ = true; }
        -: 4342:
    #####: 4343:String FastWriter::write(const Value& root) {
    #####: 4344:  document_.clear();
    #####: 4345:  writeValue(root);
    #####: 4346:  if (!omitEndingLineFeed_)
    #####: 4347:    document_ += '\n';
    #####: 4348:  return document_;
        -: 4349:}
        -: 4350:
    #####: 4351:void FastWriter::writeValue(const Value& value) {
    #####: 4352:  switch (value.type()) {
    #####: 4353:  case nullValue:
    #####: 4354:    if (!dropNullPlaceholders_)
    #####: 4355:      document_ += "null";
    #####: 4356:    break;
    #####: 4357:  case intValue:
    #####: 4358:    document_ += valueToString(value.asLargestInt());
    #####: 4359:    break;
    #####: 4360:  case uintValue:
    #####: 4361:    document_ += valueToString(value.asLargestUInt());
    #####: 4362:    break;
    #####: 4363:  case realValue:
    #####: 4364:    document_ += valueToString(value.asDouble());
    #####: 4365:    break;
    #####: 4366:  case stringValue: {
        -: 4367:    // Is NULL possible for value.string_? No.
        -: 4368:    char const* str;
        -: 4369:    char const* end;
    #####: 4370:    bool ok = value.getString(&str, &end);
    #####: 4371:    if (ok)
    #####: 4372:      document_ += valueToQuotedStringN(str, static_cast<unsigned>(end - str));
    #####: 4373:    break;
        -: 4374:  }
    #####: 4375:  case booleanValue:
    #####: 4376:    document_ += valueToString(value.asBool());
    #####: 4377:    break;
    #####: 4378:  case arrayValue: {
    #####: 4379:    document_ += '[';
    #####: 4380:    ArrayIndex size = value.size();
    #####: 4381:    for (ArrayIndex index = 0; index < size; ++index) {
    #####: 4382:      if (index > 0)
    #####: 4383:        document_ += ',';
    #####: 4384:      writeValue(value[index]);
        -: 4385:    }
    #####: 4386:    document_ += ']';
    #####: 4387:  } break;
    #####: 4388:  case objectValue: {
    #####: 4389:    Value::Members members(value.getMemberNames());
    #####: 4390:    document_ += '{';
    #####: 4391:    for (auto it = members.begin(); it != members.end(); ++it) {
    #####: 4392:      const String& name = *it;
    #####: 4393:      if (it != members.begin())
    #####: 4394:        document_ += ',';
    #####: 4395:      document_ += valueToQuotedStringN(name.data(),
    #####: 4396:                                        static_cast<unsigned>(name.length()));
    #####: 4397:      document_ += yamlCompatibilityEnabled_ ? ": " : ":";
    #####: 4398:      writeValue(value[name]);
        -: 4399:    }
    #####: 4400:    document_ += '}';
    #####: 4401:  } break;
        -: 4402:  }
    #####: 4403:}
        -: 4404:
        -: 4405:// Class StyledWriter
        -: 4406:// //////////////////////////////////////////////////////////////////
        -: 4407:
        -: 4408:StyledWriter::StyledWriter() = default;
        -: 4409:
    #####: 4410:String StyledWriter::write(const Value& root) {
    #####: 4411:  document_.clear();
    #####: 4412:  addChildValues_ = false;
    #####: 4413:  indentString_.clear();
    #####: 4414:  writeCommentBeforeValue(root);
    #####: 4415:  writeValue(root);
    #####: 4416:  writeCommentAfterValueOnSameLine(root);
    #####: 4417:  document_ += '\n';
    #####: 4418:  return document_;
        -: 4419:}
        -: 4420:
    #####: 4421:void StyledWriter::writeValue(const Value& value) {
    #####: 4422:  switch (value.type()) {
    #####: 4423:  case nullValue:
    #####: 4424:    pushValue("null");
    #####: 4425:    break;
    #####: 4426:  case intValue:
    #####: 4427:    pushValue(valueToString(value.asLargestInt()));
    #####: 4428:    break;
    #####: 4429:  case uintValue:
    #####: 4430:    pushValue(valueToString(value.asLargestUInt()));
    #####: 4431:    break;
    #####: 4432:  case realValue:
    #####: 4433:    pushValue(valueToString(value.asDouble()));
    #####: 4434:    break;
    #####: 4435:  case stringValue: {
        -: 4436:    // Is NULL possible for value.string_? No.
        -: 4437:    char const* str;
        -: 4438:    char const* end;
    #####: 4439:    bool ok = value.getString(&str, &end);
    #####: 4440:    if (ok)
    #####: 4441:      pushValue(valueToQuotedStringN(str, static_cast<unsigned>(end - str)));
        -: 4442:    else
    #####: 4443:      pushValue("");
    #####: 4444:    break;
        -: 4445:  }
    #####: 4446:  case booleanValue:
    #####: 4447:    pushValue(valueToString(value.asBool()));
    #####: 4448:    break;
    #####: 4449:  case arrayValue:
    #####: 4450:    writeArrayValue(value);
    #####: 4451:    break;
    #####: 4452:  case objectValue: {
    #####: 4453:    Value::Members members(value.getMemberNames());
    #####: 4454:    if (members.empty())
    #####: 4455:      pushValue("{}");
        -: 4456:    else {
    #####: 4457:      writeWithIndent("{");
    #####: 4458:      indent();
    #####: 4459:      auto it = members.begin();
        -: 4460:      for (;;) {
    #####: 4461:        const String& name = *it;
    #####: 4462:        const Value& childValue = value[name];
    #####: 4463:        writeCommentBeforeValue(childValue);
    #####: 4464:        writeWithIndent(valueToQuotedString(name.c_str()));
    #####: 4465:        document_ += " : ";
    #####: 4466:        writeValue(childValue);
    #####: 4467:        if (++it == members.end()) {
    #####: 4468:          writeCommentAfterValueOnSameLine(childValue);
    #####: 4469:          break;
        -: 4470:        }
    #####: 4471:        document_ += ',';
    #####: 4472:        writeCommentAfterValueOnSameLine(childValue);
    #####: 4473:      }
    #####: 4474:      unindent();
    #####: 4475:      writeWithIndent("}");
        -: 4476:    }
    #####: 4477:  } break;
        -: 4478:  }
    #####: 4479:}
        -: 4480:
    #####: 4481:void StyledWriter::writeArrayValue(const Value& value) {
    #####: 4482:  unsigned size = value.size();
    #####: 4483:  if (size == 0)
    #####: 4484:    pushValue("[]");
        -: 4485:  else {
    #####: 4486:    bool isArrayMultiLine = isMultilineArray(value);
    #####: 4487:    if (isArrayMultiLine) {
    #####: 4488:      writeWithIndent("[");
    #####: 4489:      indent();
    #####: 4490:      bool hasChildValue = !childValues_.empty();
    #####: 4491:      unsigned index = 0;
        -: 4492:      for (;;) {
    #####: 4493:        const Value& childValue = value[index];
    #####: 4494:        writeCommentBeforeValue(childValue);
    #####: 4495:        if (hasChildValue)
    #####: 4496:          writeWithIndent(childValues_[index]);
        -: 4497:        else {
    #####: 4498:          writeIndent();
    #####: 4499:          writeValue(childValue);
        -: 4500:        }
    #####: 4501:        if (++index == size) {
    #####: 4502:          writeCommentAfterValueOnSameLine(childValue);
    #####: 4503:          break;
        -: 4504:        }
    #####: 4505:        document_ += ',';
    #####: 4506:        writeCommentAfterValueOnSameLine(childValue);
    #####: 4507:      }
    #####: 4508:      unindent();
    #####: 4509:      writeWithIndent("]");
        -: 4510:    } else // output on a single line
        -: 4511:    {
    #####: 4512:      assert(childValues_.size() == size);
    #####: 4513:      document_ += "[ ";
    #####: 4514:      for (unsigned index = 0; index < size; ++index) {
    #####: 4515:        if (index > 0)
    #####: 4516:          document_ += ", ";
    #####: 4517:        document_ += childValues_[index];
        -: 4518:      }
    #####: 4519:      document_ += " ]";
        -: 4520:    }
        -: 4521:  }
    #####: 4522:}
        -: 4523:
    #####: 4524:bool StyledWriter::isMultilineArray(const Value& value) {
    #####: 4525:  ArrayIndex const size = value.size();
    #####: 4526:  bool isMultiLine = size * 3 >= rightMargin_;
    #####: 4527:  childValues_.clear();
    #####: 4528:  for (ArrayIndex index = 0; index < size && !isMultiLine; ++index) {
    #####: 4529:    const Value& childValue = value[index];
    #####: 4530:    isMultiLine = ((childValue.isArray() || childValue.isObject()) &&
    #####: 4531:                   !childValue.empty());
        -: 4532:  }
    #####: 4533:  if (!isMultiLine) // check if line length > max line length
        -: 4534:  {
    #####: 4535:    childValues_.reserve(size);
    #####: 4536:    addChildValues_ = true;
    #####: 4537:    ArrayIndex lineLength = 4 + (size - 1) * 2; // '[ ' + ', '*n + ' ]'
    #####: 4538:    for (ArrayIndex index = 0; index < size; ++index) {
    #####: 4539:      if (hasCommentForValue(value[index])) {
    #####: 4540:        isMultiLine = true;
        -: 4541:      }
    #####: 4542:      writeValue(value[index]);
    #####: 4543:      lineLength += static_cast<ArrayIndex>(childValues_[index].length());
        -: 4544:    }
    #####: 4545:    addChildValues_ = false;
    #####: 4546:    isMultiLine = isMultiLine || lineLength >= rightMargin_;
        -: 4547:  }
    #####: 4548:  return isMultiLine;
        -: 4549:}
        -: 4550:
    #####: 4551:void StyledWriter::pushValue(const String& value) {
    #####: 4552:  if (addChildValues_)
    #####: 4553:    childValues_.push_back(value);
        -: 4554:  else
    #####: 4555:    document_ += value;
    #####: 4556:}
        -: 4557:
    #####: 4558:void StyledWriter::writeIndent() {
    #####: 4559:  if (!document_.empty()) {
    #####: 4560:    char last = document_[document_.length() - 1];
    #####: 4561:    if (last == ' ') // already indented
    #####: 4562:      return;
    #####: 4563:    if (last != '\n') // Comments may add new-line
    #####: 4564:      document_ += '\n';
        -: 4565:  }
    #####: 4566:  document_ += indentString_;
        -: 4567:}
        -: 4568:
    #####: 4569:void StyledWriter::writeWithIndent(const String& value) {
    #####: 4570:  writeIndent();
    #####: 4571:  document_ += value;
    #####: 4572:}
        -: 4573:
    #####: 4574:void StyledWriter::indent() { indentString_ += String(indentSize_, ' '); }
        -: 4575:
    #####: 4576:void StyledWriter::unindent() {
    #####: 4577:  assert(indentString_.size() >= indentSize_);
    #####: 4578:  indentString_.resize(indentString_.size() - indentSize_);
    #####: 4579:}
        -: 4580:
    #####: 4581:void StyledWriter::writeCommentBeforeValue(const Value& root) {
    #####: 4582:  if (!root.hasComment(commentBefore))
    #####: 4583:    return;
        -: 4584:
    #####: 4585:  document_ += '\n';
    #####: 4586:  writeIndent();
    #####: 4587:  const String& comment = root.getComment(commentBefore);
    #####: 4588:  String::const_iterator iter = comment.begin();
    #####: 4589:  while (iter != comment.end()) {
    #####: 4590:    document_ += *iter;
    #####: 4591:    if (*iter == '\n' && ((iter + 1) != comment.end() && *(iter + 1) == '/'))
    #####: 4592:      writeIndent();
    #####: 4593:    ++iter;
        -: 4594:  }
        -: 4595:
        -: 4596:  // Comments are stripped of trailing newlines, so add one here
    #####: 4597:  document_ += '\n';
        -: 4598:}
        -: 4599:
    #####: 4600:void StyledWriter::writeCommentAfterValueOnSameLine(const Value& root) {
    #####: 4601:  if (root.hasComment(commentAfterOnSameLine))
    #####: 4602:    document_ += " " + root.getComment(commentAfterOnSameLine);
        -: 4603:
    #####: 4604:  if (root.hasComment(commentAfter)) {
    #####: 4605:    document_ += '\n';
    #####: 4606:    document_ += root.getComment(commentAfter);
    #####: 4607:    document_ += '\n';
        -: 4608:  }
    #####: 4609:}
        -: 4610:
    #####: 4611:bool StyledWriter::hasCommentForValue(const Value& value) {
    #####: 4612:  return value.hasComment(commentBefore) ||
    #####: 4613:         value.hasComment(commentAfterOnSameLine) ||
    #####: 4614:         value.hasComment(commentAfter);
        -: 4615:}
        -: 4616:
        -: 4617:// Class StyledStreamWriter
        -: 4618:// //////////////////////////////////////////////////////////////////
        -: 4619:
    #####: 4620:StyledStreamWriter::StyledStreamWriter(String indentation)
    #####: 4621:    : document_(nullptr), indentation_(std::move(indentation)),
    #####: 4622:      addChildValues_(), indented_(false) {}
        -: 4623:
    #####: 4624:void StyledStreamWriter::write(OStream& out, const Value& root) {
    #####: 4625:  document_ = &out;
    #####: 4626:  addChildValues_ = false;
    #####: 4627:  indentString_.clear();
    #####: 4628:  indented_ = true;
    #####: 4629:  writeCommentBeforeValue(root);
    #####: 4630:  if (!indented_)
    #####: 4631:    writeIndent();
    #####: 4632:  indented_ = true;
    #####: 4633:  writeValue(root);
    #####: 4634:  writeCommentAfterValueOnSameLine(root);
    #####: 4635:  *document_ << "\n";
    #####: 4636:  document_ = nullptr; // Forget the stream, for safety.
    #####: 4637:}
        -: 4638:
    #####: 4639:void StyledStreamWriter::writeValue(const Value& value) {
    #####: 4640:  switch (value.type()) {
    #####: 4641:  case nullValue:
    #####: 4642:    pushValue("null");
    #####: 4643:    break;
    #####: 4644:  case intValue:
    #####: 4645:    pushValue(valueToString(value.asLargestInt()));
    #####: 4646:    break;
    #####: 4647:  case uintValue:
    #####: 4648:    pushValue(valueToString(value.asLargestUInt()));
    #####: 4649:    break;
    #####: 4650:  case realValue:
    #####: 4651:    pushValue(valueToString(value.asDouble()));
    #####: 4652:    break;
    #####: 4653:  case stringValue: {
        -: 4654:    // Is NULL possible for value.string_? No.
        -: 4655:    char const* str;
        -: 4656:    char const* end;
    #####: 4657:    bool ok = value.getString(&str, &end);
    #####: 4658:    if (ok)
    #####: 4659:      pushValue(valueToQuotedStringN(str, static_cast<unsigned>(end - str)));
        -: 4660:    else
    #####: 4661:      pushValue("");
    #####: 4662:    break;
        -: 4663:  }
    #####: 4664:  case booleanValue:
    #####: 4665:    pushValue(valueToString(value.asBool()));
    #####: 4666:    break;
    #####: 4667:  case arrayValue:
    #####: 4668:    writeArrayValue(value);
    #####: 4669:    break;
    #####: 4670:  case objectValue: {
    #####: 4671:    Value::Members members(value.getMemberNames());
    #####: 4672:    if (members.empty())
    #####: 4673:      pushValue("{}");
        -: 4674:    else {
    #####: 4675:      writeWithIndent("{");
    #####: 4676:      indent();
    #####: 4677:      auto it = members.begin();
        -: 4678:      for (;;) {
    #####: 4679:        const String& name = *it;
    #####: 4680:        const Value& childValue = value[name];
    #####: 4681:        writeCommentBeforeValue(childValue);
    #####: 4682:        writeWithIndent(valueToQuotedString(name.c_str()));
    #####: 4683:        *document_ << " : ";
    #####: 4684:        writeValue(childValue);
    #####: 4685:        if (++it == members.end()) {
    #####: 4686:          writeCommentAfterValueOnSameLine(childValue);
    #####: 4687:          break;
        -: 4688:        }
    #####: 4689:        *document_ << ",";
    #####: 4690:        writeCommentAfterValueOnSameLine(childValue);
    #####: 4691:      }
    #####: 4692:      unindent();
    #####: 4693:      writeWithIndent("}");
        -: 4694:    }
    #####: 4695:  } break;
        -: 4696:  }
    #####: 4697:}
        -: 4698:
    #####: 4699:void StyledStreamWriter::writeArrayValue(const Value& value) {
    #####: 4700:  unsigned size = value.size();
    #####: 4701:  if (size == 0)
    #####: 4702:    pushValue("[]");
        -: 4703:  else {
    #####: 4704:    bool isArrayMultiLine = isMultilineArray(value);
    #####: 4705:    if (isArrayMultiLine) {
    #####: 4706:      writeWithIndent("[");
    #####: 4707:      indent();
    #####: 4708:      bool hasChildValue = !childValues_.empty();
    #####: 4709:      unsigned index = 0;
        -: 4710:      for (;;) {
    #####: 4711:        const Value& childValue = value[index];
    #####: 4712:        writeCommentBeforeValue(childValue);
    #####: 4713:        if (hasChildValue)
    #####: 4714:          writeWithIndent(childValues_[index]);
        -: 4715:        else {
    #####: 4716:          if (!indented_)
    #####: 4717:            writeIndent();
    #####: 4718:          indented_ = true;
    #####: 4719:          writeValue(childValue);
    #####: 4720:          indented_ = false;
        -: 4721:        }
    #####: 4722:        if (++index == size) {
    #####: 4723:          writeCommentAfterValueOnSameLine(childValue);
    #####: 4724:          break;
        -: 4725:        }
    #####: 4726:        *document_ << ",";
    #####: 4727:        writeCommentAfterValueOnSameLine(childValue);
    #####: 4728:      }
    #####: 4729:      unindent();
    #####: 4730:      writeWithIndent("]");
        -: 4731:    } else // output on a single line
        -: 4732:    {
    #####: 4733:      assert(childValues_.size() == size);
    #####: 4734:      *document_ << "[ ";
    #####: 4735:      for (unsigned index = 0; index < size; ++index) {
    #####: 4736:        if (index > 0)
    #####: 4737:          *document_ << ", ";
    #####: 4738:        *document_ << childValues_[index];
        -: 4739:      }
    #####: 4740:      *document_ << " ]";
        -: 4741:    }
        -: 4742:  }
    #####: 4743:}
        -: 4744:
    #####: 4745:bool StyledStreamWriter::isMultilineArray(const Value& value) {
    #####: 4746:  ArrayIndex const size = value.size();
    #####: 4747:  bool isMultiLine = size * 3 >= rightMargin_;
    #####: 4748:  childValues_.clear();
    #####: 4749:  for (ArrayIndex index = 0; index < size && !isMultiLine; ++index) {
    #####: 4750:    const Value& childValue = value[index];
    #####: 4751:    isMultiLine = ((childValue.isArray() || childValue.isObject()) &&
    #####: 4752:                   !childValue.empty());
        -: 4753:  }
    #####: 4754:  if (!isMultiLine) // check if line length > max line length
        -: 4755:  {
    #####: 4756:    childValues_.reserve(size);
    #####: 4757:    addChildValues_ = true;
    #####: 4758:    ArrayIndex lineLength = 4 + (size - 1) * 2; // '[ ' + ', '*n + ' ]'
    #####: 4759:    for (ArrayIndex index = 0; index < size; ++index) {
    #####: 4760:      if (hasCommentForValue(value[index])) {
    #####: 4761:        isMultiLine = true;
        -: 4762:      }
    #####: 4763:      writeValue(value[index]);
    #####: 4764:      lineLength += static_cast<ArrayIndex>(childValues_[index].length());
        -: 4765:    }
    #####: 4766:    addChildValues_ = false;
    #####: 4767:    isMultiLine = isMultiLine || lineLength >= rightMargin_;
        -: 4768:  }
    #####: 4769:  return isMultiLine;
        -: 4770:}
        -: 4771:
    #####: 4772:void StyledStreamWriter::pushValue(const String& value) {
    #####: 4773:  if (addChildValues_)
    #####: 4774:    childValues_.push_back(value);
        -: 4775:  else
    #####: 4776:    *document_ << value;
    #####: 4777:}
        -: 4778:
    #####: 4779:void StyledStreamWriter::writeIndent() {
        -: 4780:  // blep intended this to look at the so-far-written string
        -: 4781:  // to determine whether we are already indented, but
        -: 4782:  // with a stream we cannot do that. So we rely on some saved state.
        -: 4783:  // The caller checks indented_.
    #####: 4784:  *document_ << '\n' << indentString_;
    #####: 4785:}
        -: 4786:
    #####: 4787:void StyledStreamWriter::writeWithIndent(const String& value) {
    #####: 4788:  if (!indented_)
    #####: 4789:    writeIndent();
    #####: 4790:  *document_ << value;
    #####: 4791:  indented_ = false;
    #####: 4792:}
        -: 4793:
    #####: 4794:void StyledStreamWriter::indent() { indentString_ += indentation_; }
        -: 4795:
    #####: 4796:void StyledStreamWriter::unindent() {
    #####: 4797:  assert(indentString_.size() >= indentation_.size());
    #####: 4798:  indentString_.resize(indentString_.size() - indentation_.size());
    #####: 4799:}
        -: 4800:
    #####: 4801:void StyledStreamWriter::writeCommentBeforeValue(const Value& root) {
    #####: 4802:  if (!root.hasComment(commentBefore))
    #####: 4803:    return;
        -: 4804:
    #####: 4805:  if (!indented_)
    #####: 4806:    writeIndent();
    #####: 4807:  const String& comment = root.getComment(commentBefore);
    #####: 4808:  String::const_iterator iter = comment.begin();
    #####: 4809:  while (iter != comment.end()) {
    #####: 4810:    *document_ << *iter;
    #####: 4811:    if (*iter == '\n' && ((iter + 1) != comment.end() && *(iter + 1) == '/'))
        -: 4812:      // writeIndent();  // would include newline
    #####: 4813:      *document_ << indentString_;
    #####: 4814:    ++iter;
        -: 4815:  }
    #####: 4816:  indented_ = false;
        -: 4817:}
        -: 4818:
    #####: 4819:void StyledStreamWriter::writeCommentAfterValueOnSameLine(const Value& root) {
    #####: 4820:  if (root.hasComment(commentAfterOnSameLine))
    #####: 4821:    *document_ << ' ' << root.getComment(commentAfterOnSameLine);
        -: 4822:
    #####: 4823:  if (root.hasComment(commentAfter)) {
    #####: 4824:    writeIndent();
    #####: 4825:    *document_ << root.getComment(commentAfter);
        -: 4826:  }
    #####: 4827:  indented_ = false;
    #####: 4828:}
        -: 4829:
    #####: 4830:bool StyledStreamWriter::hasCommentForValue(const Value& value) {
    #####: 4831:  return value.hasComment(commentBefore) ||
    #####: 4832:         value.hasComment(commentAfterOnSameLine) ||
    #####: 4833:         value.hasComment(commentAfter);
        -: 4834:}
        -: 4835:
        -: 4836://////////////////////////
        -: 4837:// BuiltStyledStreamWriter
        -: 4838:
        -: 4839:/// Scoped enums are not available until C++11.
        -: 4840:struct CommentStyle {
        -: 4841:  /// Decide whether to write comments.
        -: 4842:  enum Enum {
        -: 4843:    None, ///< Drop all comments.
        -: 4844:    Most, ///< Recover odd behavior of previous versions (not implemented yet).
        -: 4845:    All   ///< Keep all comments.
        -: 4846:  };
        -: 4847:};
        -: 4848:
        -: 4849:struct BuiltStyledStreamWriter : public StreamWriter {
        -: 4850:  BuiltStyledStreamWriter(String indentation, CommentStyle::Enum cs,
        -: 4851:                          String colonSymbol, String nullSymbol,
        -: 4852:                          String endingLineFeedSymbol, bool useSpecialFloats,
        -: 4853:                          bool emitUTF8, unsigned int precision,
        -: 4854:                          PrecisionType precisionType);
        -: 4855:  int write(Value const& root, OStream* sout) override;
        -: 4856:
        -: 4857:private:
        -: 4858:  void writeValue(Value const& value);
        -: 4859:  void writeArrayValue(Value const& value);
        -: 4860:  bool isMultilineArray(Value const& value);
        -: 4861:  void pushValue(String const& value);
        -: 4862:  void writeIndent();
        -: 4863:  void writeWithIndent(String const& value);
        -: 4864:  void indent();
        -: 4865:  void unindent();
        -: 4866:  void writeCommentBeforeValue(Value const& root);
        -: 4867:  void writeCommentAfterValueOnSameLine(Value const& root);
        -: 4868:  static bool hasCommentForValue(const Value& value);
        -: 4869:
        -: 4870:  using ChildValues = std::vector<String>;
        -: 4871:
        -: 4872:  ChildValues childValues_;
        -: 4873:  String indentString_;
        -: 4874:  unsigned int rightMargin_;
        -: 4875:  String indentation_;
        -: 4876:  CommentStyle::Enum cs_;
        -: 4877:  String colonSymbol_;
        -: 4878:  String nullSymbol_;
        -: 4879:  String endingLineFeedSymbol_;
        -: 4880:  bool addChildValues_ : 1;
        -: 4881:  bool indented_ : 1;
        -: 4882:  bool useSpecialFloats_ : 1;
        -: 4883:  bool emitUTF8_ : 1;
        -: 4884:  unsigned int precision_;
        -: 4885:  PrecisionType precisionType_;
        -: 4886:};
        3: 4887:BuiltStyledStreamWriter::BuiltStyledStreamWriter(
        -: 4888:    String indentation, CommentStyle::Enum cs, String colonSymbol,
        -: 4889:    String nullSymbol, String endingLineFeedSymbol, bool useSpecialFloats,
        3: 4890:    bool emitUTF8, unsigned int precision, PrecisionType precisionType)
        3: 4891:    : rightMargin_(74), indentation_(std::move(indentation)), cs_(cs),
        6: 4892:      colonSymbol_(std::move(colonSymbol)), nullSymbol_(std::move(nullSymbol)),
        3: 4893:      endingLineFeedSymbol_(std::move(endingLineFeedSymbol)),
        -: 4894:      addChildValues_(false), indented_(false),
        -: 4895:      useSpecialFloats_(useSpecialFloats), emitUTF8_(emitUTF8),
        3: 4896:      precision_(precision), precisionType_(precisionType) {}
        3: 4897:int BuiltStyledStreamWriter::write(Value const& root, OStream* sout) {
        3: 4898:  sout_ = sout;
        3: 4899:  addChildValues_ = false;
        3: 4900:  indented_ = true;
        3: 4901:  indentString_.clear();
        3: 4902:  writeCommentBeforeValue(root);
        3: 4903:  if (!indented_)
    #####: 4904:    writeIndent();
        3: 4905:  indented_ = true;
        3: 4906:  writeValue(root);
        3: 4907:  writeCommentAfterValueOnSameLine(root);
        3: 4908:  *sout_ << endingLineFeedSymbol_;
        3: 4909:  sout_ = nullptr;
        3: 4910:  return 0;
        -: 4911:}
      283: 4912:void BuiltStyledStreamWriter::writeValue(Value const& value) {
      283: 4913:  switch (value.type()) {
    #####: 4914:  case nullValue:
    #####: 4915:    pushValue(nullSymbol_);
    #####: 4916:    break;
      160: 4917:  case intValue:
      160: 4918:    pushValue(valueToString(value.asLargestInt()));
      160: 4919:    break;
    #####: 4920:  case uintValue:
    #####: 4921:    pushValue(valueToString(value.asLargestUInt()));
    #####: 4922:    break;
    #####: 4923:  case realValue:
    #####: 4924:    pushValue(valueToString(value.asDouble(), useSpecialFloats_, precision_,
        -: 4925:                            precisionType_));
    #####: 4926:    break;
       60: 4927:  case stringValue: {
        -: 4928:    // Is NULL is possible for value.string_? No.
        -: 4929:    char const* str;
        -: 4930:    char const* end;
       60: 4931:    bool ok = value.getString(&str, &end);
       60: 4932:    if (ok)
       60: 4933:      pushValue(valueToQuotedStringN(str, static_cast<unsigned>(end - str),
       60: 4934:                                     emitUTF8_));
        -: 4935:    else
    #####: 4936:      pushValue("");
       60: 4937:    break;
        -: 4938:  }
    #####: 4939:  case booleanValue:
    #####: 4940:    pushValue(valueToString(value.asBool()));
    #####: 4941:    break;
        3: 4942:  case arrayValue:
        3: 4943:    writeArrayValue(value);
        3: 4944:    break;
       60: 4945:  case objectValue: {
      120: 4946:    Value::Members members(value.getMemberNames());
       60: 4947:    if (members.empty())
    #####: 4948:      pushValue("{}");
        -: 4949:    else {
       60: 4950:      writeWithIndent("{");
       60: 4951:      indent();
       60: 4952:      auto it = members.begin();
        -: 4953:      for (;;) {
      260: 4954:        String const& name = *it;
      260: 4955:        Value const& childValue = value[name];
      260: 4956:        writeCommentBeforeValue(childValue);
      260: 4957:        writeWithIndent(valueToQuotedStringN(
      260: 4958:            name.data(), static_cast<unsigned>(name.length()), emitUTF8_));
      260: 4959:        *sout_ << colonSymbol_;
      260: 4960:        writeValue(childValue);
      260: 4961:        if (++it == members.end()) {
       60: 4962:          writeCommentAfterValueOnSameLine(childValue);
       60: 4963:          break;
        -: 4964:        }
      200: 4965:        *sout_ << ",";
      200: 4966:        writeCommentAfterValueOnSameLine(childValue);
      200: 4967:      }
       60: 4968:      unindent();
       60: 4969:      writeWithIndent("}");
        -: 4970:    }
       60: 4971:  } break;
        -: 4972:  }
      283: 4973:}
        -: 4974:
        3: 4975:void BuiltStyledStreamWriter::writeArrayValue(Value const& value) {
        3: 4976:  unsigned size = value.size();
        3: 4977:  if (size == 0)
    #####: 4978:    pushValue("[]");
        -: 4979:  else {
       3*: 4980:    bool isMultiLine = (cs_ == CommentStyle::All) || isMultilineArray(value);
        3: 4981:    if (isMultiLine) {
        3: 4982:      writeWithIndent("[");
        3: 4983:      indent();
        3: 4984:      bool hasChildValue = !childValues_.empty();
        3: 4985:      unsigned index = 0;
        -: 4986:      for (;;) {
       20: 4987:        Value const& childValue = value[index];
       20: 4988:        writeCommentBeforeValue(childValue);
       20: 4989:        if (hasChildValue)
    #####: 4990:          writeWithIndent(childValues_[index]);
        -: 4991:        else {
       20: 4992:          if (!indented_)
       20: 4993:            writeIndent();
       20: 4994:          indented_ = true;
       20: 4995:          writeValue(childValue);
       20: 4996:          indented_ = false;
        -: 4997:        }
       20: 4998:        if (++index == size) {
        3: 4999:          writeCommentAfterValueOnSameLine(childValue);
        3: 5000:          break;
        -: 5001:        }
       17: 5002:        *sout_ << ",";
       17: 5003:        writeCommentAfterValueOnSameLine(childValue);
       17: 5004:      }
        3: 5005:      unindent();
        3: 5006:      writeWithIndent("]");
        -: 5007:    } else // output on a single line
        -: 5008:    {
    #####: 5009:      assert(childValues_.size() == size);
    #####: 5010:      *sout_ << "[";
    #####: 5011:      if (!indentation_.empty())
    #####: 5012:        *sout_ << " ";
    #####: 5013:      for (unsigned index = 0; index < size; ++index) {
    #####: 5014:        if (index > 0)
    #####: 5015:          *sout_ << ((!indentation_.empty()) ? ", " : ",");
    #####: 5016:        *sout_ << childValues_[index];
        -: 5017:      }
    #####: 5018:      if (!indentation_.empty())
    #####: 5019:        *sout_ << " ";
    #####: 5020:      *sout_ << "]";
        -: 5021:    }
        -: 5022:  }
        3: 5023:}
        -: 5024:
    #####: 5025:bool BuiltStyledStreamWriter::isMultilineArray(Value const& value) {
    #####: 5026:  ArrayIndex const size = value.size();
    #####: 5027:  bool isMultiLine = size * 3 >= rightMargin_;
    #####: 5028:  childValues_.clear();
    #####: 5029:  for (ArrayIndex index = 0; index < size && !isMultiLine; ++index) {
    #####: 5030:    Value const& childValue = value[index];
    #####: 5031:    isMultiLine = ((childValue.isArray() || childValue.isObject()) &&
    #####: 5032:                   !childValue.empty());
        -: 5033:  }
    #####: 5034:  if (!isMultiLine) // check if line length > max line length
        -: 5035:  {
    #####: 5036:    childValues_.reserve(size);
    #####: 5037:    addChildValues_ = true;
    #####: 5038:    ArrayIndex lineLength = 4 + (size - 1) * 2; // '[ ' + ', '*n + ' ]'
    #####: 5039:    for (ArrayIndex index = 0; index < size; ++index) {
    #####: 5040:      if (hasCommentForValue(value[index])) {
    #####: 5041:        isMultiLine = true;
        -: 5042:      }
    #####: 5043:      writeValue(value[index]);
    #####: 5044:      lineLength += static_cast<ArrayIndex>(childValues_[index].length());
        -: 5045:    }
    #####: 5046:    addChildValues_ = false;
    #####: 5047:    isMultiLine = isMultiLine || lineLength >= rightMargin_;
        -: 5048:  }
    #####: 5049:  return isMultiLine;
        -: 5050:}
        -: 5051:
      220: 5052:void BuiltStyledStreamWriter::pushValue(String const& value) {
      220: 5053:  if (addChildValues_)
    #####: 5054:    childValues_.push_back(value);
        -: 5055:  else
      220: 5056:    *sout_ << value;
      220: 5057:}
        -: 5058:
      383: 5059:void BuiltStyledStreamWriter::writeIndent() {
        -: 5060:  // blep intended this to look at the so-far-written string
        -: 5061:  // to determine whether we are already indented, but
        -: 5062:  // with a stream we cannot do that. So we rely on some saved state.
        -: 5063:  // The caller checks indented_.
        -: 5064:
      383: 5065:  if (!indentation_.empty()) {
        -: 5066:    // In this case, drop newlines too.
      383: 5067:    *sout_ << '\n' << indentString_;
        -: 5068:  }
      383: 5069:}
        -: 5070:
      386: 5071:void BuiltStyledStreamWriter::writeWithIndent(String const& value) {
      386: 5072:  if (!indented_)
      363: 5073:    writeIndent();
      386: 5074:  *sout_ << value;
      386: 5075:  indented_ = false;
      386: 5076:}
        -: 5077:
       63: 5078:void BuiltStyledStreamWriter::indent() { indentString_ += indentation_; }
        -: 5079:
       63: 5080:void BuiltStyledStreamWriter::unindent() {
      63*: 5081:  assert(indentString_.size() >= indentation_.size());
       63: 5082:  indentString_.resize(indentString_.size() - indentation_.size());
       63: 5083:}
        -: 5084:
      283: 5085:void BuiltStyledStreamWriter::writeCommentBeforeValue(Value const& root) {
      283: 5086:  if (cs_ == CommentStyle::None)
     283*: 5087:    return;
      283: 5088:  if (!root.hasComment(commentBefore))
      283: 5089:    return;
        -: 5090:
    #####: 5091:  if (!indented_)
    #####: 5092:    writeIndent();
    #####: 5093:  const String& comment = root.getComment(commentBefore);
    #####: 5094:  String::const_iterator iter = comment.begin();
    #####: 5095:  while (iter != comment.end()) {
    #####: 5096:    *sout_ << *iter;
    #####: 5097:    if (*iter == '\n' && ((iter + 1) != comment.end() && *(iter + 1) == '/'))
        -: 5098:      // writeIndent();  // would write extra newline
    #####: 5099:      *sout_ << indentString_;
    #####: 5100:    ++iter;
        -: 5101:  }
    #####: 5102:  indented_ = false;
        -: 5103:}
        -: 5104:
      283: 5105:void BuiltStyledStreamWriter::writeCommentAfterValueOnSameLine(
        -: 5106:    Value const& root) {
      283: 5107:  if (cs_ == CommentStyle::None)
    #####: 5108:    return;
      283: 5109:  if (root.hasComment(commentAfterOnSameLine))
    #####: 5110:    *sout_ << " " + root.getComment(commentAfterOnSameLine);
        -: 5111:
      283: 5112:  if (root.hasComment(commentAfter)) {
    #####: 5113:    writeIndent();
    #####: 5114:    *sout_ << root.getComment(commentAfter);
        -: 5115:  }
        -: 5116:}
        -: 5117:
        -: 5118:// static
    #####: 5119:bool BuiltStyledStreamWriter::hasCommentForValue(const Value& value) {
    #####: 5120:  return value.hasComment(commentBefore) ||
    #####: 5121:         value.hasComment(commentAfterOnSameLine) ||
    #####: 5122:         value.hasComment(commentAfter);
        -: 5123:}
        -: 5124:
        -: 5125:///////////////
        -: 5126:// StreamWriter
        -: 5127:
        3: 5128:StreamWriter::StreamWriter() : sout_(nullptr) {}
        -: 5129:StreamWriter::~StreamWriter() = default;
        -: 5130:StreamWriter::Factory::~Factory() = default;
        3: 5131:StreamWriterBuilder::StreamWriterBuilder() { setDefaults(&settings_); }
        -: 5132:StreamWriterBuilder::~StreamWriterBuilder() = default;
        3: 5133:StreamWriter* StreamWriterBuilder::newStreamWriter() const {
        6: 5134:  const String indentation = settings_["indentation"].asString();
        6: 5135:  const String cs_str = settings_["commentStyle"].asString();
        6: 5136:  const String pt_str = settings_["precisionType"].asString();
        3: 5137:  const bool eyc = settings_["enableYAMLCompatibility"].asBool();
        3: 5138:  const bool dnp = settings_["dropNullPlaceholders"].asBool();
        3: 5139:  const bool usf = settings_["useSpecialFloats"].asBool();
        3: 5140:  const bool emitUTF8 = settings_["emitUTF8"].asBool();
        3: 5141:  unsigned int pre = settings_["precision"].asUInt();
        3: 5142:  CommentStyle::Enum cs = CommentStyle::All;
        3: 5143:  if (cs_str == "All") {
        3: 5144:    cs = CommentStyle::All;
    #####: 5145:  } else if (cs_str == "None") {
    #####: 5146:    cs = CommentStyle::None;
        -: 5147:  } else {
    #####: 5148:    throwRuntimeError("commentStyle must be 'All' or 'None'");
        -: 5149:  }
        3: 5150:  PrecisionType precisionType(significantDigits);
        3: 5151:  if (pt_str == "significant") {
        3: 5152:    precisionType = PrecisionType::significantDigits;
    #####: 5153:  } else if (pt_str == "decimal") {
    #####: 5154:    precisionType = PrecisionType::decimalPlaces;
        -: 5155:  } else {
    #####: 5156:    throwRuntimeError("precisionType must be 'significant' or 'decimal'");
        -: 5157:  }
        6: 5158:  String colonSymbol = " : ";
        3: 5159:  if (eyc) {
    #####: 5160:    colonSymbol = ": ";
        3: 5161:  } else if (indentation.empty()) {
    #####: 5162:    colonSymbol = ":";
        -: 5163:  }
        6: 5164:  String nullSymbol = "null";
        3: 5165:  if (dnp) {
    #####: 5166:    nullSymbol.clear();
        -: 5167:  }
        3: 5168:  if (pre > 17)
    #####: 5169:    pre = 17;
        3: 5170:  String endingLineFeedSymbol;
        -: 5171:  return new BuiltStyledStreamWriter(indentation, cs, colonSymbol, nullSymbol,
        -: 5172:                                     endingLineFeedSymbol, usf, emitUTF8, pre,
        6: 5173:                                     precisionType);
        -: 5174:}
        -: 5175:
    #####: 5176:bool StreamWriterBuilder::validate(Json::Value* invalid) const {
        -: 5177:  static const auto& valid_keys = *new std::set<String>{
        -: 5178:      "indentation",
        -: 5179:      "commentStyle",
        -: 5180:      "enableYAMLCompatibility",
        -: 5181:      "dropNullPlaceholders",
        -: 5182:      "useSpecialFloats",
        -: 5183:      "emitUTF8",
        -: 5184:      "precision",
        -: 5185:      "precisionType",
    #####: 5186:  };
    #####: 5187:  for (auto si = settings_.begin(); si != settings_.end(); ++si) {
    #####: 5188:    auto key = si.name();
    #####: 5189:    if (valid_keys.count(key))
    #####: 5190:      continue;
    #####: 5191:    if (invalid)
    #####: 5192:      (*invalid)[std::move(key)] = *si;
        -: 5193:    else
    #####: 5194:      return false;
        -: 5195:  }
    #####: 5196:  return invalid ? invalid->empty() : true;
        -: 5197:}
        -: 5198:
    #####: 5199:Value& StreamWriterBuilder::operator[](const String& key) {
    #####: 5200:  return settings_[key];
        -: 5201:}
        -: 5202:// static
        3: 5203:void StreamWriterBuilder::setDefaults(Json::Value* settings) {
        -: 5204:  //! [StreamWriterBuilderDefaults]
        3: 5205:  (*settings)["commentStyle"] = "All";
        3: 5206:  (*settings)["indentation"] = "\t";
        3: 5207:  (*settings)["enableYAMLCompatibility"] = false;
        3: 5208:  (*settings)["dropNullPlaceholders"] = false;
        3: 5209:  (*settings)["useSpecialFloats"] = false;
        3: 5210:  (*settings)["emitUTF8"] = false;
        3: 5211:  (*settings)["precision"] = 17;
        3: 5212:  (*settings)["precisionType"] = "significant";
        -: 5213:  //! [StreamWriterBuilderDefaults]
        3: 5214:}
        -: 5215:
    #####: 5216:String writeString(StreamWriter::Factory const& factory, Value const& root) {
    #####: 5217:  OStringStream sout;
    #####: 5218:  StreamWriterPtr const writer(factory.newStreamWriter());
    #####: 5219:  writer->write(root, &sout);
    #####: 5220:  return sout.str();
        -: 5221:}
        -: 5222:
        3: 5223:OStream& operator<<(OStream& sout, Value const& root) {
        6: 5224:  StreamWriterBuilder builder;
        3: 5225:  StreamWriterPtr const writer(builder.newStreamWriter());
        3: 5226:  writer->write(root, &sout);
        6: 5227:  return sout;
        -: 5228:}
        -: 5229:
        -: 5230:} // namespace Json
        -: 5231:
        -: 5232:// //////////////////////////////////////////////////////////////////////
        -: 5233:// End of content of file: src/lib_json/json_writer.cpp
        -: 5234:// //////////////////////////////////////////////////////////////////////
        -: 5235:
        -: 5236:
        -: 5237:
        -: 5238:
        -: 5239:

